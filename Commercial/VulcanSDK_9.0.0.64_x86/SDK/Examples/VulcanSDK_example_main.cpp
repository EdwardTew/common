//////////////////////////////////////////////////////////////////////////////
//
// Name        : VulcanSDK_example_main.cpp
// Description : The main function for the VulcanSDK examples
// Version     : 9.0.0.64
//
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "VulcanSDK_examples.h"

void PrintUsage()
{
   printf("\nUsage: One of the following\n");
   printf("\tVulcanSDK_example -b blockmodel_filename\n");
   printf("\tVulcanSDK_example -t triangulation_filename\n");
   printf("\tVulcanSDK_example -g grid_filename\n");
   printf("\tVulcanSDK_example -d designdatabase_filename\n");
}

int main(int argc, char *argv[])
{
   if (argc != 3)
   {
      PrintUsage();
      return 1;
   }

   if (strcmp(argv[1], "-b") == 0)
   {
      return RunBlockModelExample(argv[2]);
   }
   else if (strcmp(argv[1], "-t") == 0)
   {
      return RunTriangulationExample(argv[2]);
   }
   else if (strcmp(argv[1], "-g") == 0)
   {
      return RunGridExample(argv[2]);
   }
   else if (strcmp(argv[1], "-d") == 0)
   {
      return RunDesignExample(argv[2]);
   }
   else
   {
      PrintUsage();
      return 1;
   }
}