//////////////////////////////////////////////////////////////////////////////
//
// Name        : VulcanSDK_design_example.cpp
// Description : A basic example of using the VulcanSDK with design data
// Version     : 9.0.0.64
//
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "VulcanSDK_examples.h"
#include "mtkExportedApi_ReadWrite.h"

int RunDesignExample(char* filename)
{
   int status, i;

   MTK_Database* database;
   MTK_Layer* layer;
   MTK_Object* object;

   printf("Initialising API...");
   MTK_API_SetCertificate("Maptek_Example.crt");

   if (MTK_API_Init("vulcan_sdk_developer"))
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   printf("Opening design database %s...", filename);
   database = MTK_Database_Open(filename, DESIGN_MODE_WRITE);
   if (!database)
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   // This code creates a new polyline that happens to be in the form of a
   //    triangle
   printf("Creating a new polyline...");
   object = MTK_Object_New();
   if (!object)
   {
      HandleError();
      return 1;
   }
   status = MTK_Object_SetType(object, DESIGN_POLYLINE);
   if (status)
   {
      HandleError();
      return 1;
   }
   status = MTK_Object_Polyline_AppendPoint(
               object,
               78000, 4500, 0,  // Coordinate
               0,               // w value
               0,               // Whether or not to draw to the point
               ""               // Point name
            );
   if (status)
   {
      HandleError();
      return 1;
   }
   for (i = 5; i <= 50; i = i + 5)
   {
      status = MTK_Object_Polyline_AppendPoint(
                  object, (double)(i + 78000), double(i + 4500), double(i),
                  0, 1, ""
               );
      if (status)
      {
         HandleError();
         return 1;
      }
   }
   status = MTK_Object_Polyline_AppendPoint(object, 78050, 4550, 0, 0, 1, "");
   if (status)
   {
      HandleError();
      return 1;
   }
   status = MTK_Object_Polyline_SetAttributes(
               object,
               -1, // Leave linetype default
               -1, // Leave pattern default
               -1, // Leave orientation default
               1   // Set 'closed' to true
            );
   if (status)
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   // This code creates a new layer with an arbitrary name/description and
   //    adds the new polyline to it
   printf("Creating a new layer and adding that polyline to it...");
   layer = MTK_Layer_New();
   if (!layer)
   {
      HandleError();
      return 1;
   }
   status = MTK_Layer_SetName(layer, "ExampleLayer");
   if (status)
   {
      HandleError();
      return 1;
   }
   status = MTK_Layer_SetDescription(layer, "Layer created in VulcanSDK example");
   if (status)
   {
      HandleError();
      return 1;
   }
   status = MTK_Layer_AppendObject(layer, object);
   if (status)
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   printf("Saving that layer to database %s...", filename);
   status = MTK_Database_SaveLayer(database, layer);
   if (status)
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   if (MTK_Database_Close(database))
   {
      HandleError();
      return 1;
   }

   return 0;
}