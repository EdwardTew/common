//////////////////////////////////////////////////////////////////////////////
//
// Name        : mtkExportedApi_ReadWrite.h
// Description : The header for the read-write mtkExportedApi
// Version     : 9.0.0.64
//
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
// Basic API Functions
//
//    MTK_API_*
//
//       SetCertificate
//          Note: If you are using certificate authorisation
//                   MTK_API_SetCertificate must be called before
//                   MTK_API_Init.
//       Init
//          Note: MTK_API_Init must either be called first (if not using
//                   certificate authorisation) or second (immediately after
//                   MTK_API_SetCertificate).
//       HasError
//       GetError
//       IsTagAuthorised
//       StartLogging, FinishLogging
//
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
// Block Model API Functions
//
//    MTK_BlockModel_*
//
//       Open, Close, IsOpen
//
//       GetModelExtent, GetModelOrigin, GetModelOrientation
//
//       IsStratigraphicModel
//
//       NVariables, NPredefinedVariables, NNumericVariables, NStringVariables,
//          NClassificationVariables
//       GetVariableName, GetPredefinedVariableName, GetNumericVariableName,
//          GetStringVariableName, GetClassificationVariableName
//       NTranslations, GetTranslationName, GetVariableDescription
//       IsNumber, GetNumber, GetDefaultNumber, SetNumber
//       IsString, GetString, GetDefaultString, SetString
//       GetVariableType
//
//       FirstBlock, NextBlock, IsEof
//       NBlocks, GetBlockExtent, GetPosition, SetPosition
//       FindXyz, FindWorldXyz
//
//       ModelToWorld, WorldToModel
//
//       ClearMatch
//       AddMatchTriangulation, SetMatchExtent
//       SetUseExactVolume
//       GetBlockMatchExtent, GetBlockMatchVolume
//
//       NSchemas
//       GetSchemaExtent, GetSchemaSizes, GetSchemaDimensions
//
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
// Triangulation API Functions
//
//    MTK_Triangulation_*
//
//       New, Open, Close, Save
//       IsValid
//
//       GetColour, SetColour
//       IsTranslucent, GetTranslucency, SetTranslucency
//       IsTextured, GetTexture, SetTexture
//
//       NumPoints, GetPoint, AddPoint
//       NumTriangles, GetTriangle, AddTriangle
//
//       GetAttributeNames, DeleteAttribute, GetAttributeType
//       AddAttributeInt32, AddAttributeInt64, AddAttributeBoolean
//       AddAttributeDouble, AddAttributeString, AddAttributeDateYMD
//       AddAttributeDateMDY, AddAttributeDateDMY
//       GetAttributeAsInt32, GetAttributeAsInt64, GetAttributeAsBoolean
//       GetAttributeAsDouble, GetAttributeAsString, GetAttributeAsDate
//
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
// Grid API Functions
//
//    MTK_Grid_*
//
//       New, Open, Close, Save
//       IsValid
//
//       GetExtent, GetSpacing
//
//       NumNodes, GetNodeValue, SetNodeValue
//
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
// Design API Functions
//
// Note: Any changes to an existing object must be saved using the
//       MTK_Layer_SetObject function.  Simply changing a retrieved object
//       will not save the changes.
//
//    MTK_Object_*
//
//       New, Close
//       GetName, GetGroup, GetDescription, GetColour, GetColourIndex,
//       GetValue, GetType, GetFeature, GetPrimitive
//       SetName, SetGroup, SetDescription, SetColour, SetValue,
//       SetType, SetFeature, SetPrimitive
//
//    MTK_Object_Polyline_*
//
//       NPoints, AppendPoint, DeletePoint, DeleteAllPoints
//       GetAttributes, GetPointInfo, GetPointType, GetLineType
//       SetAttributes, SetPointInfo, SetPointType, SetLineType
//
//    MTK_Object_Text_*
//
//       NRecords, GetRecord
//       SetRecord, AppendRecord, DeleteRecord, DeleteAllRecords
//       GetOrigin, GetAttributes
//       SetOrigin, SetAttributes
//
//    MTK_Object_3DText_*
//
//       NRecords, GetRecord
//       SetRecord, AppendRecord, DeleteRecord, DeleteAllRecords
//       GetOrigin, GetOriginPosition, GetDirection, GetNormal, GetAttributes
//       SetOrigin, SetOriginPosition, SetDirection, SetNormal, SetAttributes
//
//    MTK_Object_Arrow_*
//
//       GetStart, GetEnd, GetNormal, GetAttributes, GetHeadInfo,
//          GetRelativeHeadInfo, GetScaledHeadInfo
//       SetStart, SetEnd, SetNormal, SetAttributes, SetHeadInfo,
//          SetRelativeHeadInfo
//
//    MTK_Object_DimensionAngle_*
//
//       GetRadius, GetSweep, GetOrigin, GetAttributes, GetTextAttributes
//       SetRadius, SetSweep, SetOrigin, SetAttributes, SetTextAttributes
//
//    MTK_Object_DimensionArc_*
//
//       GetRadius, GetSweep, GetOrigin, GetAttributes, GetTextAttributes
//       SetRadius, SetSweep, SetOrigin, SetAttributes, SetTextAttributes
//
//    MTK_Object_DimensionLine_*
//
//       GetStart, GetEnd, GetNormal, GetAttributes, GetTextAttributes
//       SetStart, SetEnd, SetNormal, SetAttributes, SetTextAttributes
//
//    MTK_Object_DimensionRadius_*
//
//       GetStart, GetEnd, GetNormal, GetAttributes, GetTextAttributes
//       SetStart, SetEnd, SetNormal, SetAttributes, SetTextAttributes
//
//    MTK_Layer_*
//
//       New, IsEdited, Close
//       NObjects, GetObject
//       SetObject, AppendObject, DeleteObject
//       GetName, GetDescription
//       SetName, SetDescription
//
//    MTK_Database_*
//
//       Open, Close, IsOpen
//       GetName
//       NLayers, LayerExists, GetLayerByName, GetLayerByIndex,
//       MTK_Database_GetLayerNameByIndex
//       SaveLayer, SaveLayerAs, DeleteLayer
//
//////////////////////////////////////////////////////////////////////////////


#ifndef MTK_EXPORTED_API_READWRITE_H
#define MTK_EXPORTED_API_READWRITE_H

#ifdef MAKING_EXPORTED_API_DLL
#define MTK_EXPORTED_API_DLL_EXPORT __declspec(dllexport)
#else
#define MTK_EXPORTED_API_DLL_EXPORT __declspec(dllimport)
#endif

#ifdef WIN32
typedef __int64 Int64;
#else
typedef signed long long Int64;
#endif

struct MTK_BlockModel;
struct MTK_Triangulation;
struct MTK_Grid;
struct MTK_Object;
struct MTK_Layer;
struct MTK_Database;

#define MTK_BUFFER_SIZE 255

enum MTK_Database_OpenMode
{
   DESIGN_MODE_READONLY,
   DESIGN_MODE_WRITE,
   DESIGN_MODE_CREATEIF,
   DESIGN_MODE_CREATE
};

enum MTK_ObjectType
{
   DESIGN_NULL,
   DESIGN_POLYLINE,
   DESIGN_TEXT,
   DESIGN_3DTEXT,
   DESIGN_ARROW,
   DESIGN_DIMENSIONLINE,
   DESIGN_DIMENSIONRADIUS,
   DESIGN_DIMENSIONARC,
   DESIGN_DIMENSIONANGLE
};

enum MTK_Object_TextHoriPos
{
   TEXT_HORI_LEFT,
   TEXT_HORI_CENTRE,
   TEXT_HORI_RIGHT
};

enum MTK_Object_TextVertPos
{
   TEXT_VERT_BOTTOM,
   TEXT_VERT_CENTRE,
   TEXT_VERT_TOP
};

enum MTK_Object_ArrowType
{
   ARROW_2D,
   ARROW_3D,
   ARROW_FAN
};

enum MTK_BlockModel_VariableType
{
   BM_VAR_UNKNOWN,
   BM_VAR_BIT,
   BM_VAR_BYTE,
   BM_VAR_INT16,
   BM_VAR_UINT16,
   BM_VAR_INT32,
   BM_VAR_INT64,
   BM_VAR_FLOAT,
   BM_VAR_DOUBLE,
   BM_VAR_STRING
};

enum MTK_Triangulation_AttributeType
{
   TRI_ATTRIBUTE_UNDEFINED,
   TRI_ATTRIBUTE_INT32,
   TRI_ATTRIBUTE_INT64,
   TRI_ATTRIBUTE_BOOLEAN,
   TRI_ATTRIBUTE_DOUBLE,
   TRI_ATTRIBUTE_STRING,
   TRI_ATTRIBUTE_DATE_YMD,
   TRI_ATTRIBUTE_DATE_DMY,
   TRI_ATTRIBUTE_DATE_MDY
};

extern "C" {

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_SetCertificate
//  Description : Sets the certificate name that will be checked.
//                This must be run first if certificate authorisation is
//                   being used.
//  Parameters  : certificateName - filename of the certificate
//  Returns     :
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
void MTK_API_SetCertificate(const char* certificateName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_Init
//  Description : Initialises the API. MTK_API_SetCertificate must be run
//                   first; otherwise this will fail.
//                This must be run either first (if not using certificate
//                   authorisation) or second (immediately after
//                   MTK_API_SetCertificate).
//  Parameters  : licenseName - filename of the license
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
int MTK_API_Init(const char* licenseName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_HasError
//  Description : Check if blockmodel working correctly
//  Parameters  :
//  Returns     : 1 - error
//              : 0 - no error
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_API_HasError();

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_GetError
//  Description : Get current error string
//  Parameters  : error - buffer to receive the error message (char*)
//  Returns     :
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
void MTK_API_GetError(char* error);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_StartLogging
//  Description : Turn logging on. Does nothing if logging is already on.
//  Parameters  : filename - the name of the logfile
//  Returns     :
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
void MTK_API_StartLogging(const char* filename);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_FinishLogging
//  Description : Dump stats to the logfile, then stop logging. Does nothing
//                   if logging is already off.
//  Parameters  :
//  Returns     :
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
void MTK_API_FinishLogging();

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_IsTagAuthorised
//  Description : Check if a tag is authorised
//  Parameters  : tag - the name of the tag to check (char*)
//  Returns     : 1  - it is
//              : 0  - it is not
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_API_IsTagAuthorised(const char* tag);

//////////////////////////////////////////////////////////////////////////////
//
//  Name        : MTK_BlockModel_Open
//  Description : Open blockmodel
//  Parameters  : name - blockmodel filename (char*)
//              : readOnly - set to non-zero if readonly (int)
//  Returns     : Pointer to an empty MTK_BlockModel struct - success
//              : Null pointer - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_BlockModel* MTK_BlockModel_Open(const char* name, const int& readOnly);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_Close
//  Description : Close blockmodel.  Release any memory used.
//              : Note that this saves any changes to the block model file
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_Close(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//
//  Name        : MTK_BlockModel_IsOpen
//  Description : Query current object state
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : 0 - closed
//              : 1 - open
//              : other - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_IsOpen(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetModelExtent
//  Description : Get the model extent in model coordinates
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : x0 - lower left x coordinate  (double*)
//              : y0 - lower left y coordinate  (double*)
//              : z0 - lower left z coordinate  (double*)
//              : x1 - upper right x coordinate (double*)
//              : y1 - upper right y coordinate (double*)
//              : z1 - upper right z coordinate (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetModelExtent(MTK_BlockModel* bmodel,
                                  double* x0, double* y0, double* z0,
                                  double* x1, double* y1, double* z1);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetModelOrigin
//  Description : Get the model origin
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : x - x coordinate (double*)
//              : y - y coordinate (double*)
//              : z - z coordinate (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetModelOrigin(MTK_BlockModel* bmodel,
                                  double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetModelOrientation
//  Description : Get the model orientation
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : dip     - dip of the block model     (double*)
//              : plunge  - plunge of the block model  (double*)
//              : bearing - bearing of the block model (double*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetModelOrientation(MTK_BlockModel* bmodel,
                                       double* dip, double* plunge,
                                       double* bearing);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_IsStratigraphicModel
//  Description : Check if the block model has infinite Z sub-blocking
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//  Returns     : 1  - it is
//              : 0  - it is not
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_IsStratigraphicModel(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NVariables
//  Description : Number of variables
//              : Note that this does not include predefined variables
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : int - the number of blockmodel variables
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NVariables(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NNumericVariables
//  Description : Number of numeric variables
//              : Note that this does not include predefined variables
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : int - the number of numeric blockmodel variables
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NNumericVariables(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NStringVariables
//  Description : Number of string variables
//              : Note that this does not include predefined variables
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : int - the number of string/character blockmodel variables
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NStringVariables(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NPredefinedVariables
//  Description : Get the number of predefined variables
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : int - number of predefined blockmodel variables
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NPredefinedVariables(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NClassificationVariables
//  Description : Get the number of 'classification' variables
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : int - number of 'classification' blockmodel variables
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NClassificationVariables(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetVariableName
//  Description : Get a variable name based on the index
//              : Note that this does not include predefined variables
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : index   - the index of the variable to retrieve (int)
//              : varName - buffer to receive the variable name (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetVariableName(MTK_BlockModel* bmodel,
                                   const int& index, char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetNumericVariableName
//  Description : Get a numeric variable name based on the index
//              : Note that this does not include predefined variables
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : index   - the index of the variable to retrieve (int)
//              : varName - buffer to receive the variable name (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetNumericVariableName(MTK_BlockModel* bmodel,
                                          const int& index,
                                          char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetStringVariableName
//  Description : Get a string variable name based on the index
//              : Note that this does not include predefined variables
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : index   - the index of the variable to retrieve (int)
//              : varName - buffer to receive the variable name (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetStringVariableName(MTK_BlockModel* bmodel,
                                         const int& index,
                                         char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetPredefinedVariableName
//  Description : Get a predefined variable name based on the index
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : index   - the index of the variable to retrieve (int)
//              : varName - buffer to receive the variable name (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetPredefinedVariableName(MTK_BlockModel* bmodel,
                                             const int& index,
                                             char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetClassificationVariableName
//  Description : Get a classification variable name based on the index
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : index   - the index of the variable to retrieve (int)
//              : varName - buffer to receive the variable name (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetClassificationVariableName(MTK_BlockModel* bmodel,
                                                 const int& index,
                                                 char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NTranslations
//  Description : Get the number of translations for a string variable
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//  Returns     : int - number of translations for the variable
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NTranslations(MTK_BlockModel* bmodel, const char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetTranslationName
//  Description : Get a translation name based on the variable and index
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - string variable name (const char*)
//              : index   - the index of the translation to retrieve (int)
//              : translation - buffer to receive the translation name (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetTranslationName(MTK_BlockModel* bmodel,
                                      const char* varName, const int& index,
                                      char* translation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetVariableDescription
//  Description : Get the description of a variable
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - string variable name (const char*)
//              : description - buffer to receive the description (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetVariableDescription(MTK_BlockModel* bmodel,
                                          const char* varName,
                                          char* description);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_IsNumber
//  Description : Check if variable is numeric
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//  Returns     : 1  - it is
//              : 0  - it is not
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_IsNumber(MTK_BlockModel* bmodel, const char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetNumber
//  Description : Get double value for variable specified
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//              : value   - the number (double*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetNumber(MTK_BlockModel* bmodel,
                             const char* varName, double* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetDefaultNumber
//  Description : Get default double value for variable specified
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//              : value - the number (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetDefaultNumber(MTK_BlockModel* bmodel,
                                    const char *varName, double* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_SetNumber
//  Description : Set double value for variable specified
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//              : value   - the number (double)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_SetNumber(MTK_BlockModel* bmodel,
                             const char* varName, const double& value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_IsString
//  Description : Check if variable is string
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//  Returns     : 1  - it is
//              : 0  - it is not
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_IsString(MTK_BlockModel* bmodel, const char* varName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetString
//  Description : Get string value for variable specified
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//              : value   - the string (char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetString(MTK_BlockModel* bmodel,
                             const char* varName, char* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetDefaultString
//  Description : Get default string value for variable specified
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//              : value - the string (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetDefaultString(MTK_BlockModel* bmodel,
                                    const char *varName, char *value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_SetString
//  Description : Set string value for variable specified
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//              : value   - the string (const char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_SetString(MTK_BlockModel* bmodel,
                             const char* varName, const char* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetVariableType
//  Description : Test the variable type
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : varName - variable name (const char*)
//              : type    - variable type (MTK_BlockModel_VariableType*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetVariableType(MTK_BlockModel* bmodel,
                                   const char* varName,
                                   MTK_BlockModel_VariableType *type);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_FirstBlock
//  Description : Position at first block
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_FirstBlock(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NextBlock
//  Description : Position at the next block
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NextBlock(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_IsEof
//  Description : Check if at the end of the file (blockmodel)
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : 1  - at end of file (blockmodel)
//              : 0  - not at the end yet
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_IsEof(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NBlocks
//  Description : Get the total number of model blocks
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : Int64 - number of model blocks
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
Int64 MTK_BlockModel_NBlocks(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetBlockExtent
//  Description : Get the block extent in model coordinates
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : x0 - lower left x coordinate  (double*)
//              : y0 - lower left y coordinate  (double*)
//              : z0 - lower left z coordinate  (double*)
//              : x1 - upper right x coordinate (double*)
//              : y1 - upper right y coordinate (double*)
//              : z1 - upper right z coordinate (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetBlockExtent(MTK_BlockModel* bmodel,
                                  double* x0, double* y0, double* z0,
                                  double* x1, double* y1, double* z1);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetPosition
//  Description : Get current block postion
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : Int64 - current block position
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
Int64 MTK_BlockModel_GetPosition(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_SetPosition
//  Description : Set the block navigation to the block at the position
//              :    specified
//              : Note that this will nullify the current selection.
//  Parameters  : bmodel   - pointer to an MTK_BlockModel struct
//              : position - block position (Int64)
//  Returns     : 0        - success
//              : other    - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_SetPosition(MTK_BlockModel* bmodel, const Int64& position);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_FindXyz
//  Description : Locate block with block coordinates supplied
//              : Note that this will nullify the current selection.
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : mx - model x coordinate (double)
//              : my - model y coordinate (double)
//              : mz - model z coordinate (double)
//  Returns     : 1  - located
//              : 0  - not found
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_FindXyz(MTK_BlockModel* bmodel,
                           const double& mx,
                           const double& my,
                           const double& mz);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_FindWorldXyz
//  Description : Locate block with world coordinates supplied
//              : Note that this will nullify the current selection.
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : wx - world x coordinate (double)
//              : wy - world y coordinate (double)
//              : wz - world z coordinate (double)
//  Returns     : 1  - block not found
//              : 0  - success
//              : -1 - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_FindWorldXyz(MTK_BlockModel* bmodel,
                                const double& wx,
                                const double& wy,
                                const double& wz);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_ModelToWorld
//  Description : Convert block coordinate to world coordinate
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : mx - model x coordinate (double)
//              : my - model y coordinate (double)
//              : mz - model z coordinate (double)
//              : wx - world x coordinate (double*)
//              : wy - world y coordinate (double*)
//              : wz - world z coordinate (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_ModelToWorld(MTK_BlockModel* bmodel,
                                const double& mx,
                                const double& my,
                                const double& mz,
                                double* wx, double* wy, double* wz);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_WorldToModel
//  Description : Convert world coordinate to model coordinate
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : wx - world x coordinate (double)
//              : wy - world y coordinate (double)
//              : wz - world z coordinate (double)
//              : mx - model x coordinate (double*)
//              : my - model y coordinate (double*)
//              : mz - model z coordinate (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_WorldToModel(MTK_BlockModel* bmodel,
                                const double& wx,
                                const double& wy,
                                const double& wz,
                                double* mx, double* my, double* mz);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_ClearMatch
//  Description : Clear current blockmodel match
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_ClearMatch(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_AddMatchTriangulation
//  Description : Add triangultion to match blockmodel against.
//  Parameters  : bmodel  - pointer to an MTK_BlockModel struct
//              : triName - triangulation name (const char*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_AddMatchTriangulation(MTK_BlockModel* bmodel,
                                         const char* triName);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_SetMatchExtent
//  Description : Set the match extent in model coordinates
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : x0 - lower left x coordinate  (double)
//              : y0 - lower left y coordinate  (double)
//              : z0 - lower left z coordinate  (double)
//              : x1 - upper right x coordinate (double)
//              : y1 - upper right y coordinate (double)
//              : z1 - upper right z coordinate (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_SetMatchExtent(MTK_BlockModel* bmodel,
                                  const double& x0, const double& y0,
                                  const double& z0, const double& x1,
                                  const double& y1, const double& z1);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_SetUseExactVolume
//  Description : Set or unset the use exact volume flag.  When the exact
//              : volume flag is set, the extent of the block match is
//              : modified to match the volume of the intersection between the
//              : solid and the original block extent.  This is also called
//              : proportional matching.
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : use    - if 1, exact intersection volumes are returned,
//              :          otherwise the volume is the volume of the entire
//              :          block (int)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_SetUseExactVolume(MTK_BlockModel* bmodel, const int& use);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetBlockMatchVolume
//  Description : Get current block matched volume
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : double - block match volume
//              : -1     - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
double MTK_BlockModel_GetBlockMatchVolume(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetBlockMatchExtent
//  Description : Return the match extent of the current matched block.
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : x0 - lower left x coordinate  (double*)
//              : y0 - lower left y coordinate  (double*)
//              : z0 - lower left z coordinate  (double*)
//              : x1 - upper right x coordinate (double*)
//              : y1 - upper right y coordinate (double*)
//              : z1 - upper right z coordinate (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetBlockMatchExtent(MTK_BlockModel* bmodel,
                                       double* x0, double* y0, double* z0,
                                       double* x1, double* y1, double* z1);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_NSchemas
//  Description : Number of schemas
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//  Returns     : int - number of schemas
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_NSchemas(MTK_BlockModel* bmodel);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetSchemaExtent
//  Description : Get specific schema extent
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : nsch   - schema index to query (int)
//              : x0 - lower left x coordinate  (double*)
//              : y0 - lower left y coordinate  (double*)
//              : z0 - lower left z coordinate  (double*)
//              : x1 - upper right x coordinate (double*)
//              : y1 - upper right y coordinate (double*)
//              : z1 - upper right z coordinate (double*)
//  Returns     : 0      - successful
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetSchemaExtent(MTK_BlockModel* bmodel,
                                   const int& nsch,
                                   double* x0, double* y0, double* z0,
                                   double* x1, double* y1, double* z1);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_BlockModel_GetSchemaSizes
//  Description : Get specific schema sizes (min/max)
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : nsch   - schema index to query (int)
//              : minx, miny, minz - lower limit (double*)
//              : maxx, maxy, maxz - upper limit (double*)
//  Returns     : 0      - successful
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetSchemaSizes(MTK_BlockModel* bmodel,
                                   const int& nsch,
                                   double* minx, double* miny, double* minz,
                                   double* maxx, double* maxy, double* maxz);

//////////////////////////////////////////////////////////////////////////////
//
//  Name        : MTK_BlockModel_GetSchemaDimensions
//  Description : Get dimensions of a schema
//  Parameters  : bmodel - pointer to an MTK_BlockModel struct
//              : nsch   - schema index to query (int)
//              : ix, iy, iz - the number of blocks in each direction (double*)
//  Returns     : 0      - successful
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_BlockModel_GetSchemaDimensions(MTK_BlockModel* bmodel,
                                       const int& nsch,
                                       double* ix, double* iy, double* iz);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_New
//  Description : Create a blank triangulation that values can be set to
//              :    Note: this does not create an actual file
//  Parameters  : None
//  Returns     : Pointer to an empty MTK_Triangulation struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Triangulation* MTK_Triangulation_New();

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_Open
//  Description : Open triangulation file
//  Parameters  : name - triangulation filename (const char*)
//  Returns     : Pointer to an empty MTK_Triangulation struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Triangulation* MTK_Triangulation_Open(const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_Close
//  Description : Close triangulation.  Release any memory used.
//              :    Note: this does not save any changes
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_Close(MTK_Triangulation* triang);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_Save
//  Description : Save the triangulation to a file
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - triangulation name (const char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_Save(MTK_Triangulation* triang, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_IsValid
//  Description : Query current object state
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//  Returns     : 1  - valid
//              : 0  - invalid
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_IsValid(MTK_Triangulation* triang);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetColour
//  Description : Get colour of triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : r - red   (int*)
//              : g - green (int*)
//              : b - blue  (int*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetColour(MTK_Triangulation* triang,
                                int* r, int* g, int* b);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_SetColour
//  Description : Set the colour of the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : r - red   (int)
//              : g - green (int)
//              : b - blue  (int)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_SetColour(MTK_Triangulation* triang,
                                const int& r, const int& g, const int& b);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_IsTranslucent
//  Description : Determine if translucency is on or off
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//  Returns     : 1  - translucency enabled
//              : 0  - translucency not enabled
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_IsTranslucent(MTK_Triangulation* triang);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetTranslucency
//  Description : Get the translucency value
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//  Returns     : double - translucency on a percentage scale
//              : -1     - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
double MTK_Triangulation_GetTranslucency(MTK_Triangulation* triang);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_SetTranslucency
//  Description : Set the translucency of the triangulation
//  Parameters  : triang  - pointer to an MTK_Triangulation struct
//              : value   - the percentage of how translucent the
//              :           triangulation is
//              : enabled - whether translucency is on or off
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_SetTranslucency(MTK_Triangulation* triang,
                                      const double& value, const int& enabled);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_IsTextured
//  Description : Determine if triangulation texture is enabled
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//  Returns     : 1  - Texture is enabled
//              : 0  - Texture is not enabled
//              : -1 - failed
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_IsTextured(MTK_Triangulation* triang);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetTexture
//  Description : Get the texture for the triangulation
//  Parameters  : triang  - pointer to an MTK_Triangulation struct
//              : texture - buffer of size MTK_BUFFER_SIZE to receive the path
//              :           of the texture
//  Returns     : 0     - success
//              : other - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetTexture(MTK_Triangulation* triang, char* texture);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_SetTexture
//  Description : Set the texture of the triangulation
//  Parameters  : triang  - pointer to an MTK_Triangulation struct
//              : texture - buffer of the path of the texture to use for the
//              :           triangulation
//              : enabled - whether to turn the texture on or off
//  Returns     : 0     - success
//              : other - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_SetTexture(MTK_Triangulation* triang,
                                 const char* texture, const int& enabled);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_NumPoints
//  Description : Get number of points in triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//  Returns     : int - number of triangulation points (int)
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_NumPoints(MTK_Triangulation* triang);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetPoint
//  Description : Get point located at specified index
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : i - point index   (int)
//              : x - point x value (double*)
//              : y - point y value (double*)
//              : z - point z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetPoint(MTK_Triangulation* triang,
                               const int& i, double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddPoint
//  Description : Add the next point in the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : x - point x value (double)
//              : y - point y value (double)
//              : z - point z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddPoint(MTK_Triangulation* triang,
                               const double& x,
                               const double& y,
                               const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_NumTriangles
//  Description : Get number of triangles in triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//  Returns     : int - number of triangulation triangles (int)
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_NumTriangles(MTK_Triangulation* triang);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetTriangle
//  Description : Get triangle point index at specified index
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : i  - triangle index (int)
//              : p0 - point 0 index  (int*)
//              : p1 - point 1 index  (int*)
//              : p2 - point 2 index  (int*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetTriangle(MTK_Triangulation* triang,
                                  const int& i, int* p0, int* p1, int* p2);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddTriangle
//  Description : Add the next triangle in the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : p0 - point 0 index (int)
//              : p1 - point 1 index (int)
//              : p2 - point 2 index (int)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddTriangle(MTK_Triangulation* triang,
                                  const int& p0, const int& p1, const int& p2);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeNames
//  Description : Gets a list of current attribute names in the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : names  - buffer of size MTK_BUFFER_SIZE * MTK_BUFFER_SIZE to
//                         receive the attribute name list (char**)
//              : count  - number of attribute names returned in buffer (int*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeNames(MTK_Triangulation* triang,
                                        char** names, int* count);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_DeleteAttribute
//  Description : Delete the attribute with given name
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_DeleteAttribute(MTK_Triangulation* triang,
                                      const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeInt32
//  Description : Add a 32-bit integer attribute to the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : value  - value of the added attribute (int)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeInt32(MTK_Triangulation* triang,
                                        const char* name, const int& value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeInt64
//  Description : Add a 64-bit integer attribute to the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : value  - value of the added attribute (long long)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeInt64(MTK_Triangulation* triang,
                                        const char* name,
                                        const long long& value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeBoolean
//  Description : Add a boolean attribute to the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : value  - value of the added attribute (int)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeBoolean(MTK_Triangulation* triang,
                                          const char* name, const int& value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeDouble
//  Description : Add a double attribute to the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : value  - value of the added attribute (double)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeDouble(MTK_Triangulation* triang,
                                         const char* name, const double& value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeString
//  Description : Add a string attribute to the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : value  - value of the added attribute (char*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeString(MTK_Triangulation* triang,
                                         const char* name, const char* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeDateYMD
//  Description : Add a YYYY-MM-DD formatted date attribute to triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : year   - year of the added attribute (int)
//              : month  - month of the added attribute (int)
//              : day    - day of the added attribute (int)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeDateYMD(MTK_Triangulation* triang,
                                          const char* name, const int& year,
                                          const int& month, const int& day);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeDateMDY
//  Description : Add a MM-DD-YYYY formatted date attribute to triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : year   - year of the added attribute (int)
//              : month  - month of the added attribute (int)
//              : day    - day of the added attribute (int)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeDateMDY(MTK_Triangulation* triang,
                                          const char* name, const int& year,
                                          const int& month, const int& day);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_AddAttributeDateDMY
//  Description : Add a MM-DD-YYYY formatted date attribute to triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to be added (char*)
//              : year   - year of the added attribute (int)
//              : month  - month of the added attribute (int)
//              : day    - day of the added attribute (int)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_AddAttributeDateDMY(MTK_Triangulation* triang,
                                          const char* name, const int& year,
                                          const int& month, const int& day);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeType
//  Description : Get the specified attributes type
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to get (char*)
//              : type   - attribute type (MTK_Triangulation_AttributeType*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeType(MTK_Triangulation* triang,
                                       const char* name,
                                       MTK_Triangulation_AttributeType* type);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeAsInt32
//  Description : Get the attribute with specified name from the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to get (char*)
//              : value  - value of the attribute (int*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeAsInt32(MTK_Triangulation* triang,
                                          const char* name, int* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeAsInt64
//  Description : Get the attribute with specified name from the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to get (char*)
//              : value  - value of the attribute (long long*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeAsInt64(MTK_Triangulation* triang,
                                          const char* name, long long* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeAsBoolean
//  Description : Get the attribute with specified name from the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to get (char*)
//              : value  - value of the attribute (int*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeAsBoolean(MTK_Triangulation* triang,
                                            const char* name, int* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeAsDouble
//  Description : Get the attribute with specified name from the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to get (char*)
//              : value  - value of the attribute (double*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeAsDouble(MTK_Triangulation* triang,
                                           const char* name, double* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeAsString
//  Description : Get the attribute with specified name from the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to get (char*)
//              : value  - value of the attribute (char*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeAsString(MTK_Triangulation* triang,
                                           const char* name, char* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Triangulation_GetAttributeAsDate
//  Description : Get the attribute with specified name from the triangulation
//  Parameters  : triang - pointer to an MTK_Triangulation struct
//              : name   - name of the attribute to get (char*)
//              : year   - year of the attribute (int*)
//              : month  - month of the attribute (int*)
//              : day    - day of the attribute (int*)
//  Returns     : 0      - success
//              : other  - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Triangulation_GetAttributeAsDate(MTK_Triangulation* triang,
                                         const char* name, int* year,
                                         int* month, int* day);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_New
//  Description : Create a blank MTK_Object that values can be set to
//  Parameters  : None
//  Returns     : Pointer to an empty MTK_Object struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Object* MTK_Object_New();

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Close
//  Description : Clean up the object.
//  Parameters  : object - pointer to an MTK_Object struct
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Close(MTK_Object* object);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetName
//  Description : Get the name of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : name  - object name (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetName(MTK_Object* obj, char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetGroup
//  Description : Get the group of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : group - object group (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetGroup(MTK_Object* obj, char* group);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetDescription
//  Description : Get the name of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : desc  - object description (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetDescription(MTK_Object* obj, char* desc);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetColour
//  Description : Get the colour of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : r     - red   (int*)
//              : g     - green (int*)
//              : b     - blue  (int*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetColour(MTK_Object* obj,
                         int* r, int* g, int* b);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetColourIndex
//  Description : Get the colour index of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : index - object colour index (int*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetColourIndex(MTK_Object* obj, int* index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetValue
//  Description : Get the value of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : value - the number (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetValue(MTK_Object* obj, double* value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetType
//  Description : Get the type of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : type  - object type (MTK_ObjectType*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetType(MTK_Object* obj, MTK_ObjectType* type);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetFeature
//  Description : Get the feature name of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : feature  - object feature (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetFeature(MTK_Object* obj, char* feature);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_GetPrimitive
//  Description : Get the primtive name of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : primitive  - object primitive (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_GetPrimitive(MTK_Object* obj, char* primitive);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetName
//  Description : Set the name of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : name  - object name (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetName(MTK_Object* obj, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetGroup
//  Description : Set the group of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : group - object group (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetGroup(MTK_Object* obj, const char* group);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetDescription
//  Description : Set the description of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : desc  - object description (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetDescription(MTK_Object* obj, const char* desc);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetColour
//  Description : Set the colour of an object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : colour - object colour (const int)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetColour(MTK_Object* obj, const int& colour);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetValue
//  Description : Set the value of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : value - the value to be assigned (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetValue(MTK_Object* obj, const double& value);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetType
//  Description : Set the type of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : type  - object type (MTK_ObjectType)
//  Note        : This will clear the object completely.  This function should
//              :    be called sparingly, usually immediately after
//              :    MTK_Object_New.
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetType(MTK_Object* obj, const MTK_ObjectType& type);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetFeature
//  Description : Set the feature of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : group - object feature (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetFeature(MTK_Object* obj, const char* feature);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_SetPrimitive
//  Description : Set the primitive of an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : group - object primitive (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_SetPrimitive(MTK_Object* obj, const char* primitive);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_NPoints
//  Description : Get the number of points in a polyline object
//  Parameters  : obj - pointer to an MTK_Object struct
//  Returns     : int - number of points in the object
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_NPoints(MTK_Object* obj);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_GetAttributes
//  Description : Get the polyline attributes of a polyline object
//  Parameters  : obj       - pointer to an MTK_Object struct
//              : linetype  - the linetype of the object (int*)
//              :    Note: linetypes documented at
//              :       MTK_Object_Polyline_GetLineType
//              : pattern   - the pattern of the object (int*)
//              : clockwise - whether the object is clockwise or not (int*)
//              : closed    - whether the object is closed or not (int*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_GetAttributes(MTK_Object* obj,
                                      int* linetype, int* pattern,
                                      int* clockwise, int* closed);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_GetPointInfo
//  Description : Get the information about a point in a polyline object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : index - the index of the point (int)
//              : x     - the x coordinate of the point (double*)
//              : y     - the y coordinate of the point (double*)
//              : z     - the z coordinate of the point (double*)
//              : w     - the w value of the point (double*)
//              : t     - the type of the point (int*)
//              :    Note: points types documented at
//              :       MTK_Object_Polyline_GetPointType
//              : name  - the name of the point, if any (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_GetPointInfo(MTK_Object* obj, const int& index,
                                     double* x, double* y, double* z,
                                     double* w, int* t, char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_GetPointType
//  Description : Get the type of a point in a polyline object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : index - the index of the point (int)
//              : t     - the type of the point  (int*)
//  Returns     : 0     - success
//              : other - failure
//
//  Possible Point Types : 0 - "move"  - no line is drawn from this point
//                       : 1 - "draw"  - a line is drawn from this point
//                       : 2 - "point" - this point is unconnected to any
//                       :                  other point
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_GetPointType(MTK_Object* obj, const int& index, int* t);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_GetLineType
//  Description : Get the type (style and thickness) of the line of a
//              :    polyline object.
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : type  - the type of line (int*)
//              :         style is different dashing styles from 0 to 7
//              :         thickness is different line thicknesses from 1 to 10
//  Returns     : 0     - success
//              : other - failure
//
//  Possible Line Types : style + (thickness-1)*10
//     style ranges from 0 to 7
//     thickness ranges from 1 to 10
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_GetLineType(MTK_Object* obj, int* type);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_SetAttributes
//  Description : Set the polyline attributes of a polyline object
//              : Passing a -1 will skip setting that attribute
//  Parameters  : obj       - pointer to an MTK_Object struct
//              : linetype  - the linetype of the object             (int)
//              :    Note: linetypes documented at
//              :       MTK_Object_Polyline_GetLineType
//              : pattern   - the pattern of the object              (int)
//              : clockwise - whether the object is clockwise or not (int)
//              : closed    - whether the object is closed or not    (int)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_SetAttributes(MTK_Object* obj,
                                      const int& linetype, const int& pattern,
                                      const int& clockwise, const int& closed);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_SetPointInfo
//  Description : Set the information about a point in a polyline object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : index - the index of the point        (int)
//              : x     - the x coordinate of the point (double)
//              : y     - the y coordinate of the point (double)
//              : z     - the z coordinate of the point (double)
//              : w     - the w value of the point      (double)
//              : t     - the type of the point         (int)
//              :    Note: points types documented at
//              :       MTK_Object_Polyline_GetPointType
//              : name  - the name of the point (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_SetPointInfo(MTK_Object* obj, const int& index,
                       const double& x, const double& y, const double& z,
                       const double& w, const int& t, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_SetPointType
//  Description : Set the type of a point in a polyline object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : index - the index of the point (int)
//              : t     - the type of the point  (int)
//  Returns     : 0     - success
//              : other - failure
//
//  Possible Point Types : 0 - "move"  - no line is drawn from this point
//                       : 1 - "draw"  - a line is drawn from this point
//                       : 2 - "point" - this point is unconnected to any
//                       :                  other point
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_SetPointType(MTK_Object* obj,
                                     const int& index, const int& t);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_SetLineType
//  Description : Set the type of the line of a polyline object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : type  - the type of line (int)
//  Returns     : 0     - success
//              : other - failure
//
//  Possible Line Types : style + (thickness-1)*10
//     style ranges from 0 to 7
//     thickness ranges from 1 to 10
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_SetLineType(MTK_Object* obj, const int& type);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_AppendPoint
//  Description : Append a point to an object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - the x coordinate of the point (double)
//              : y     - the y coordinate of the point (double)
//              : z     - the z coordinate of the point (double)
//              : w     - the w value of the point      (double)
//              : t     - the type of the point         (int)
//              :    Note: points types documented at
//              :       MTK_Object_Polyline_GetPointType
//              : name  - the name of the point         (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_AppendPoint(MTK_Object* obj,
                                    const double& x, const double& y,
                                    const double& z, const double& w,
                                    const int& t, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_DeletePoint
//  Description : Delete a point in a polyline object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : index - the index of the point (int)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_DeletePoint(MTK_Object* obj, const int& index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Polyline_DeleteAllPoints
//  Description : Delete all points in a polyline object
//  Parameters  : obj   - pointer to an MTK_Object struct
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Polyline_DeleteAllPoints(MTK_Object* obj);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_NRecords
//  Description : Get the number of records in a text object
//  Parameters  : obj - pointer to an MTK_Object struct
//  Returns     : int - number of records (lines) in the object
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_NRecords(MTK_Object* obj);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_GetRecord
//  Description : Get a specific record (line) of a text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : index  - the index of the desired record (int)
//              : record - the record at the index (char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_GetRecord(MTK_Object* obj, const int& index, char* record);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_SetRecord
//  Description : Set a specific record (line) of a text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : index  - the desired index of the record (int)
//              : record - the record (const char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_SetRecord(MTK_Object* obj, const int& index,
                              const char* record);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_AppendRecord
//  Description : Append a record (line) to a text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : record - the record (const char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_AppendRecord(MTK_Object* obj, const char* record);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_DeleteRecord
//  Description : Delete a specific record (line) of a text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : index  - the index of the record to delete (int)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_DeleteRecord(MTK_Object* obj, const int& index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_DeleteAllRecords
//  Description : Delete all records (lines) of a text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_DeleteAllRecords(MTK_Object* obj);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_GetOrigin
//  Description : Get the origin of a text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double*)
//              : y     - origin y value (double*)
//              : z     - origin z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_GetOrigin(MTK_Object* obj,
                              double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_GetAttributes
//  Description : Get the attributes of a text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - font of the text   (char*)
//              : scale  - scale of the text  (char*)
//              : angle  - angle of the text  (double*)
//              : height - height of the text (double*)
//              : width  - width of the text  (double*)
//              : boxed  - whether the text is boxed or not (int*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_GetAttributes(MTK_Object* obj,
                                  char* font, char* scale, double* angle,
                                  double* height, double* width, int* boxed);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_SetOrigin
//  Description : Set the origin of a text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double)
//              : y     - origin y value (double)
//              : z     - origin z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_SetOrigin(MTK_Object* obj,
                              const double& x,
                              const double& y,
                              const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Text_SetAttributes
//  Description : Set the attributes of a text object
//              : Passing a -1 or NULL will skip setting that attribute
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - font of the text   (const char*)
//              : scale  - scale of the text  (const char*)
//              : angle  - angle of the text  (double)
//              : height - height of the text (double)
//              : width  - width of the text  (double)
//              : boxed  - whether the text is boxed or not (int)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Text_SetAttributes(MTK_Object* obj,
                                  const char* font, const char* scale,
                                  const double& angle, const double& height,
                                  const double& width, const int& boxed);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_NRecords
//  Description : Get the number of records in a 3D text object
//  Parameters  : obj - pointer to an MTK_Object struct
//  Returns     : int - number of records (lines) in the object
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_NRecords(MTK_Object* obj);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_GetRecord
//  Description : Get a specific record (line) of a 3D text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : index  - the index of the desired record (int)
//              : record - the record at the index (char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_GetRecord(MTK_Object* obj,
                                const int& index, char* record);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_SetRecord
//  Description : Set a specific record (line) of a 3D text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : index  - the desired index of the record (int)
//              : record - the record (const char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_SetRecord(MTK_Object* obj, const int& index,
                                const char* record);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_AppendRecord
//  Description : Append a record (line) to a 3D text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : record - the record (const char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_AppendRecord(MTK_Object* obj, const char* record);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_DeleteRecord
//  Description : Delete a specific record (line) of a 3D text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : index  - the index of the record to delete (int)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_DeleteRecord(MTK_Object* obj, const int& index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_DeleteAllRecords
//  Description : Delete all records (lines) of a 3D text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_DeleteAllRecords(MTK_Object* obj);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_GetOrigin
//  Description : Get the origin of a 3D text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double*)
//              : y     - origin y value (double*)
//              : z     - origin z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_GetOrigin(MTK_Object* obj,
                                double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_GetOriginPosition
//  Description : Get the position of the origin relative to the rest of the
//              :    3D text object
//  Parameters  : obj     - pointer to an MTK_Object struct
//              : horiPos - horizontal position of the origin
//              :           (MTK_Object_TextHoriPos*)
//              : vertPos - vertical position of the origin
//              :           (MTK_Object_TextVertPos*)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_GetOriginPosition(MTK_Object* obj,
                                        MTK_Object_TextHoriPos* horiPos,
                                        MTK_Object_TextVertPos* vertPos);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_GetDirection
//  Description : Get the direction of a 3D text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - direction x value (double*)
//              : y     - direction y value (double*)
//              : z     - direction z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_GetDirection(MTK_Object* obj,
                                   double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_GetNormal
//  Description : Get the normal of a 3D text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double*)
//              : y     - normal y value (double*)
//              : z     - normal z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_GetNormal(MTK_Object* obj,
                                double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_GetAttributes
//  Description : Get the attributes of a 3D text object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - font of the text   (char*)
//              : scale  - scale of the text  (char*)
//              : angle  - angle of the text  (double*)
//              : height - height of the text (double*)
//              : width  - width of the text  (double*)
//              : italic - whether the text is in italics or not (int*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_GetAttributes(MTK_Object* obj,
                                    char* font, char* scale,
                                    double* height, double* width,
                                    int* italic);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_SetOrigin
//  Description : Set the origin of a 3D text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double)
//              : y     - origin y value (double)
//              : z     - origin z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_SetOrigin(MTK_Object* obj,
                                const double& x,
                                const double& y,
                                const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_SetOriginPosition
//  Description : Set the position of the origin relative to the rest of the
//              :    3D text object
//  Parameters  : obj     - pointer to an MTK_Object struct
//              : horiPos - horizontal position of the origin
//              :           (MTK_Object_TextHoriPos)
//              : vertPos - vertical position of the origin
//              :           (MTK_Object_TextVertPos)
//  Returns     : 0       - success
//              : other   - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_SetOriginPosition(MTK_Object* obj,
                                 const MTK_Object_TextHoriPos& horiPos,
                                 const MTK_Object_TextVertPos& vertPos);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_SetDirection
//  Description : Set the direction of a 3D text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - direction x value (double)
//              : y     - direction y value (double)
//              : z     - direction z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_SetDirection(MTK_Object* obj,
                                   const double& x,
                                   const double& y,
                                   const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_SetNormal
//  Description : Set the normal of a 3D text object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double)
//              : y     - normal y value (double)
//              : z     - normal z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_SetNormal(MTK_Object* obj,
                                const double& x,
                                const double& y,
                                const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_3DText_SetAttributes
//  Description : Set the attributes of a 3D text object
//              : Passing a -1 or NULL will skip setting that attribute
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - font of the text   (const char*)
//              : scale  - scale of the text  (const char*)
//              : angle  - angle of the text  (double)
//              : height - height of the text (double)
//              : width  - width of the text  (double)
//              : italic - whether the text is in italics or not (int)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_3DText_SetAttributes(MTK_Object* obj,
                                    const char* font, const char* scale,
                                    const double& height, const double& width,
                                    const int& italic);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_GetStart
//  Description : Get the start location of an arrow object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - start x value (double*)
//              : y     - start y value (double*)
//              : z     - start z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_GetStart(MTK_Object* obj,
                              double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_GetEnd
//  Description : Get the end location of an arrow object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - end x value (double*)
//              : y     - end y value (double*)
//              : z     - end z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_GetEnd(MTK_Object* obj, double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_GetNormal
//  Description : Get the normal of an arrow object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double*)
//              : y     - normal y value (double*)
//              : z     - normal z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_GetNormal(MTK_Object* obj,
                               double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_GetAttributes
//  Description : Get the attributes of an arrow object
//  Parameters  : obj       - pointer to an MTK_Object struct
//              : autoscale - whether or not the arrow autoscales (int*)
//              : filled    - whether or not the arrow is filled  (int*)
//              : numfacets - the number of facets in the arrow   (int*)
//              : arrowtype - the type of the arrow (MTK_Object_ArrowType*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_GetAttributes(MTK_Object* obj,
                                   int* autoscale, int* filled, int* numfacets,
                                   MTK_Object_ArrowType* arrowtype);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_GetHeadInfo
//  Description : Get the length/width of the head of an arrow object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : length - head length value (double*)
//              : width  - head width value  (double*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_GetHeadInfo(MTK_Object* obj,
                                 double* length, double* width);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_GetRelativeHeadInfo
//  Description : Get the relative length/width of the head of an arrow object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : length - relative head length value (double*)
//              : width  - relative head width value  (double*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_GetRelativeHeadInfo(MTK_Object* obj,
                                         double* length, double* width);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_GetScaledHeadInfo
//  Description : Get the scaled length/width of the head of an arrow object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : length - scaled head length value (double*)
//              : width  - scaled head width value  (double*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_GetScaledHeadInfo(MTK_Object* obj,
                                       double* length, double* width);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_SetStart
//  Description : Set the start location of an arrow object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - start x value (double)
//              : y     - start y value (double)
//              : z     - start z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_SetStart(MTK_Object* obj,
                              const double& x,
                              const double& y,
                              const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_SetEnd
//  Description : Set the end location of an arrow object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - end x value (double)
//              : y     - end y value (double)
//              : z     - end z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_SetEnd(MTK_Object* obj,
                            const double& x, const double& y, const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_SetNormal
//  Description : Set the normal of an arrow object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double)
//              : y     - normal y value (double)
//              : z     - normal z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_SetNormal(MTK_Object* obj,
                               const double& x,
                               const double& y,
                               const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_SetAttributes
//  Description : Set the attributes of an arrow object
//  Parameters  : obj       - pointer to an MTK_Object struct
//              : autoscale - whether or not the arrow autoscales (int)
//              : filled    - whether or not the arrow is filled  (int)
//              : numfacets - the number of facets in the arrow   (int)
//              : arrowtype - the type of the arrow (MTK_Object_ArrowType)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_SetAttributes(MTK_Object* obj, const int& autoscale,
                                   const int& filled, const int& numfacets,
                                   const MTK_Object_ArrowType& arrowtype);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_SetHeadInfo
//  Description : Set the length/width of the head of an arrow object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : length - head length value (double)
//              : width  - head width value  (double)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_SetHeadInfo(MTK_Object* obj,
                                 const double& length, const double& width);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_Arrow_SetRelativeHeadInfo
//  Description : Set the relative length/width of the head of an arrow object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : length - relative head length value (double)
//              : width  - relative head width value  (double)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_Arrow_SetRelativeHeadInfo(MTK_Object* obj,
                                         const double& length,
                                         const double& width);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_GetRadius
//  Description : Get the radius location of a dimension angle object
//              :    (the start of the angle)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - radius x value (double*)
//              : y     - radius y value (double*)
//              : z     - radius z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_GetRadius(MTK_Object* obj,
                                        double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_GetSweep
//  Description : Get the sweep location of a dimension angle object
//              :    (the end of the angle)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - sweep x value (double*)
//              : y     - sweep y value (double*)
//              : z     - sweep z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_GetSweep(MTK_Object* obj,
                                       double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_GetOrigin
//  Description : Get the origin location of a dimension angle object
//              :    (the centre of the angle)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double*)
//              : y     - origin y value (double*)
//              : z     - origin z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_GetOrigin(MTK_Object* obj,
                                        double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_GetAttributes
//  Description : Get the attributes of a dimension angle object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension angle (char*)
//              : tickGap        - the tick gap (double*)
//              : tickOffset     - the tick offset (double*)
//              : tickExtentSize - the tick extent size (double*)
//              : arrowLength    - the length of the arrow head (double*)
//              : arrowWidth     - the width of the arrow head (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_GetAttributes(MTK_Object* obj,
                                 char* label, double* tickGap,
                                 double* tickOffset, double* tickExtentSize,
                                 double* arrowLength, double* arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_GetTextAttributes
//  Description : Get the attributes of the text on a dimension angle object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension angle
//              :          (char*)
//              : scale  - the scale of the text on the dimension angle
//              :          (char*)
//              : height - the height of the text on the dimension angle
//              :          (double*)
//              : textLocation - the location of the text relative to the
//              :                dimension angle (MTK_Object_TextVertPos*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_GetTextAttributes(MTK_Object* obj,
                                       char* font, char* scale,
                                       double* height,
                                       MTK_Object_TextVertPos* textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_SetRadius
//  Description : Set the radius location of a dimension angle object
//              :    (the start of the angle)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - radius x value (double)
//              : y     - radius y value (double)
//              : z     - radius z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_SetRadius(MTK_Object* obj,
                                        const double& x,
                                        const double& y,
                                        const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_SetSweep
//  Description : Set the sweep location of a dimension angle object
//              :    (the end of the angle)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - sweep x value (double)
//              : y     - sweep y value (double)
//              : z     - sweep z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_SetSweep(MTK_Object* obj,
                                       const double& x,
                                       const double& y,
                                       const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_SetOrigin
//  Description : Set the origin location of a dimension angle object
//              :    (the centre of the angle)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double)
//              : y     - origin y value (double)
//              : z     - origin z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_SetOrigin(MTK_Object* obj,
                                        const double& x,
                                        const double& y,
                                        const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_SetAttributes
//  Description : Set the attributes of a dimension angle object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension angle
//                                 (const char*)
//              : tickGap        - the tick gap (double)
//              : tickOffset     - the tick offset (double)
//              : tickExtentSize - the tick extent size (double)
//              : arrowLength    - the length of the arrow head (double)
//              : arrowWidth     - the width of the arrow head (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_SetAttributes(MTK_Object* obj,
                     const char* label, const double& tickGap,
                     const double& tickOffset, const double& tickExtentSize,
                     const double& arrowLength, const double& arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionAngle_SetTextAttributes
//  Description : Set the attributes of the text on a dimension angle object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension angle
//              :          (const char*)
//              : scale  - the scale of the text on the dimension angle
//              :          (const char*)
//              : height - the height of the text on the dimension angle
//              :          (double)
//              : textLocation - the location of the text relative to the
//              :                dimension angle (MTK_Object_TextVertPos)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionAngle_SetTextAttributes(MTK_Object* obj,
                                 const char* font, const char* scale,
                                 const double& height,
                                 const MTK_Object_TextVertPos& textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_GetRadius
//  Description : Get the radius location of a dimension arc object
//              :    (the start of the arc)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - radius x value (double*)
//              : y     - radius y value (double*)
//              : z     - radius z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_GetRadius(MTK_Object* obj,
                                      double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_GetSweep
//  Description : Get the sweep location of a dimension arc object
//              :    (the end of the arc)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - sweep x value (double*)
//              : y     - sweep y value (double*)
//              : z     - sweep z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_GetSweep(MTK_Object* obj,
                                     double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_GetOrigin
//  Description : Get the origin location of a dimension arc object
//              :    (the centre of the arc)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double*)
//              : y     - origin y value (double*)
//              : z     - origin z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_GetOrigin(MTK_Object* obj,
                                      double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_GetAttributes
//  Description : Get the attributes of a dimension arc object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension arc (char*)
//              : tickGap        - the tick gap (double*)
//              : tickOffset     - the tick offset (double*)
//              : tickExtentSize - the tick extent size (double*)
//              : arrowLength    - the length of the arrow head (double*)
//              : arrowWidth     - the width of the arrow head (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_GetAttributes(MTK_Object* obj,
                                 char* label, double* tickGap,
                                 double* tickOffset, double* tickExtentSize,
                                 double* arrowLength, double* arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_GetTextAttributes
//  Description : Get the attributes of the text on a dimension arc object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension arc
//              :          (char*)
//              : scale  - the scale of the text on the dimension arc
//              :          (char*)
//              : height - the height of the text on the dimension arc
//              :          (double*)
//              : textLocation - the location of the text relative to the
//              :                dimension arc (MTK_Object_TextVertPos*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_GetTextAttributes(MTK_Object* obj,
                                       char* font, char* scale,
                                       double* height,
                                       MTK_Object_TextVertPos* textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_SetRadius
//  Description : Set the radius location of a dimension arc object
//              :    (the start of the arc)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - radius x value (double)
//              : y     - radius y value (double)
//              : z     - radius z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_SetRadius(MTK_Object* obj,
                                      const double& x,
                                      const double& y,
                                      const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_SetSweep
//  Description : Set the sweep location of a dimension arc object
//              :    (the end of the arc)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - sweep x value (double)
//              : y     - sweep y value (double)
//              : z     - sweep z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_SetSweep(MTK_Object* obj,
                                     const double& x,
                                     const double& y,
                                     const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_SetOrigin
//  Description : Set the origin location of a dimension arc object
//              :    (the centre of the arc)
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - origin x value (double)
//              : y     - origin y value (double)
//              : z     - origin z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_SetOrigin(MTK_Object* obj,
                                      const double& x,
                                      const double& y,
                                      const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_SetAttributes
//  Description : Set the attributes of a dimension arc object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension arc (const char*)
//              : tickGap        - the tick gap (double)
//              : tickOffset     - the tick offset (double)
//              : tickExtentSize - the tick extent size (double)
//              : arrowLength    - the length of the arrow head (double)
//              : arrowWidth     - the width of the arrow head (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_SetAttributes(MTK_Object* obj,
                     const char* label, const double& tickGap,
                     const double& tickOffset, const double& tickExtentSize,
                     const double& arrowLength, const double& arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionArc_SetTextAttributes
//  Description : Set the attributes of the text on a dimension arc object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension arc
//              :          (const char*)
//              : scale  - the scale of the text on the dimension arc
//              :          (const char*)
//              : height - the height of the text on the dimension arc
//              :          (double)
//              : textLocation - the location of the text relative to the
//              :                dimension arc (MTK_Object_TextVertPos)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionArc_SetTextAttributes(MTK_Object* obj,
                                 const char* font, const char* scale,
                                 const double& height,
                                 const MTK_Object_TextVertPos& textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_GetStart
//  Description : Get the start location of a dimension line object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - start x value (double*)
//              : y     - start y value (double*)
//              : z     - start z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_GetStart(MTK_Object* obj,
                                      double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_GetEnd
//  Description : Get the end location of a dimension line object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - end x value (double*)
//              : y     - end y value (double*)
//              : z     - end z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_GetEnd(MTK_Object* obj,
                                    double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_GetNormal
//  Description : Get the normal of a dimension line object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double*)
//              : y     - normal y value (double*)
//              : z     - normal z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_GetNormal(MTK_Object* obj,
                                       double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_GetAttributes
//  Description : Get the attributes of a dimension line object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension line (char*)
//              : tickGap        - the tick gap (double*)
//              : tickOffset     - the tick offset (double*)
//              : tickExtentSize - the tick extent size (double*)
//              : arrowLength    - the length of the arrow head (double*)
//              : arrowWidth     - the width of the arrow head (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_GetAttributes(MTK_Object* obj,
                                 char* label, double* tickGap,
                                 double* tickOffset, double* tickExtentSize,
                                 double* arrowLength, double* arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_GetTextAttributes
//  Description : Get the attributes of the text on a dimension line object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension line
//              :          (char*)
//              : scale  - the scale of the text on the dimension line
//              :          (char*)
//              : height - the height of the text on the dimension line
//              :          (double*)
//              : textLocation - the location of the text relative to the
//              :                dimension line (MTK_Object_TextVertPos*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_GetTextAttributes(MTK_Object* obj,
                                       char* font, char* scale,
                                       double* height,
                                       MTK_Object_TextVertPos* textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_SetStart
//  Description : Set the start location of a dimension line object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - start x value (double)
//              : y     - start y value (double)
//              : z     - start z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_SetStart(MTK_Object* obj,
                                      const double& x,
                                      const double& y,
                                      const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_SetEnd
//  Description : Set the end location of a dimension line object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - end x value (double)
//              : y     - end y value (double)
//              : z     - end z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_SetEnd(MTK_Object* obj,
                                    const double& x,
                                    const double& y,
                                    const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_SetNormal
//  Description : Set the normal of a dimension line object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double)
//              : y     - normal y value (double)
//              : z     - normal z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_SetNormal(MTK_Object* obj,
                                       const double& x,
                                       const double& y,
                                       const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_SetAttributes
//  Description : Set the attributes of a dimension line object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension line
//                                 (const char*)
//              : tickGap        - the tick gap (double)
//              : tickOffset     - the tick offset (double)
//              : tickExtentSize - the tick extent size (double)
//              : arrowLength    - the length of the arrow head (double)
//              : arrowWidth     - the width of the arrow head (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_SetAttributes(MTK_Object* obj,
                     const char* label, const double& tickGap,
                     const double& tickOffset, const double& tickExtentSize,
                     const double& arrowLength, const double& arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionLine_SetTextAttributes
//  Description : Set the attributes of the text on a dimension line object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension line
//              :          (const char*)
//              : scale  - the scale of the text on the dimension line
//              :          (const char*)
//              : height - the height of the text on the dimension line
//              :          (double)
//              : textLocation - the location of the text relative to the
//              :                dimension line (MTK_Object_TextVertPos)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionLine_SetTextAttributes(MTK_Object* obj,
                                 const char* font, const char* scale,
                                 const double& height,
                                 const MTK_Object_TextVertPos& textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_GetStart
//  Description : Get the start location of a dimension radius object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - start x value (double*)
//              : y     - start y value (double*)
//              : z     - start z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_GetStart(MTK_Object* obj,
                                        double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_GetEnd
//  Description : Get the end location of a dimension radius object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - end x value (double*)
//              : y     - end y value (double*)
//              : z     - end z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_GetEnd(MTK_Object* obj,
                                      double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_GetNormal
//  Description : Get the normal of a dimension radius object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double*)
//              : y     - normal y value (double*)
//              : z     - normal z value (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_GetNormal(MTK_Object* obj,
                                         double* x, double* y, double* z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_GetAttributes
//  Description : Get the attributes of a dimension radius object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension radius (char*)
//              : tickGap        - the tick gap (double*)
//              : tickOffset     - the tick offset (double*)
//              : tickExtentSize - the tick extent size (double*)
//              : arrowLength    - the length of the arrow head (double*)
//              : arrowWidth     - the width of the arrow head (double*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_GetAttributes(MTK_Object* obj,
                                 char* label, double* tickGap,
                                 double* tickOffset, double* tickExtentSize,
                                 double* arrowLength, double* arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_GetTextAttributes
//  Description : Get the attributes of the text on a dimension radius object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension radius
//              :          (char*)
//              : scale  - the scale of the text on the dimension radius
//              :          (char*)
//              : height - the height of the text on the dimension radius
//              :          (double*)
//              : textLocation - the location of the text relative to the
//              :                dimension radius (MTK_Object_TextVertPos*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_GetTextAttributes(MTK_Object* obj,
                                       char* font, char* scale,
                                       double* height,
                                       MTK_Object_TextVertPos* textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_SetStart
//  Description : Set the start location of a dimension radius object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - start x value (double)
//              : y     - start y value (double)
//              : z     - start z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_SetStart(MTK_Object* obj,
                                        const double& x,
                                        const double& y,
                                        const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_SetEnd
//  Description : Set the end location of a dimension radius object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - end x value (double)
//              : y     - end y value (double)
//              : z     - end z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_SetEnd(MTK_Object* obj,
                                      const double& x,
                                      const double& y,
                                      const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_SetNormal
//  Description : Set the normal of a dimension radius object
//  Parameters  : obj   - pointer to an MTK_Object struct
//              : x     - normal x value (double)
//              : y     - normal y value (double)
//              : z     - normal z value (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_SetNormal(MTK_Object* obj,
                                         const double& x,
                                         const double& y,
                                         const double& z);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_SetAttributes
//  Description : Set the attributes of a dimension radius object
//  Parameters  : obj            - pointer to an MTK_Object struct
//              : label          - the label of the dimension radius
//                                 (const char*)
//              : tickGap        - the tick gap (double)
//              : tickOffset     - the tick offset (double)
//              : tickExtentSize - the tick extent size (double)
//              : arrowLength    - the length of the arrow head (double)
//              : arrowWidth     - the width of the arrow head (double)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_SetAttributes(MTK_Object* obj,
                     const char* label, const double& tickGap,
                     const double& tickOffset, const double& tickExtentSize,
                     const double& arrowLength, const double& arrowWidth);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Object_DimensionRadius_SetTextAttributes
//  Description : Set the attributes of the text on a dimension radius object
//  Parameters  : obj    - pointer to an MTK_Object struct
//              : font   - the font of the text on the dimension radius
//              :          (const char*)
//              : scale  - the scale of the text on the dimension radius
//              :          (const char*)
//              : height - the height of the text on the dimension radius
//              :          (double)
//              : textLocation - the location of the text relative to the
//              :                dimension radius (MTK_Object_TextVertPos)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Object_DimensionRadius_SetTextAttributes(MTK_Object* obj,
                                 const char* font, const char* scale,
                                 const double& height,
                                 const MTK_Object_TextVertPos& textLocation);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_New
//  Description : Start with a blank MTK_Layer that values can be set to
//  Parameters  : None
//  Returns     : Pointer to an empty MTK_Layer struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Layer* MTK_Layer_New();

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_Close
//  Description : Clean up the layer.
//  Parameters  : layer - pointer to an MTK_Layer struct
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_Close(MTK_Layer* layer);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_IsEdited
//  Description : Get if a layer has been edited
//  Parameters  : layer - pointer to an MTK_Layer struct
//  Returns     : 1     - edited
//              : 0     - not edited
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_IsEdited(MTK_Layer* layer);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_NObjects
//  Description : Get the number of objects in a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//  Returns     : int - number of objects in the layer
//              : -1  - failed
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_NObjects(MTK_Layer* layer);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_GetObject
//  Description : Get an object from a layer.  Allocates a new object which
//              : will need freeing with MTK_Object_Close
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : index - index of the object (int)
//  Returns     : A pointer to an MTK_Object - success
//              : NULL - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Object* MTK_Layer_GetObject(MTK_Layer* layer, const int& index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_GetName
//  Description : Get the name of a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : name  - layer name (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_GetName(MTK_Layer* layer, char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_GetDescription
//  Description : Get the description of a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : desc  - layer description (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_GetDescription(MTK_Layer* layer, char* desc);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_SetObject
//  Description : Set an object in a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : obj   - pointer to an MTK_Object struct
//              : index - index of the object (int)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_SetObject(MTK_Layer* layer, MTK_Object* obj, const int& index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_AppendObject
//  Description : Add an object to a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : obj   - pointer to an MTK_Object struct
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_AppendObject(MTK_Layer* layer, MTK_Object* obj);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_DeleteObject
//  Description : Delete an object from a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : index - index of the object (int)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_DeleteObject(MTK_Layer* layer, const int& index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_SetName
//  Description : Set the name of a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : name  - layer name (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_SetName(MTK_Layer* layer, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Layer_SetDescription
//  Description : Set the description of a layer
//  Parameters  : layer - pointer to an MTK_Layer struct
//              : desc  - layer description (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Layer_SetDescription(MTK_Layer* layer, const char* desc);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_Open
//  Description : Open database file
//  Parameters  : name - database filename (const char*)
//              : mode - file open mode
//              :     DESIGN_MODE_READONLY -  readonly
//              :     DESIGN_MODE_WRITE    -  read/write
//              :     DESIGN_MODE_CREATEIF -  read/write create if doesn't exist
//              :     DESIGN_MODE_CREATE   -  read/write create even if exists
//  Returns     : Pointer to a MTK_Database struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Database* MTK_Database_Open(const char* name, MTK_Database_OpenMode mode);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_Close
//  Description : Close database.  Release any memory used.
//  Parameters  : data  - pointer to an MTK_Database struct
//  Returns     : 0     - successful
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_Close(MTK_Database* data);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_IsOpen
//  Description : Query current database state
//  Parameters  : data  - pointer to an MTK_Database struct
//  Returns     : 0     - closed
//              : 1     - open
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_IsOpen(MTK_Database* data);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_GetName
//  Description : Get the name of a database
//  Parameters  : data  - pointer to an MTK_Database struct
//              : name  - database name (char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_GetName(MTK_Database* data, char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_NLayers
//  Description : Get the number of layers in a database
//  Parameters  : data - pointer to an MTK_Database struct
//  Returns     : int - number of layers in the database
//              : -1  - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_NLayers(MTK_Database* data);

//////////////////////////////////////////////////////////////////////////////
//
//  Name        : MTK_Database_GetLayerNameByIndex
//  Description : Get a specific layer name without loading the layer
//  Parameters  : data  - pointer to an MTK_Database struct
//              : index  - index of desired layer (const int&)
//              : name  - returned name of desired layer (char*)
//  Returns     : 0 - success
//              : other - failure
//
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_GetLayerNameByIndex(MTK_Database* data, const int& index,
                                     char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_GetLayerByName
//  Description : Get the layer with the specified name from a database.
//              : Allocates a new object which needs to be freed using
//              :    MTK_Layer_Close.
//  Parameters  : data  - pointer to an MTK_Database struct
//              : name  - name of desired layer (const char*)
//  Returns     : Pointer to an MTK_Layer struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Layer* MTK_Database_GetLayerByName(MTK_Database* data, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_GetLayerByIndex
//  Description : Get the layer with the specified index from a database.
//              : Allocates a new object which needs to be freed using
//              :    MTK_Layer_Close.
//  Parameters  : data  - pointer to an MTK_Database struct
//              : index - index of desired layer (int)
//  Returns     : Pointer to an MTK_Layer struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Layer* MTK_Database_GetLayerByIndex(MTK_Database* data, const int& index);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_LayerExists
//  Description : Find if a specific layer exists in a database
//  Parameters  : data  - pointer to an MTK_Database struct
//              : name  - name of layer (const char*)
//  Returns     : 1     - exists
//              : 0     - does not exist
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_LayerExists(MTK_Database* data, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_SaveLayer
//  Description : Save a layer in a database
//  Parameters  : data  - pointer to an MTK_Database struct
//              : layer - pointer to an MTK_Layer struct
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_SaveLayer(MTK_Database* data, MTK_Layer* layer);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_SaveLayerAs
//  Description : Save a layer in a database as a selected name
//  Parameters  : data  - pointer to an MTK_Database struct
//              : layer - pointer to an MTK_Layer struct
//              : name  - desired name of the layer (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_SaveLayerAs(MTK_Database* data, MTK_Layer* layer,
                                 const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Database_DeleteLayer
//  Description : Delete a layer from a database
//  Parameters  : data  - pointer to an MTK_Database struct
//              : name  - name of the layer (const char*)
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Database_DeleteLayer(MTK_Database* data, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_New
//  Description : Create a grid with the given structure, but with no data
//              :    Note: this does not create an actual file
//  Parameters  : lowerBoundX - X lower left corner (double)
//              : lowerBoundY - Y lower left corner (double)
//              : upperBoundX - Y upper right corner (double)
//              : upperBoundY - Y upper right corner (double)
//              : cellSizeX - grid cell size in X direction (double)
//              : cellSizeY - grid cell size in Y direction (double)
//  Returns     : Pointer to a MTK_Grid struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Grid* MTK_Grid_New(const double& lowerBoundX, const double& lowerBoundY,
                       const double& upperBoundX, const double& upperBoundY,
                       const double& cellSizeX, const double& cellSizeY);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_Open
//  Description : Open an existing grid file
//  Parameters  : name - grid filename (const char*)
//  Returns     : Pointer to a MTK_Grid struct - success
//              : Null pointer - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
MTK_Grid* MTK_Grid_Open(const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_Close
//  Description : Close grid.  Release any memory used.
//              :    Note: this does not save any changes
//  Parameters  : grid - pointer to an MTK_Grid struct
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_Close(MTK_Grid* grid);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_Save
//  Description : Save the grid to a file
//  Parameters  : grid - pointer to an MTK_Grid struct
//              : name   - grid name (const char*)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_Save(MTK_Grid* grid, const char* name);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_IsValid
//  Description : Query the validity of a grid.
//  Parameters  : grid - pointer to an MTK_Grid struct
//  Returns     : 1  - valid
//              : 0  - invalid
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_IsValid(MTK_Grid* grid);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_GetExtent
//  Description : Query the extent of a grid.
//  Parameters  : grid - pointer to an MTK_Grid struct
//              : lowerBoundX - X lower left corner (double)
//              : lowerBoundY - Y lower left corner (double)
//              : upperBoundX - Y upper right corner (double)
//              : upperBoundY - Y upper right corner (double)
//  Returns     : 1  - valid
//              : 0  - invalid
//              : -1 - failed
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_GetExtent(MTK_Grid* grid,
                       double* lowerBoundX, double* lowerBoundY,
                       double* upperBoundX, double* upperBoundY);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_GetSpacing
//  Description : Query the cellsizes of a grid.
//  Parameters  : grid - pointer to an MTK_Grid struct
//              : cellSizeX - cell size in the X direction (double)
//              : cellSizeY - cell size in the Y direction (double)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_GetSpacing(MTK_Grid* grid, double* cellSizeX, double* cellSizeY);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_NumNodes
//  Description : Query the number of nodes in each direction of a grid.
//  Parameters  : grid - pointer to an MTK_Grid struct
//              : numberInX - number of cells in the X direction (int)
//              : numberInY - number of cells in the Y direction (int)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_NumNodes(MTK_Grid* grid, int* numberInX, int* numberInY);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_GetNodeValue
//  Description : Query the value of a grid node.
//  Parameters  : grid - pointer to an MTK_Grid struct
//              : i - x node index (0+) (int)
//              : j - y node index (0+) (int)
//              : value - node value (double*)
//              : mask  - node mask value (int*)
//              :        1 - active  (not masked)
//              :        0 - inactive    (masked)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_GetNodeValue(MTK_Grid* grid, const int& i, const int& j,
                          double* value, int* mask);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_Grid_SetNodeValue
//  Description : Set the value of a grid node.
//  Parameters  : grid - pointer to an MTK_Grid struct
//              : i - x node index (0+) (int)
//              : j - y node index (0+) (int)
//              : value - node value (double)
//              : mask  - node mask value (int)
//              :        1 - active  (not masked)
//              :        0 - inactive    (masked)
//  Returns     : 0      - success
//              : other  - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_Grid_SetNodeValue(MTK_Grid* grid, const int& i, const int& j,
                          const double& value, const int& mask);

}  // extern "C"

#if __cplusplus
#include <string>

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_InitBlockApi
//  Description : C++ initialisation for block api (used internally)
//  Parameters  : mode  - the mode
//  Returns     : 0     - success
//              : other - failure
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
int MTK_API_InitBlockApi(int setupValues[]);

//////////////////////////////////////////////////////////////////////////////
//  Name        : MTK_API_SetErrorMessage
//  Description : C++ method to set error message (used internally)
//  Parameters  : message - the message
//  Returns     :
//////////////////////////////////////////////////////////////////////////////
MTK_EXPORTED_API_DLL_EXPORT
void MTK_API_SetErrorMessage(const std::string& message);

#endif

#endif
