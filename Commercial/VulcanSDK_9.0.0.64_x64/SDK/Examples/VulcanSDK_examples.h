//////////////////////////////////////////////////////////////////////////////
//
// Name        : VulcanSDK_examples.h
// Description : The header for the VulcanSDK examples
// Version     : 9.0.0.64
//
//////////////////////////////////////////////////////////////////////////////

#ifndef __VULCANSDK_EXAMPLES_H__
#define __VULCANSDK_EXAMPLES_H__

#include "mtkExportedApi_ReadWrite.h"

static void HandleError()
{
   char error[MTK_BUFFER_SIZE];
   MTK_API_GetError(error);
   printf("%s\n", error);
}

int RunTriangulationExample(char* filename);
int RunBlockModelExample(char* filename);
int RunGridExample(char* filename);
int RunDesignExample(char* filename);

#endif