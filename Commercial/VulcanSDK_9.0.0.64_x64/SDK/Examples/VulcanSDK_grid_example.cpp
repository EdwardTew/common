//////////////////////////////////////////////////////////////////////////////
//
// Name        : VulcanSDK_grid_example.cpp
// Description : A basic example of using the VulcanSDK with grids
// Version     : 9.0.0.64
//
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "VulcanSDK_examples.h"
#include "mtkExportedApi_ReadWrite.h"

int RunGridExample(char* filename)
{
   int status;
   MTK_Grid *grid;
   int numNodesX, numNodesY;
   char newFilename[MTK_BUFFER_SIZE];
   char* extension;

   printf("Initialising API...");
   MTK_API_SetCertificate("Maptek_Example.crt");

   if (MTK_API_Init("vulcan_sdk_developer"))
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   printf("Opening grid %s...", filename);
   grid = MTK_Grid_Open(filename);
   if (!grid)
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   status = MTK_Grid_NumNodes(grid, &numNodesX, &numNodesY);
   if (status)
   {
      HandleError();
      return 1;
   }
   printf("Grid %s has %d x nodes and %d y nodes\n",
          filename, numNodesX, numNodesY);

   // This code reads the value of an arbitrary node and replaces it with a
   //    different arbitrary value
   // This would usually be done based on some calculation
   if (numNodesX > 0 && numNodesY > 0)
   {
      double nodeValue;
      int nodeMasked;
      status = MTK_Grid_GetNodeValue(grid, 0, 0, &nodeValue, &nodeMasked);
      if (status)
      {
         HandleError();
         return 1;
      }
      printf("The node at 0,0 has a value of %f\n", nodeValue);

      status = MTK_Grid_SetNodeValue(grid, 0, 0, 50, nodeMasked);
      if (status)
      {
         HandleError();
         return 1;
      }
      printf("The node at 0,0 now has a value of 50\n");
   }

   strncpy(newFilename, filename, MTK_BUFFER_SIZE - 1);
   newFilename[MTK_BUFFER_SIZE - 1] = '\0';
   extension = strstr(newFilename, ".00g");
   if (extension && strlen(newFilename) < MTK_BUFFER_SIZE - 9)
   {
      strcpy(extension, "_with_00_at_50.00g");
   }
   else
   {
      strcpy(newFilename, "NewGridMadeFromExampleWith00At50.00g");
   }

   printf("Saving triangulation as %s...", newFilename);
   status = MTK_Grid_Save(grid, newFilename);
   if (status)
   {
      HandleError();
      return 1;
   }
   printf("done\n");

   status = MTK_Grid_Close(grid);
   if (status)
   {
      HandleError();
      return 1;
   }

   return 0;
}