//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.10.22 at 11:47:32 PM MESZ 
//


package org.citygml4j.jaxb.citygml.core._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.citygml4j.jaxb.gml._3_1_1.AbstractFeatureType;
import org.citygml4j.jaxb.gml._3_1_1.MultiPointPropertyType;


/**
 * Type for addresses. It references the xAL address standard issued by the OASIS consortium. Please note, that addresses are
 *                 modelled as GML features. Every address can be assigned zero or more 2D or 3D point geometries (one gml:MultiPoint geometry) locating the
 *                 entrance(s). 
 * 
 * <p>Java class for AddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.opengis.net/gml}AbstractFeatureType">
 *       &lt;sequence>
 *         &lt;element name="xalAddress" type="{http://www.opengis.net/citygml/1.0}xalAddressPropertyType"/>
 *         &lt;element name="multiPoint" type="{http://www.opengis.net/gml}MultiPointPropertyType" minOccurs="0"/>
 *         &lt;element ref="{http://www.opengis.net/citygml/1.0}_GenericApplicationPropertyOfAddress" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressType", propOrder = {
    "xalAddress",
    "multiPoint",
    "genericApplicationPropertyOfAddress"
})
public class AddressType
    extends AbstractFeatureType
{

    @XmlElement(required = true)
    protected XalAddressPropertyType xalAddress;
    protected MultiPointPropertyType multiPoint;
    @XmlElementRef(name = "_GenericApplicationPropertyOfAddress", namespace = "http://www.opengis.net/citygml/1.0", type = JAXBElement.class)
    protected List<JAXBElement<Object>> genericApplicationPropertyOfAddress;

    /**
     * Gets the value of the xalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link XalAddressPropertyType }
     *     
     */
    public XalAddressPropertyType getXalAddress() {
        return xalAddress;
    }

    /**
     * Sets the value of the xalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link XalAddressPropertyType }
     *     
     */
    public void setXalAddress(XalAddressPropertyType value) {
        this.xalAddress = value;
    }

    public boolean isSetXalAddress() {
        return (this.xalAddress!= null);
    }

    /**
     * Gets the value of the multiPoint property.
     * 
     * @return
     *     possible object is
     *     {@link MultiPointPropertyType }
     *     
     */
    public MultiPointPropertyType getMultiPoint() {
        return multiPoint;
    }

    /**
     * Sets the value of the multiPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiPointPropertyType }
     *     
     */
    public void setMultiPoint(MultiPointPropertyType value) {
        this.multiPoint = value;
    }

    public boolean isSetMultiPoint() {
        return (this.multiPoint!= null);
    }

    /**
     * Gets the value of the genericApplicationPropertyOfAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericApplicationPropertyOfAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    get_GenericApplicationPropertyOfAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * 
     */
    public List<JAXBElement<Object>> get_GenericApplicationPropertyOfAddress() {
        if (genericApplicationPropertyOfAddress == null) {
            genericApplicationPropertyOfAddress = new ArrayList<JAXBElement<Object>>();
        }
        return this.genericApplicationPropertyOfAddress;
    }

    public boolean isSet_GenericApplicationPropertyOfAddress() {
        return ((this.genericApplicationPropertyOfAddress!= null)&&(!this.genericApplicationPropertyOfAddress.isEmpty()));
    }

    public void unset_GenericApplicationPropertyOfAddress() {
        this.genericApplicationPropertyOfAddress = null;
    }

    /**
     * Sets the value of the _GenericApplicationPropertyOfAddress property.
     * 
     * @param genericApplicationPropertyOfAddress
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void set_GenericApplicationPropertyOfAddress(List<JAXBElement<Object>> genericApplicationPropertyOfAddress) {
        this.genericApplicationPropertyOfAddress = genericApplicationPropertyOfAddress;
    }

}
