//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.10.22 at 11:47:32 PM MESZ 
//


package org.citygml4j.jaxb.citygml.grp._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.citygml4j.jaxb.citygml.bldg._1.AbstractBoundarySurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.AbstractBuildingType;
import org.citygml4j.jaxb.citygml.bldg._1.AbstractOpeningType;
import org.citygml4j.jaxb.citygml.bldg._1.BuildingFurnitureType;
import org.citygml4j.jaxb.citygml.bldg._1.BuildingInstallationType;
import org.citygml4j.jaxb.citygml.bldg._1.BuildingPartType;
import org.citygml4j.jaxb.citygml.bldg._1.BuildingType;
import org.citygml4j.jaxb.citygml.bldg._1.CeilingSurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.ClosureSurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.DoorType;
import org.citygml4j.jaxb.citygml.bldg._1.FloorSurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.GroundSurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.IntBuildingInstallationType;
import org.citygml4j.jaxb.citygml.bldg._1.InteriorWallSurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.RoofSurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.RoomType;
import org.citygml4j.jaxb.citygml.bldg._1.WallSurfaceType;
import org.citygml4j.jaxb.citygml.bldg._1.WindowType;
import org.citygml4j.jaxb.citygml.core._1.AbstractCityObjectType;
import org.citygml4j.jaxb.citygml.core._1.AbstractSiteType;
import org.citygml4j.jaxb.citygml.dem._1.AbstractReliefComponentType;
import org.citygml4j.jaxb.citygml.dem._1.BreaklineReliefType;
import org.citygml4j.jaxb.citygml.dem._1.MassPointReliefType;
import org.citygml4j.jaxb.citygml.dem._1.RasterReliefType;
import org.citygml4j.jaxb.citygml.dem._1.ReliefFeatureType;
import org.citygml4j.jaxb.citygml.dem._1.TINReliefType;
import org.citygml4j.jaxb.citygml.frn._1.CityFurnitureType;
import org.citygml4j.jaxb.citygml.gen._1.GenericCityObjectType;
import org.citygml4j.jaxb.citygml.luse._1.LandUseType;
import org.citygml4j.jaxb.citygml.tran._1.AbstractTransportationObjectType;
import org.citygml4j.jaxb.citygml.tran._1.AuxiliaryTrafficAreaType;
import org.citygml4j.jaxb.citygml.tran._1.RailwayType;
import org.citygml4j.jaxb.citygml.tran._1.RoadType;
import org.citygml4j.jaxb.citygml.tran._1.SquareType;
import org.citygml4j.jaxb.citygml.tran._1.TrackType;
import org.citygml4j.jaxb.citygml.tran._1.TrafficAreaType;
import org.citygml4j.jaxb.citygml.tran._1.TransportationComplexType;
import org.citygml4j.jaxb.citygml.veg._1.AbstractVegetationObjectType;
import org.citygml4j.jaxb.citygml.veg._1.PlantCoverType;
import org.citygml4j.jaxb.citygml.veg._1.SolitaryVegetationObjectType;
import org.citygml4j.jaxb.citygml.wtr._1.AbstractWaterBoundarySurfaceType;
import org.citygml4j.jaxb.citygml.wtr._1.AbstractWaterObjectType;
import org.citygml4j.jaxb.citygml.wtr._1.WaterBodyType;
import org.citygml4j.jaxb.citygml.wtr._1.WaterClosureSurfaceType;
import org.citygml4j.jaxb.citygml.wtr._1.WaterGroundSurfaceType;
import org.citygml4j.jaxb.citygml.wtr._1.WaterSurfaceType;
import org.w3c.dom.Element;


/**
 * Denotes the relation of a CityObjectGroup to its members, which are _CityObjects. The CityObjectGroupMemberType element must
 *                 either carry a reference to a _CityObject object or contain a _CityObject object inline, but neither both nor none. 
 * 
 * <p>Java class for CityObjectGroupMemberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CityObjectGroupMemberType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element ref="{http://www.opengis.net/citygml/1.0}_CityObject"/>
 *         &lt;element ref="{http://www.opengis.net/gml}_ADEComponent" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.opengis.net/gml}AssociationAttributeGroup"/>
 *       &lt;attribute name="role" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CityObjectGroupMemberType", propOrder = {
    "cityObject",
    "adeComponent"
})
public class CityObjectGroupMemberType {

    @XmlElementRef(name = "_CityObject", namespace = "http://www.opengis.net/citygml/1.0", type = JAXBElement.class)
    protected JAXBElement<? extends AbstractCityObjectType> cityObject;
    @XmlAnyElement
    protected Element adeComponent;
    @XmlAttribute(name = "role")
    protected String groupRole;
    @XmlAttribute(name = "remoteSchema", namespace = "http://www.opengis.net/gml")
    @XmlSchemaType(name = "anyURI")
    protected String remoteSchema;
    @XmlAttribute(name = "type", namespace = "http://www.w3.org/1999/xlink")
    protected String type;
    @XmlAttribute(name = "href", namespace = "http://www.w3.org/1999/xlink")
    @XmlSchemaType(name = "anyURI")
    protected String href;
    @XmlAttribute(name = "role", namespace = "http://www.w3.org/1999/xlink")
    @XmlSchemaType(name = "anyURI")
    protected String role;
    @XmlAttribute(name = "arcrole", namespace = "http://www.w3.org/1999/xlink")
    @XmlSchemaType(name = "anyURI")
    protected String arcrole;
    @XmlAttribute(name = "title", namespace = "http://www.w3.org/1999/xlink")
    protected String title;
    @XmlAttribute(name = "show", namespace = "http://www.w3.org/1999/xlink")
    protected String show;
    @XmlAttribute(name = "actuate", namespace = "http://www.w3.org/1999/xlink")
    protected String actuate;

    /**
     * Gets the value of the _CityObject property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RoadType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractWaterBoundarySurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CeilingSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PlantCoverType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RasterReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WallSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TrafficAreaType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CityFurnitureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterGroundSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransportationComplexType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SolitaryVegetationObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractWaterObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractReliefComponentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractBoundarySurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RoofSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ClosureSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SquareType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractBuildingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RailwayType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractTransportationObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GroundSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CityObjectGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BreaklineReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractOpeningType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DoorType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TINReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AuxiliaryTrafficAreaType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractVegetationObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MassPointReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FloorSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingFurnitureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RoomType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WindowType }{@code >}
     *     {@link JAXBElement }{@code <}{@link IntBuildingInstallationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingInstallationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReliefFeatureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingPartType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterClosureSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractCityObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractSiteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TrackType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GenericCityObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterBodyType }{@code >}
     *     {@link JAXBElement }{@code <}{@link LandUseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link InteriorWallSurfaceType }{@code >}
     *     
     */
    public JAXBElement<? extends AbstractCityObjectType> get_CityObject() {
        return cityObject;
    }

    /**
     * Sets the value of the _CityObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RoadType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractWaterBoundarySurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CeilingSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PlantCoverType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RasterReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WallSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TrafficAreaType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CityFurnitureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterGroundSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransportationComplexType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SolitaryVegetationObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractWaterObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractReliefComponentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractBoundarySurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RoofSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ClosureSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SquareType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractBuildingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RailwayType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractTransportationObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GroundSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CityObjectGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BreaklineReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractOpeningType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DoorType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TINReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AuxiliaryTrafficAreaType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractVegetationObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MassPointReliefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FloorSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingFurnitureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RoomType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WindowType }{@code >}
     *     {@link JAXBElement }{@code <}{@link IntBuildingInstallationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingInstallationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReliefFeatureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingPartType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterClosureSurfaceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractCityObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AbstractSiteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TrackType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BuildingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GenericCityObjectType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WaterBodyType }{@code >}
     *     {@link JAXBElement }{@code <}{@link LandUseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link InteriorWallSurfaceType }{@code >}
     *     
     */
    public void set_CityObject(JAXBElement<? extends AbstractCityObjectType> value) {
        this.cityObject = ((JAXBElement<? extends AbstractCityObjectType> ) value);
    }

    public boolean isSet_CityObject() {
        return (this.cityObject!= null);
    }

    /**
     * Gets the value of the _ADEComponent property.
     * 
     * @return
     *     possible object is
     *     {@link Element }
     *     
     */
    public Element get_ADEComponent() {
        return adeComponent;
    }

    /**
     * Sets the value of the _ADEComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Element }
     *     
     */
    public void set_ADEComponent(Element value) {
        this.adeComponent = value;
    }

    public boolean isSet_ADEComponent() {
        return (this.adeComponent!= null);
    }

    /**
     * Gets the value of the groupRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupRole() {
        return groupRole;
    }

    /**
     * Sets the value of the groupRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupRole(String value) {
        this.groupRole = value;
    }

    public boolean isSetGroupRole() {
        return (this.groupRole!= null);
    }

    /**
     * Gets the value of the remoteSchema property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteSchema() {
        return remoteSchema;
    }

    /**
     * Sets the value of the remoteSchema property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteSchema(String value) {
        this.remoteSchema = value;
    }

    public boolean isSetRemoteSchema() {
        return (this.remoteSchema!= null);
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        if (type == null) {
            return "simple";
        } else {
            return type;
        }
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    public boolean isSetType() {
        return (this.type!= null);
    }

    /**
     * Gets the value of the href property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHref() {
        return href;
    }

    /**
     * Sets the value of the href property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHref(String value) {
        this.href = value;
    }

    public boolean isSetHref() {
        return (this.href!= null);
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    public boolean isSetRole() {
        return (this.role!= null);
    }

    /**
     * Gets the value of the arcrole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArcrole() {
        return arcrole;
    }

    /**
     * Sets the value of the arcrole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArcrole(String value) {
        this.arcrole = value;
    }

    public boolean isSetArcrole() {
        return (this.arcrole!= null);
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    public boolean isSetTitle() {
        return (this.title!= null);
    }

    /**
     * Gets the value of the show property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShow() {
        return show;
    }

    /**
     * Sets the value of the show property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShow(String value) {
        this.show = value;
    }

    public boolean isSetShow() {
        return (this.show!= null);
    }

    /**
     * Gets the value of the actuate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActuate() {
        return actuate;
    }

    /**
     * Sets the value of the actuate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActuate(String value) {
        this.actuate = value;
    }

    public boolean isSetActuate() {
        return (this.actuate!= null);
    }

}
