//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.10.22 at 11:47:32 PM MESZ 
//


package org.citygml4j.jaxb.citygml._0_4;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Type describing the closure surface between water bodys. As subclass of _CityObject, a WaterClosureSurface inherits all attributes
 *                 and relations, in particular an id, names, external references, generic attributes and generalization relations. 
 * 
 * <p>Java class for WaterClosureSurfaceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WaterClosureSurfaceType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.citygml.org/citygml/1/0/0}_WaterBoundarySurfaceType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.citygml.org/citygml/1/0/0}_GenericApplicationPropertyOfWaterClosureSurface" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WaterClosureSurfaceType", propOrder = {
    "genericApplicationPropertyOfWaterClosureSurface"
})
public class WaterClosureSurfaceType
    extends _WaterBoundarySurfaceType
{

    @XmlElementRef(name = "_GenericApplicationPropertyOfWaterClosureSurface", namespace = "http://www.citygml.org/citygml/1/0/0", type = JAXBElement.class)
    protected List<JAXBElement<Object>> genericApplicationPropertyOfWaterClosureSurface;

    /**
     * Gets the value of the genericApplicationPropertyOfWaterClosureSurface property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericApplicationPropertyOfWaterClosureSurface property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    get_GenericApplicationPropertyOfWaterClosureSurface().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * 
     */
    public List<JAXBElement<Object>> get_GenericApplicationPropertyOfWaterClosureSurface() {
        if (genericApplicationPropertyOfWaterClosureSurface == null) {
            genericApplicationPropertyOfWaterClosureSurface = new ArrayList<JAXBElement<Object>>();
        }
        return this.genericApplicationPropertyOfWaterClosureSurface;
    }

    public boolean isSet_GenericApplicationPropertyOfWaterClosureSurface() {
        return ((this.genericApplicationPropertyOfWaterClosureSurface!= null)&&(!this.genericApplicationPropertyOfWaterClosureSurface.isEmpty()));
    }

    public void unset_GenericApplicationPropertyOfWaterClosureSurface() {
        this.genericApplicationPropertyOfWaterClosureSurface = null;
    }

    /**
     * Sets the value of the _GenericApplicationPropertyOfWaterClosureSurface property.
     * 
     * @param genericApplicationPropertyOfWaterClosureSurface
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void set_GenericApplicationPropertyOfWaterClosureSurface(List<JAXBElement<Object>> genericApplicationPropertyOfWaterClosureSurface) {
        this.genericApplicationPropertyOfWaterClosureSurface = genericApplicationPropertyOfWaterClosureSurface;
    }

}
