//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.10.22 at 11:47:32 PM MESZ 
//


package org.citygml4j.jaxb.citygml._0_4;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Type describing the abstract superclass for vegetation objects. A subclass is either a SolitaryVegetationObject or a PlantCover.
 *             
 * 
 * <p>Java class for _VegetationObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="_VegetationObjectType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.citygml.org/citygml/1/0/0}_CityObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.citygml.org/citygml/1/0/0}_GenericApplicationPropertyOfVegetationObject" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "_VegetationObjectType", propOrder = {
    "genericApplicationPropertyOfVegetationObject"
})
@XmlSeeAlso({
    PlantCoverType.class,
    SolitaryVegetationObjectType.class
})
public abstract class _VegetationObjectType
    extends _CityObjectType
{

    @XmlElementRef(name = "_GenericApplicationPropertyOfVegetationObject", namespace = "http://www.citygml.org/citygml/1/0/0", type = JAXBElement.class)
    protected List<JAXBElement<Object>> genericApplicationPropertyOfVegetationObject;

    /**
     * Gets the value of the genericApplicationPropertyOfVegetationObject property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericApplicationPropertyOfVegetationObject property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    get_GenericApplicationPropertyOfVegetationObject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * 
     */
    public List<JAXBElement<Object>> get_GenericApplicationPropertyOfVegetationObject() {
        if (genericApplicationPropertyOfVegetationObject == null) {
            genericApplicationPropertyOfVegetationObject = new ArrayList<JAXBElement<Object>>();
        }
        return this.genericApplicationPropertyOfVegetationObject;
    }

    public boolean isSet_GenericApplicationPropertyOfVegetationObject() {
        return ((this.genericApplicationPropertyOfVegetationObject!= null)&&(!this.genericApplicationPropertyOfVegetationObject.isEmpty()));
    }

    public void unset_GenericApplicationPropertyOfVegetationObject() {
        this.genericApplicationPropertyOfVegetationObject = null;
    }

    /**
     * Sets the value of the _GenericApplicationPropertyOfVegetationObject property.
     * 
     * @param genericApplicationPropertyOfVegetationObject
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void set_GenericApplicationPropertyOfVegetationObject(List<JAXBElement<Object>> genericApplicationPropertyOfVegetationObject) {
        this.genericApplicationPropertyOfVegetationObject = genericApplicationPropertyOfVegetationObject;
    }

}
