//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.10.22 at 11:47:32 PM MESZ 
//


package org.citygml4j.jaxb.citygml._0_4;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.citygml4j.jaxb.gml._3_1_1.GeometryPropertyType;


/**
 * An IntBuildingInstallation (German translation is 'Gebäudeinstallation') is an interior part of a Building which has a specific
 *                 function or semantical meaning. Examples are interior stairs, railings, radiators or pipes. As subclass of _CityObject, a
 *                 nIntBuildingInstallation inherits all attributes and relations, in particular an id, names, external references, generic attributes and
 *                 generalization relations. 
 * 
 * <p>Java class for IntBuildingInstallationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntBuildingInstallationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.citygml.org/citygml/1/0/0}_CityObjectType">
 *       &lt;sequence>
 *         &lt;element name="class" type="{http://www.citygml.org/citygml/1/0/0}IntBuildingInstallationClassType" minOccurs="0"/>
 *         &lt;element name="function" type="{http://www.citygml.org/citygml/1/0/0}IntBuildingInstallationFunctionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="usage" type="{http://www.citygml.org/citygml/1/0/0}IntBuildingInstallationUsageType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="lod4Geometry" type="{http://www.opengis.net/gml}GeometryPropertyType" minOccurs="0"/>
 *         &lt;element ref="{http://www.citygml.org/citygml/1/0/0}_GenericApplicationPropertyOfIntBuildingInstallation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntBuildingInstallationType", propOrder = {
    "clazz",
    "function",
    "usage",
    "lod4Geometry",
    "genericApplicationPropertyOfIntBuildingInstallation"
})
public class IntBuildingInstallationType
    extends _CityObjectType
{

    @XmlElement(name = "class")
    protected String clazz;
    protected List<String> function;
    protected List<String> usage;
    protected GeometryPropertyType lod4Geometry;
    @XmlElementRef(name = "_GenericApplicationPropertyOfIntBuildingInstallation", namespace = "http://www.citygml.org/citygml/1/0/0", type = JAXBElement.class)
    protected List<JAXBElement<Object>> genericApplicationPropertyOfIntBuildingInstallation;

    /**
     * Gets the value of the clazz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClazz(String value) {
        this.clazz = value;
    }

    public boolean isSetClazz() {
        return (this.clazz!= null);
    }

    /**
     * Gets the value of the function property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the function property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFunction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFunction() {
        if (function == null) {
            function = new ArrayList<String>();
        }
        return this.function;
    }

    public boolean isSetFunction() {
        return ((this.function!= null)&&(!this.function.isEmpty()));
    }

    public void unsetFunction() {
        this.function = null;
    }

    /**
     * Gets the value of the usage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getUsage() {
        if (usage == null) {
            usage = new ArrayList<String>();
        }
        return this.usage;
    }

    public boolean isSetUsage() {
        return ((this.usage!= null)&&(!this.usage.isEmpty()));
    }

    public void unsetUsage() {
        this.usage = null;
    }

    /**
     * Gets the value of the lod4Geometry property.
     * 
     * @return
     *     possible object is
     *     {@link GeometryPropertyType }
     *     
     */
    public GeometryPropertyType getLod4Geometry() {
        return lod4Geometry;
    }

    /**
     * Sets the value of the lod4Geometry property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeometryPropertyType }
     *     
     */
    public void setLod4Geometry(GeometryPropertyType value) {
        this.lod4Geometry = value;
    }

    public boolean isSetLod4Geometry() {
        return (this.lod4Geometry!= null);
    }

    /**
     * Gets the value of the genericApplicationPropertyOfIntBuildingInstallation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericApplicationPropertyOfIntBuildingInstallation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    get_GenericApplicationPropertyOfIntBuildingInstallation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * 
     */
    public List<JAXBElement<Object>> get_GenericApplicationPropertyOfIntBuildingInstallation() {
        if (genericApplicationPropertyOfIntBuildingInstallation == null) {
            genericApplicationPropertyOfIntBuildingInstallation = new ArrayList<JAXBElement<Object>>();
        }
        return this.genericApplicationPropertyOfIntBuildingInstallation;
    }

    public boolean isSet_GenericApplicationPropertyOfIntBuildingInstallation() {
        return ((this.genericApplicationPropertyOfIntBuildingInstallation!= null)&&(!this.genericApplicationPropertyOfIntBuildingInstallation.isEmpty()));
    }

    public void unset_GenericApplicationPropertyOfIntBuildingInstallation() {
        this.genericApplicationPropertyOfIntBuildingInstallation = null;
    }

    /**
     * Sets the value of the function property.
     * 
     * @param function
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunction(List<String> function) {
        this.function = function;
    }

    /**
     * Sets the value of the usage property.
     * 
     * @param usage
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsage(List<String> usage) {
        this.usage = usage;
    }

    /**
     * Sets the value of the _GenericApplicationPropertyOfIntBuildingInstallation property.
     * 
     * @param genericApplicationPropertyOfIntBuildingInstallation
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void set_GenericApplicationPropertyOfIntBuildingInstallation(List<JAXBElement<Object>> genericApplicationPropertyOfIntBuildingInstallation) {
        this.genericApplicationPropertyOfIntBuildingInstallation = genericApplicationPropertyOfIntBuildingInstallation;
    }

}
