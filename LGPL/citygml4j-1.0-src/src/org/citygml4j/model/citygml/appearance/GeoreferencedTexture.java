/*
 * This file is part of citygml4j.
 * Copyright (c) 2007 - 2010
 * Institute for Geodesy and Geoinformation Science
 * Technische Universitaet Berlin, Germany
 * http://www.igg.tu-berlin.de/
 *
 * The citygml4j library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see 
 * <http://www.gnu.org/licenses/>.
 */
package org.citygml4j.model.citygml.appearance;

import java.util.List;

import org.citygml4j.model.citygml.ade.ADEComponent;
import org.citygml4j.model.citygml.core.TransformationMatrix2x2;
import org.citygml4j.model.gml.geometry.primitives.PointProperty;

public interface GeoreferencedTexture extends AbstractTexture {
	public Boolean getPreferWorldFile();
	public PointProperty getReferencePoint();
	public TransformationMatrix2x2 getOrientation();
	public List<String> getTarget();
	public List<ADEComponent> getGenericApplicationPropertyOfGeoreferencedTexture();
	public boolean isSetPreferWorldFile();
	public boolean isSetReferencePoint();
	public boolean isSetOrientation();
	public boolean isSetTarget();
	public boolean isSetGenericApplicationPropertyOfGeoreferencedTexture();
	
	public void setPreferWorldFile(Boolean preferWorldFile);
	public void setReferencePoint(PointProperty referencePoint);
	public void setOrientation(TransformationMatrix2x2 orientation);
	public void setTarget(List<String> target);
	public void addTarget(String target);	
	public void addGenericApplicationPropertyOfGeoreferencedTexture(ADEComponent ade);
	public void setGenericApplicationPropertyOfGeoreferencedTexture(List<ADEComponent> ade);
	public void unsetPreferWorldFile();
	public void unsetReferencePoint();
	public void unsetOrientation();
	public void unsetTarget();
	public boolean unsetTarget(String target);
	public void unsetGenericApplicationPropertyOfGeoreferencedTexture();
	public boolean unsetGenericApplicationPropertyOfGeoreferencedTexture(ADEComponent ade);
}
