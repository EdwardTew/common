/*
 * This file is part of citygml4j.
 * Copyright (c) 2007 - 2010
 * Institute for Geodesy and Geoinformation Science
 * Technische Universitaet Berlin, Germany
 * http://www.igg.tu-berlin.de/
 *
 * The citygml4j library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see 
 * <http://www.gnu.org/licenses/>.
 */
package org.citygml4j.impl.citygml.cityobjectgroup;

import java.util.ArrayList;
import java.util.List;

import org.citygml4j.builder.copy.CopyBuilder;
import org.citygml4j.impl.citygml.core.AbstractCityObjectImpl;
import org.citygml4j.impl.gml.feature.BoundingShapeImpl;
import org.citygml4j.model.citygml.CityGMLClass;
import org.citygml4j.model.citygml.ade.ADEComponent;
import org.citygml4j.model.citygml.cityobjectgroup.CityObjectGroup;
import org.citygml4j.model.citygml.cityobjectgroup.CityObjectGroupMember;
import org.citygml4j.model.citygml.cityobjectgroup.CityObjectGroupParent;
import org.citygml4j.model.citygml.core.LodRepresentation;
import org.citygml4j.model.common.child.ChildList;
import org.citygml4j.model.common.visitor.FeatureFunctor;
import org.citygml4j.model.common.visitor.FeatureVisitor;
import org.citygml4j.model.common.visitor.GMLFunctor;
import org.citygml4j.model.common.visitor.GMLVisitor;
import org.citygml4j.model.gml.feature.BoundingShape;
import org.citygml4j.model.gml.geometry.AbstractGeometry;
import org.citygml4j.model.gml.geometry.GeometryProperty;
import org.citygml4j.model.module.citygml.CityObjectGroupModule;

public class CityObjectGroupImpl extends AbstractCityObjectImpl implements CityObjectGroup {
	private String clazz;
	private List<String> function;
	private List<String> usage;
	private List<CityObjectGroupMember> groupMember;
	private CityObjectGroupParent groupParent;
	private GeometryProperty<? extends AbstractGeometry> geometry;
	private List<ADEComponent> ade;
	private CityObjectGroupModule module;
	
	public CityObjectGroupImpl() {
		
	}
	
	public CityObjectGroupImpl(CityObjectGroupModule module) {
		this.module = module;
	}
	
	public void addFunction(String function) {
		if (this.function == null)
			this.function = new ArrayList<String>();
		
		this.function.add(function);
	}

	public void addGenericApplicationPropertyOfCityObjectGroup(ADEComponent ade) {
		if (this.ade == null)
			this.ade = new ChildList<ADEComponent>(this);

		this.ade.add(ade);
	}

	public void addGroupMember(CityObjectGroupMember groupMember) {
		if (this.groupMember == null)
			this.groupMember = new ChildList<CityObjectGroupMember>(this);

		this.groupMember.add(groupMember);
	}

	public void addUsage(String usage) {
		if (this.usage == null)
			this.usage = new ArrayList<String>();
		
		this.usage.add(usage);
	}

	public String getClazz() {
		return clazz;
	}

	public List<String> getFunction() {
		if (function == null)
			function = new ArrayList<String>();
		
		return function;
	}

	public List<ADEComponent> getGenericApplicationPropertyOfCityObjectGroup() {
		if (ade == null)
			ade = new ChildList<ADEComponent>(this);

		return ade;
	}

	public GeometryProperty<? extends AbstractGeometry> getGeometry() {
		return geometry;
	}

	public List<CityObjectGroupMember> getGroupMember() {
		if (groupMember == null)
			groupMember = new ChildList<CityObjectGroupMember>(this);

		return groupMember;
	}

	public CityObjectGroupParent getGroupParent() {
		return groupParent;
	}

	public List<String> getUsage() {
		if (usage == null)
			usage = new ArrayList<String>();
		
		return usage;
	}

	public boolean isSetClazz() {
		return clazz != null;
	}

	public boolean isSetFunction() {
		return function != null && !function.isEmpty();
	}

	public boolean isSetGenericApplicationPropertyOfCityObjectGroup() {
		return ade != null && !ade.isEmpty();
	}

	public boolean isSetGeometry() {
		return geometry != null;
	}

	public boolean isSetGroupMember() {
		return groupMember != null && !groupMember.isEmpty();
	}

	public boolean isSetGroupParent() {
		return groupParent != null;
	}

	public boolean isSetUsage() {
		return usage != null && !usage.isEmpty();
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public void setFunction(List<String> function) {
		this.function = function;
	}

	public void setGenericApplicationPropertyOfCityObjectGroup(List<ADEComponent> ade) {
		this.ade = new ChildList<ADEComponent>(this, ade);
	}

	public void setGeometry(GeometryProperty<? extends AbstractGeometry> geometry) {
		if (geometry != null)
			geometry.setParent(this);
		
		this.geometry = geometry;
	}

	public void setGroupMember(List<CityObjectGroupMember> groupMember) {
		this.groupMember = new ChildList<CityObjectGroupMember>(this, groupMember);
	}

	public void setGroupParent(CityObjectGroupParent groupParent) {
		if (groupParent != null)
			groupParent.setParent(this);
		
		this.groupParent = groupParent;
	}

	public void setUsage(List<String> usage) {
		this.usage = usage;
	}

	public void unsetClazz() {
		clazz = null;
	}

	public void unsetFunction() {
		function = null;
	}

	public boolean unsetFunction(String function) {
		return isSetFunction() ? this.function.remove(function) : false;
	}

	public void unsetGenericApplicationPropertyOfCityObjectGroup() {
		if (isSetGenericApplicationPropertyOfCityObjectGroup())
			ade.clear();

		ade = null;
	}

	public boolean unsetGenericApplicationPropertyOfCityObjectGroup(ADEComponent ade) {
		return isSetGenericApplicationPropertyOfCityObjectGroup() ? this.ade.remove(ade) : false;
	}

	public void unsetGeometry() {
		if (isSetGeometry())
			geometry.unsetParent();
		
		geometry = null;
	}

	public void unsetGroupMember() {
		if (isSetGroupMember())
			groupMember.clear();

		groupMember = null;
	}

	public boolean unsetGroupMember(CityObjectGroupMember groupMember) {
		return isSetGroupMember() ? this.groupMember.remove(groupMember) : false;
	}

	public void unsetGroupParent() {
		if (isSetGroupParent())
			groupParent.unsetParent();
		
		groupParent = null;
	}

	public void unsetUsage() {
		usage = null;
	}

	public boolean unsetUsage(String usage) {
		return isSetUsage() ? this.usage.remove(usage) : false;
	}

	public CityGMLClass getCityGMLClass() {
		return CityGMLClass.CITY_OBJECT_GROUP;
	}

	public final CityObjectGroupModule getCityGMLModule() {
		return module;
	}

	public boolean isSetCityGMLModule() {
		return module != null;
	}

	@Override
	public BoundingShape calcBoundedBy(boolean setBoundedBy) {
		BoundingShape boundedBy = new BoundingShapeImpl();
		
		if (isSetGeometry()) {
			if (geometry.isSetGeometry()) {
				calcBoundedBy(boundedBy, geometry.getGeometry());
			} else {
				// xlink
			}
		}
		
		if (isSetGroupMember()) {
			for (CityObjectGroupMember member : groupMember) {
				if (member.isSetObject()) {
					calcBoundedBy(boundedBy, member.getObject(), setBoundedBy);
				} else {
					// xlink
				}					
			}
		}
		
		if (boundedBy.isSetEnvelope()) {
			if (setBoundedBy)
				setBoundedBy(boundedBy);

			return boundedBy;
		} else
			return null;
	}
	
	@Override
	public LodRepresentation getLodRepresentation() {
		LodRepresentation lodRepresentation = new LodRepresentation();
		
		if (isSetGeometry()) {
			for (int lod = 0; lod < 5; lod++)
				lodRepresentation.getLodGeometry(lod).add(geometry);
		}
		
		return lodRepresentation;
	}

	public Object copy(CopyBuilder copyBuilder) {
		return copyTo(new CityObjectGroupImpl(), copyBuilder);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object copyTo(Object target, CopyBuilder copyBuilder) {
		CityObjectGroup copy = (target == null) ? new CityObjectGroupImpl() : (CityObjectGroup)target;
		super.copyTo(copy, copyBuilder);
		
		if (isSetClazz())
			copy.setClazz(copyBuilder.copy(clazz));
		
		if (isSetFunction())
			copy.setFunction((List<String>)copyBuilder.copy(function));
		
		if (isSetUsage())
			copy.setFunction((List<String>)copyBuilder.copy(usage));
		
		if (isSetGroupMember()) {
			for (CityObjectGroupMember part : groupMember) {
				CityObjectGroupMember copyPart = (CityObjectGroupMember)copyBuilder.copy(part);
				copy.addGroupMember(copyPart);

				if (part != null && copyPart == part)
					part.setParent(this);
			}
		}
		
		if (isSetGroupParent()) {
			copy.setGroupParent((CityObjectGroupParent)copyBuilder.copy(groupParent));
			if (copy.getGroupParent() == groupParent)
				groupParent.setParent(this);
		}
		
		if (isSetGeometry()) {
			copy.setGeometry((GeometryProperty<? extends AbstractGeometry>)copyBuilder.copy(geometry));
			if (copy.getGeometry() == geometry)
				geometry.setParent(this);
		}
		
		if (isSetGenericApplicationPropertyOfCityObjectGroup()) {
			for (ADEComponent part : ade) {
				ADEComponent copyPart = (ADEComponent)copyBuilder.copy(part);
				copy.addGenericApplicationPropertyOfCityObjectGroup(copyPart);
				
				if (part != null && copyPart == part)
					part.setParent(this);
			}
		}
		
		return copy;
	}
	
	public void accept(FeatureVisitor visitor) {
		visitor.visit(this);
	}
	
	public <T> T accept(FeatureFunctor<T> visitor) {
		return visitor.apply(this);
	}
	
	public void accept(GMLVisitor visitor) {
		visitor.visit(this);
	}
	
	public <T> T accept(GMLFunctor<T> visitor) {
		return visitor.apply(this);
	}

}
