No specific licence type is specified

Permission to use is granted in the COPYING file.

No specific limitations are placed on the end user 
Code files must original retain copyright information.

No restrictions on comercial use.

code downloaded from the web
http://curl.haxx.se/libcurl/
Windows 32bit code and binary library

Build Instructions
------------------
Extract Zip File
run CMAKE
  Set Source directory to the top level folder from the Zip File
  Set the binaries directory to be a "build" folder under the top level folder
  Click the 'Configure' Button
	Set the Generator to be 'Visual studio 10'
	Select 'Native Compilers'
	Click 'Finish'
  Wait while CMAKE processes the source.
  Ensure the Grouped and Advanced checkboxes are checked
  Expand the BUILD entry
  Set the BUILD_CURL_TESTS to Unchecked
  Set the CMAKE_USE_OPENSSL to Unchecked
  Set the CMAKE_SKIP_RPATH to Checked
  Set the CMAKE_USE_RELATIVE_PATHS to Checked
  Set the CURL_DISABLE_LDAP to Checked
  Click the Configure Button Again to apply the changes
  Click the Generate button.
 
 Run Visual Studio
  Open the visual studio solution "CURL.sln" from the build folder
  Set the build configuration to "Release"
  Build the Solution.

  The generated DLL can be found in the "\build\lib\Release" folder.
 
 Note: the CMAKE process creates references to the current folder structure instead of using relative 
  paths and if the CURL source code is moved to another folder then the CMAKE steps should be repeated.

 
 
  
  
  
  
  
  
