// MarkupTest.cpp : implementation file
//
// Markup Release 11.5
// Copyright (C) 2011 First Objective Software, Inc. All rights reserved
// Go to www.firstobject.com for the latest CMarkup and EDOM documentation
// Use in commercial applications requires written permission
// This software is provided "as is", with no warranty.

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <sys/timeb.h>
#include <time.h>
#include "MarkupTest.h"

#define MARKUP_RELEASE MCD_T("11.5")

#if defined(MARKUP_MBCS) && defined(MARKUP_WINCONV)
#include <mbstring.h>
#endif

#ifdef MARKUP_MSXML
#include "MarkupMSXML.h"
#endif

#if _MSC_VER >= 1300 // VC++ 2002 (7.0)
#pragma warning(disable:4127)
#endif // VC++ 2002 (7.0)

#ifdef MARKUP_MSXML
void SimpleMerge( CMarkupMSXML& xmlMaster, CMarkupMSXML& xmlUpdate )
#else
void SimpleMerge( CMarkup& xmlMaster, CMarkup& xmlUpdate )
#endif
{
	// Generic merge xmlUpdate into xmlMaster when element names are unique among siblings
	// removing elements from xmlUpdate as added to or overrided in xmlMaster
	//
	MCD_STR strMergeName;
	xmlMaster.ResetPos();
	xmlUpdate.ResetPos();
	bool bMergeFinished = false;
	if ( ! xmlMaster.FindChildElem() )
	{
		xmlMaster = xmlUpdate;
		bMergeFinished = true;
	}
	xmlUpdate.FindChildElem();
	while ( ! bMergeFinished )
	{
		// Process Element
		xmlMaster.IntoElem();
		xmlUpdate.IntoElem();
		strMergeName = xmlMaster.GetTagName();

		// Did this one match?
		xmlUpdate.ResetMainPos();
		bool bMatched = xmlUpdate.FindElem( strMergeName );
		if ( bMatched )
		{
			// Merge attributes
			for ( int nAttrib=0; !MCD_STRISEMPTY((strMergeName=xmlUpdate.GetAttribName(nAttrib))); ++nAttrib )
				xmlMaster.SetAttrib( strMergeName, xmlUpdate.GetAttrib(strMergeName) );
		}

		// Next element (depth first)
		bool bChildFound = xmlMaster.FindChildElem();
		while ( ! bChildFound && ! bMergeFinished )
		{
			if ( bMatched )
			{
				while ( xmlUpdate.FindChildElem() )
				{
					xmlMaster.AddChildSubDoc( xmlUpdate.GetChildSubDoc() );
					xmlUpdate.RemoveChildElem();
				}
				xmlUpdate.RemoveElem();
			}
			if ( xmlMaster.OutOfElem() )
			{
				xmlUpdate.OutOfElem();
				bChildFound = xmlMaster.FindChildElem();
				if ( ! bChildFound )
				{
					bMatched = true;
					xmlUpdate.ResetChildPos();
				}
			}
			else
				bMergeFinished = true;
		}
	}
}

MCD_STR TruncToLastEOL( MCD_STR strVal )
{
	// locate and truncate from final EOL
	// also cuts any trailing indentation
	int nLength = MCD_STRLENGTH(strVal);
	const MCD_CHAR* pszVal = MCD_2PCSZ(strVal);
	int nLastEOL = nLength;
	int nChar = 0;
	while ( nChar < nLength )
	{
		// Go to next EOL
		nLastEOL = nLength;
		while ( nChar < nLength && pszVal[nChar] != '\r'
				&& pszVal[nChar] != '\n' )
			nChar += MCD_CLEN(&pszVal[nChar]);
		nLastEOL = nChar;
		while ( nChar < nLength
				&& MCD_PSZCHR(MCD_T(" \t\n\r"),pszVal[nChar]) )
			++nChar;
	}
	if ( nLastEOL < nLength )
		strVal = MCD_STRMID( strVal, 0, nLastEOL );
	return strVal;
}

int GetMilliCount()
{
	// Something like GetTickCount but portable
	// It rolls over every ~ 12.1 days (0x100000/24/60/60)
	// Use GetMilliSpan to correct for rollover
	timeb tb;
	ftime( &tb );
	int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}

int GetMilliSpan( int nTimeStart )
{
	int nSpan = GetMilliCount() - nTimeStart;
	if ( nSpan < 0 )
		nSpan += 0x100000 * 1000;
	return nSpan;
}

/////////////////////////////////////////////////////////////////////////////
// CMarkupTest
//
// Cross-platform testing for CMarkup (and VC++ testing for CMarkupMSXML)
//

int CMarkupTest::Alert( MCD_STR strMsg )
{
	AppendLog( MCD_T("\r\n") + strMsg + MCD_T("\r\n") );
	m_strResult += strMsg + MCD_T("\r\n");
	return -1;
}

void CMarkupTest::StartCheckZone( MCD_STR strCheckZone )
{
	m_nCheckCount = 0;
	++m_nTotalZones;
	m_strCheckZone = strCheckZone;
	AppendLog( MCD_T("\r\n") + strCheckZone );
}

int CMarkupTest::Check( int bCorrect, MCD_PCSZ pszProblem/*=NULL*/ )
{
	++m_nCheckCount;
	++m_nTotalChecks;
	MCD_CHAR szCheckNumber[100];
	MCD_SPRINTF( MCD_SSZ(szCheckNumber), MCD_T(" %d"), m_nCheckCount );
	MCD_STR strMsg = szCheckNumber;
	AppendLog( strMsg );
	if ( ! bCorrect )
	{
		if ( MCD_STRISEMPTY(m_strCheckZone) )
			m_strCheckZone = MCD_T("Unknown Check Zone");
		MCD_STR strError = MCD_T("Error: ");
		strError += m_strCheckZone;
		strError += MCD_T(", check");
		strError += szCheckNumber;
		if ( pszProblem )
		{
			strError += MCD_T(" ");
			strError += pszProblem;
		}
		++m_nErrorCount;
		return Alert( strError );
	}
	return 0;
}

void CMarkupTest::AppendLog( MCD_STR strMsg )
{
	if ( m_pszLogFile )
	{
		FILE* fp = NULL;
		MCD_FOPEN( fp, m_pszLogFile, (m_nLogWordWrap==-1)?MCD_T_FILENAME("wb"):MCD_T_FILENAME("ab") );
		if ( m_nLogWordWrap == -1 )
			m_nLogWordWrap = 0;
		if ( fp )
		{
			int nLen = MCD_STRLENGTH(strMsg);
			for ( int nChar=0; nChar<nLen; ++nChar )
			{
				if ( strMsg[nChar] == '\n' )
					m_nLogWordWrap = 0;
				if ( m_nLogWordWrap > 64 && strMsg[nChar] == ' ' )
				{
					fputc( '\r', fp );
					fputc( '\n', fp );
					m_nLogWordWrap = 0;
				}
				fputc( (int)strMsg[nChar], fp );
				++m_nLogWordWrap;
				
			}
			fclose( fp );
		}
	}
}

int CMarkupTest::RandInt( int nNumber )
{
	//
	// Return an integer between 0 and nNumber
	//
	static bool bSeeded = false;
	if ( ! bSeeded )
	{
		srand( (unsigned)time( NULL ) );
		bSeeded = true;
	}

	// Sometimes rand() returns something close enough to 1 to make nRandom == nNumber
	int nRandom = rand() * nNumber / RAND_MAX;
	if ( nRandom == nNumber )
		--nRandom;
	return nRandom;
}

int CMarkupTest::RunTest()
{
	// Instantiate XML objects for use in tests
#ifdef MARKUP_MSXML
	CMarkupMSXML xml, xml2, xml3;
#else
	CMarkup xml, xml2, xml3, mResult;
#endif

	m_nErrorCount = 0;
	m_nTotalZones = 0;
	m_nTotalChecks = 0;
	m_nLogWordWrap = -1;

	// Indicate class and build options
	m_strVer = MARKUP_RELEASE;
	MCD_STRCLEAR(m_strBuild);
#if defined(MARKUP_MSXML)
	m_strBuild += MCD_T("MSXML");
#if defined(MARKUP_MSXML1)
	m_strBuild += MCD_T("1");
#else // not MSXML1
#if defined(MARKUP_MSXML3)
	m_strBuild += MCD_T("3");
#elif defined(MARKUP_MSXML4)
	m_strBuild += MCD_T("4");
#else // not MSXML3 or MSXML4
	m_strBuild += MCD_T("6");
#endif // not MSXML4
	MCD_CHAR szVer[30];
	MCD_SPRINTF( MCD_SSZ(szVer), MCD_T(" (instance MSXML%d)"), xml.GetVersionCreated() );
	m_strBuild += szVer;
#endif // not MSXML1
#endif // MSXML
	if ( ! MCD_STRISEMPTY(m_strBuild) )
		m_strBuild += MCD_T(" ");
#if defined(MARKUP_STL)
	m_strBuild += MCD_T("STL");
#else // not STL
	m_strBuild += MCD_T("MFC");
#endif // not STL
#if defined(MARKUP_ICONV)
	m_strBuild += MCD_T(" ICONV");
#endif // ICONV
#if defined(MARKUP_WINCONV)
	m_strBuild += MCD_T(" WINCONV");
#endif // WINCONV
#if defined(MARKUP_WCHAR)
	m_strBuild += MCD_T(" WCHAR");
#endif // WCHAR
#if defined(MARKUP_MBCS)
	m_strBuild += MCD_T(" MBCS");
#endif // MBCS
#if defined(_DEBUG)
	m_strBuild += MCD_T(" Debug");
#endif // DEBUG
	if ( sizeof(MCD_INTFILEOFFSET) > 4 )
		m_strBuild += MCD_T(" HUGE"); // file mode supports files over 2GB
	m_strResult = MCD_T("CMarkup ") + m_strVer + MCD_T(" ") + m_strBuild + MCD_T("\r\n");

	// Performance Measure for create and parse
	const int nPerfTestElems = 10000; // controls size of test document
	const int nPerfMilliSample = 150; // how long to test
	int nPerfElems = 0;
	int nPerfCreateTime = 0;
	int nPerfCreateLength = 0;
	int nTimeStart = GetMilliCount();
	xml.AddElem( MCD_T("root") );
	xml.IntoElem();
	while ( nPerfCreateTime < nPerfMilliSample )
	{
		xml.AddElem( MCD_T("elem"), MCD_T("data") );
		xml.SetAttrib( MCD_T("attrib"), MCD_T("value") );
		++nPerfElems;
		if ( ! (nPerfElems % nPerfTestElems) )
		{
			nPerfCreateLength += MCD_STRLENGTH( xml.GetDoc() );
			xml.SetDoc( NULL );
			xml.AddElem( MCD_T("root") );
			xml.IntoElem();
		}
		if ( ! (nPerfElems % 100) )
			nPerfCreateTime = GetMilliSpan( nTimeStart );
	}
	nPerfCreateLength += MCD_STRLENGTH( xml.GetDoc() );
	nPerfElems = 0;
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("root") );
	xml.IntoElem();
	while ( nPerfElems < nPerfTestElems )
	{
		xml.AddElem( MCD_T("elem"), MCD_T("data") );
		xml.SetAttrib( MCD_T("attrib"), MCD_T("value") );
		++nPerfElems;
	}
	int nPerfParseTime = 0;
	int nPerfParseLength = 0;
	nTimeStart = GetMilliCount();
	while ( nPerfParseTime < nPerfMilliSample )
	{
		xml.SetDoc( xml.GetDoc() ); // this performs the parse
		nPerfParseLength += MCD_STRLENGTH( xml.GetDoc() );
		nPerfParseTime = GetMilliSpan( nTimeStart );
	}
	m_nCreateSpeed = nPerfCreateTime? nPerfCreateLength/nPerfCreateTime: 0;
	m_nParseSpeed = nPerfParseTime? nPerfParseLength/nPerfParseTime: 0;

	// Integer Method Test
	StartCheckZone( MCD_T("Integer Method Test") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("R"), 5 );
	Check( MCD_STRTOINT(xml.GetData()) == 5 );
	xml.SetData( MCD_T("") );
	Check( xml.InsertChildElem( MCD_T("C"), 1 ) );
	Check( xml.AddChildElem( MCD_T("C"), 3 ) );
	Check( xml.IntoElem() );
	Check( xml.InsertElem( MCD_T("C"), 2 ) );
	xml.ResetMainPos();
	Check( xml.FindElem() );
	Check( MCD_STRTOINT(xml.GetData()) == 1 );
	Check( xml.FindElem() );
	Check( MCD_STRTOINT(xml.GetData()) == 2 );
	Check( xml.FindElem() );
	Check( MCD_STRTOINT(xml.GetData()) == 3 );

	// Quote and special chars in attrib value test
	StartCheckZone( MCD_T("Attrib Value Test") );
	Check( xml.SetDoc( MCD_T("<H g1=\">\" g2=\'>\' d=\"d\'d\" s=\'s\"s\'/>") ) );
	Check( xml.FindElem() );
	Check( xml.GetAttrib(MCD_T("d")) == MCD_T("d\'d") );
	Check( xml.GetAttrib(MCD_T("s")) == MCD_T("s\"s") );
	Check( xml.SetAttrib( MCD_T("d"), MCD_T("d\'d") ) );
	Check( xml.GetAttrib(MCD_T("d")) == MCD_T("d\'d") );
	Check( xml.GetAttrib(MCD_T("s")) == MCD_T("s\"s") );
	Check( xml.GetAttrib(MCD_T("g1")) == MCD_T(">") );
	Check( xml.GetAttrib(MCD_T("g2")) == MCD_T(">") );

	// FindElem Tag Name Test
	StartCheckZone( MCD_T("FindElem Tag Name Test") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("R") );
	xml.AddChildElem( MCD_T("N"), MCD_T("A") );
	xml.AddChildElem( MCD_T("Name"), MCD_T("B") );
	xml.AddChildElem( MCD_T("Na"), MCD_T("C") );
	xml.AddChildElem( MCD_T("Nam"), MCD_T("D") );
	xml.ResetChildPos();
	Check( xml.FindChildElem(MCD_T("Name")) );
	Check( xml.GetChildData() == MCD_T("B") );
	Check( xml.FindChildElem(MCD_T("Nam")) );
	Check( xml.GetChildData() == MCD_T("D") );

	// Copy Constructor Heap Test, requires #include <afx.h>
#if ! defined(MARKUP_MSXML) && ! defined(MARKUP_STL)
	StartCheckZone( MCD_T("Copy Constructor Heap Test") );
	Check( AfxCheckMemory() );
	CMarkup xmlTest(xml);
	Check( AfxCheckMemory() );
	xmlTest.AddElem( MCD_T("AfterCopyEmpty") );
	Check( AfxCheckMemory() );
	xmlTest.SetDoc( NULL );
	xml = xmlTest;
	xml.AddElem( MCD_T("AfterCopyEmpty") );
	Check( AfxCheckMemory() );
#endif // not MSXML

	// CDATA Section Test
	StartCheckZone( MCD_T("CDATA Section Test") );
	xml.SetDoc( NULL );
	Check( xml.AddElem( MCD_T("R") ) );
	Check( xml.AddChildElem( MCD_T("C") ) );
	Check( xml.SetChildData( MCD_T("a]]>b]]>c]]>d"), 1 ) );
	Check( xml.GetChildData() == MCD_T("a]]>b]]>c]]>d") );
	Check( xml.IntoElem() );
	Check( xml.SetData( MCD_T("]]>"), 1 ) );
	Check( xml.GetData() == MCD_T("]]>") );
	MCD_STR strCDATASubDoc = MCD_T("<C><![CDATA[a]]>]]&gt;<![CDATA[a]]></C>");
	Check( xml.AddSubDoc( strCDATASubDoc ) );
	Check( xml.GetData() == MCD_T("a]]>a") );

	// Underscore tag name check
	StartCheckZone( MCD_T("Tag Name Test") );
	xml.SetDoc( NULL );
	Check( xml.AddElem( MCD_T("_Underscore") ) );
	MCD_STR strTagNameDoc = xml.GetDoc();
	Check( xml.SetDoc( strTagNameDoc ) );
	xml.FindElem();
	Check( xml.GetTagName() == MCD_T("_Underscore") );

	// ORDER Test, simple XML example
	StartCheckZone( MCD_T("ORDER Test") );
	xml.SetDoc( NULL );
	Check( xml.AddElem( MCD_T("ORDER") ) );
	Check( xml.IntoElem() );
	Check( xml.AddElem( MCD_T("ITEM") ) );
	Check( xml.AddChildElem( MCD_T("NAME"), MCD_T("carrots") ) );
	Check( xml.AddChildElem( MCD_T("QTY"), MCD_T("1") ) );
	Check( xml.AddChildElem( MCD_T("PRICE"), MCD_T(".98") ) );
	Check( xml.AddChildAttrib( MCD_T("unit"), MCD_T("1 lb") ) );
	Check( xml.AddElem( MCD_T("ITEM") ) );
	Check( xml.AddChildElem( MCD_T("NAME"), MCD_T("onions") ) );
	Check( xml.AddChildElem( MCD_T("QTY"), MCD_T("1") ) );
	Check( xml.AddChildElem( MCD_T("PRICE"), MCD_T("1.10") ) );
	Check( xml.AddChildAttrib( MCD_T("unit"), MCD_T("3 lb bag") ) );
	Check( xml.AddChildElem( MCD_T("SUPPLIER"), MCD_T("Hanover") ) );

	// Create List
	StartCheckZone( MCD_T("List Test") );
	MCD_STR strList;
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("ORDER") ) );
	while ( xml.FindChildElem( MCD_T("ITEM") ) )
	{
		xml.IntoElem();
		xml.FindChildElem( MCD_T("NAME") );
		strList += xml.GetChildData();
		strList += MCD_T("\n");
#if ! defined(MARKUP_MSXML)
		xml.SavePos();
#endif // not MSXML
		xml.OutOfElem();
	}
#if ! defined(MARKUP_MSXML)
	xml.RestorePos();
	Check( xml.GetChildData() == MCD_T("onions") );
#endif // not MSXML
	Check( strList == MCD_T("carrots\nonions\n") );

	// December Test, add and remove five elements repeatedly
	StartCheckZone( MCD_T("December Test") );
	struct TestData { MCD_PCSZ szName; MCD_PCSZ szValue; } td[] =
	{
		{ MCD_T("patridge"), MCD_T("PEARTREE") },
		{ MCD_T("2turtle"), MCD_T("DOVES") },
		{ MCD_T("3french"), MCD_T("HENS") },
		{ MCD_T("4calling"), MCD_T("BIRDS") },
		{ MCD_T("5golden"), MCD_T("RINGS") },
	};
	int nTD;
	xml.SetDoc( MCD_T("") );
	Check( xml.AddElem( MCD_T("RESULT") ) );
	for ( nTD=0; nTD<=4; ++nTD )
	{
		xml.AddChildElem( MCD_T("VAR"), td[nTD].szValue );
		xml.SetChildAttrib( MCD_T("name"), td[nTD].szName );
	}
	for ( nTD=0; nTD<=4; ++nTD )
	{
		xml.ResetPos();
		while ( xml.FindChildElem( MCD_T("VAR") ) )
			if ( xml.GetChildAttrib( MCD_T("name") ) == td[nTD].szName )
			{
				xml.RemoveChildElem();
				break;
			}
		xml.AddChildElem( MCD_T("VAR"), td[nTD].szValue );
		xml.SetChildAttrib( MCD_T("name"), td[nTD].szName );
	}
	for ( nTD=4; nTD>=0; --nTD )
	{
		xml.ResetPos();
		while ( xml.FindChildElem( MCD_T("VAR") ) )
			if ( xml.GetChildAttrib( MCD_T("name") ) == td[nTD].szName )
			{
				xml.RemoveChildElem();
				break;
			}
		xml.ResetChildPos();
		xml.AddChildElem( MCD_T("VAR"), td[nTD].szValue );
		xml.SetChildAttrib( MCD_T("name"), td[nTD].szName );
	}
	// <RESULT>
	// <VAR name="5golden">RINGS</VAR>
	// <VAR name="4calling">BIRDS</VAR>
	// <VAR name="3french">HENS</VAR>
	// <VAR name="2turtle">DOVES</VAR>
	// <VAR name="patridge">PEARTREE</VAR>
	// </RESULT>
	xml.ResetPos();
	for ( nTD=4; nTD>=0; --nTD )
	{
		Check( xml.FindChildElem() );
		Check( xml.GetChildData() == td[nTD].szValue );
		Check( xml.GetChildAttrib( MCD_T("name") ) == td[nTD].szName );
	}

	// Add Insert Test, test siblings created in correct sequence
	MCD_PCSZ aszNums[] = { MCD_T("zero"),MCD_T("one"),MCD_T("two"),MCD_T("three"),MCD_T("four"),MCD_T("five"),MCD_T("six") };
	MCD_STR strInside = MCD_T("_in6");
	StartCheckZone( MCD_T("Insert Test") );
	xml.SetDoc( MCD_T("") );
	xml.AddElem( MCD_T("root") );
	xml.AddChildElem( aszNums[1] );
	xml.ResetChildPos();
	xml.AddChildElem( aszNums[3] );
	xml.ResetChildPos();
	xml.FindChildElem();
	xml.InsertChildElem( aszNums[0] );
	xml.RemoveChildElem();
	xml.InsertChildElem( aszNums[0] );
	xml.FindChildElem();
	xml.AddChildElem( aszNums[2] );
	xml.FindChildElem();
	xml.AddChildElem( aszNums[4] );
	xml.FindChildElem();
	xml.AddChildElem( aszNums[5] );
	xml.ResetChildPos();
	xml.IntoElem();
	xml.FindElem();
	xml.RemoveElem();
	xml.InsertElem( aszNums[0] );
	xml.ResetMainPos();
	xml.AddElem( aszNums[6] );
	xml.IntoElem();
	xml.AddElem( aszNums[1] + strInside );
	xml.ResetMainPos();
	xml.AddElem( aszNums[3] + strInside );
	xml.ResetMainPos();
	xml.FindElem();
	xml.InsertElem( aszNums[0] + strInside );
	xml.RemoveElem();
	xml.ResetMainPos();
	xml.InsertElem( aszNums[0] + strInside );
	xml.FindElem();
	xml.AddElem( aszNums[2] + strInside );
	xml.FindElem();
	xml.AddElem( aszNums[4] + strInside );
	xml.FindElem();
	xml.AddElem( aszNums[5] + strInside );
	// Should result in the following (indented to illustrate):
	// <root>
	//   <zero/>
	//   <one/>
	//   <two/>
	//   <three/>
	//   <four/>
	//   <five/>
	//   <six>
	//     <zero_in6/>
	//     <one_in6/>
	//     <two_in6/>
	//     <three_in6/>
	//     <four_in6/>
	//     <five_in6/>
	//   </six>
	// </root>
	xml.ResetPos();
	for ( int nIT=0; nIT<7; ++nIT )
	{
		Check( xml.FindChildElem() );
		Check( xml.IntoElem() );
		Check( xml.GetTagName() == aszNums[nIT] );
		if ( nIT == 6 )
		{
			for ( int nITinner=0; nITinner<6; ++nITinner )
			{
				Check( xml.FindChildElem() );
				Check( xml.GetChildTagName() == aszNums[nITinner]+strInside );
			}
		}
		Check( xml.OutOfElem() );
	}

	// Palmer Test
	StartCheckZone( MCD_T("Palmer Test") );
	xml.SetDoc( MCD_T("") );
	Check( xml.AddElem( MCD_T("one") ) );
	Check( xml.AddChildElem( MCD_T("two") ) );
	Check( xml.RemoveChildElem() );
	Check( xml.AddChildElem( MCD_T("three") ) );
	// <one>
	// <three/>
	// </one>
	xml.ResetPos();
	Check( xml.FindChildElem() );
	Check( xml.GetChildTagName() == MCD_T("three") );

	// SetData Test, check empty elements, normal, and CDATA
	StartCheckZone( MCD_T("SetData Test") );
	Check( xml.SetDoc( MCD_T("<?xml version=\"1.0\"?>\r\n<ROOT>data</ROOT>") ) );
	Check( xml.FindElem() );
	xml.SetData( MCD_T("moredata<>&") );
	Check( xml.GetData() == MCD_T("moredata<>&") );
	xml.SetData( MCD_T("n<6"), 1 );
	Check( xml.GetData() == MCD_T("n<6") );
	xml.SetData( MCD_T("") );
	xml.AddChildElem( MCD_T("CHILD") );
	xml.SetChildData( MCD_T("data<>&") );
	Check( xml.GetChildData() == MCD_T("data<>&") );
	xml.SetChildData( MCD_T("n<6"), 1 );
	xml.InsertChildElem( MCD_T("CHILD") );
	xml.IntoElem();
	Check( xml.SetData(MCD_T("")) );
	xml.SetData( MCD_T("final") );
	Check( xml.GetData() == MCD_T("final") );
	xml.FindElem();
	Check( xml.GetData() == MCD_T("n<6") );

	// Car Test, add and replace randomly chosen attributes
	StartCheckZone( MCD_T("Random Car Part Test") );
	struct CarTestData { MCD_PCSZ szLow; MCD_PCSZ szUp; MCD_PCSZ szMix; } aCT[] =
	{
		{ MCD_T("up"), MCD_T("SKY"), MCD_T("Light") },
		{ MCD_T("down"), MCD_T("FLOOR"), MCD_T("Dust") },
		{ MCD_T("left"), MCD_T("DOOR"), MCD_T("Handle") },
		{ MCD_T("right"), MCD_T("SEAT"), MCD_T("Gear") },
		{ MCD_T("back"), MCD_T("TRUNK"), MCD_T("Tread") },
		{ MCD_T("forward"), MCD_T("GRILL"), MCD_T("Motor") },
		{ MCD_T(""), MCD_T(""), MCD_T("") }
	};
	xml.SetDoc( MCD_T("") );
	xml.AddElem( MCD_T("CAR") );
	int nAt, nPart;
	MCD_PCSZ szLow;
	MCD_PCSZ szUp;
	MCD_PCSZ szMix;
	for ( nAt = 0; nAt < 20; ++nAt )
	{
		szLow = aCT[RandInt(6)].szLow;
		szMix = aCT[RandInt(6)].szMix;
		xml.SetAttrib( szLow, szMix );
		Check( xml.GetAttrib(szLow) == szMix );
	}
	for ( nPart=0; nPart<100; ++nPart )
	{
		xml.AddChildElem( aCT[RandInt(6)].szUp );
		for ( nAt = 0; nAt < 8; ++nAt )
		{
			szLow = aCT[RandInt(6)].szLow;
			szMix = aCT[RandInt(6)].szMix;
			xml.SetChildAttrib( szLow, szMix );
			Check( xml.GetChildAttrib(szLow) == szMix );
		}
		szLow = aCT[RandInt(6)].szLow;
		szUp = aCT[RandInt(6)].szUp;
		xml.SetChildAttrib( szLow, szUp );
		Check( xml.GetChildAttrib(szLow) == szUp );
	}
	MCD_STR strCarDoc = xml.GetDoc();
	Check( xml.SetDoc( strCarDoc ) );

	// Car Attrib Speed Test
	const int nPerfTestAttribElems = 10000; // controls size of test
	StartCheckZone( MCD_T("Car Attrib Speed Test") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("CARSPEED") );
	nTimeStart = GetMilliCount();
	for ( nPart=0; nPart<nPerfTestAttribElems; ++nPart )
	{
		xml.AddChildElem( aCT[RandInt(6)].szUp );
		for ( nAt = 0; nAt < 4; ++nAt )
		{
			szLow = aCT[RandInt(6)].szLow;
			szMix = aCT[RandInt(6)].szMix;
			xml.SetChildAttrib( szLow, szMix );
			szLow = aCT[RandInt(6)].szLow;
			szUp = aCT[RandInt(6)].szUp;
			xml.SetChildAttrib( szLow, szUp );
		}
	}
	int nAttribCreateTime = GetMilliSpan( nTimeStart );
	int nPerfAttribLength = 0;
	xml.ResetPos();
	nTimeStart = GetMilliCount();
	MCD_STR strAttribName, strAttribs;
	while ( xml.FindChildElem() )
	{
		xml.IntoElem();
		nAt = 0;
		MCD_STRCLEAR(strAttribs);
		strAttribName = xml.GetAttribName( nAt );
		while ( ! MCD_STRISEMPTY(strAttribName) )
		{
			strAttribs += xml.GetAttrib( strAttribName );
			++nAt;
			strAttribName = xml.GetAttribName( nAt );
			nPerfAttribLength += MCD_STRLENGTH(strAttribName) + 4;
		}
		nPerfAttribLength += MCD_STRLENGTH(xml.GetTagName()) + 5 + MCD_STRLENGTH(strAttribs) + nAt*3 + (nAt-1);
		xml.OutOfElem();
	}
	int nAttribNavTime = GetMilliSpan( nTimeStart );
	m_nAttribParseSpeed = nAttribNavTime? nPerfAttribLength/nAttribNavTime: 0;
	m_nAttribCreateSpeed = nAttribCreateTime? nPerfAttribLength/nAttribCreateTime: 0;

	// SubDoc Test, use subdocument methods
	StartCheckZone( MCD_T("SubDoc Test") );
	MCD_STR strTestAttrib = MCD_T("<<&\"symb>>");
	MCD_STR strSubDoc, strChildSubDoc;
	xml.SetDoc( MCD_T("") );
	Check( xml.AddElem(MCD_T("ORDER")) );
	xml.AddChildElem( MCD_T("ITEM") );
	xml.IntoElem();
	Check( xml.AddChildSubDoc(MCD_T("<?xml version=\"1.0\"?>\r\n<ITEM/>\r\n")) );
	Check( xml.AddChildAttrib(MCD_T("test"), strTestAttrib) );
	Check( xml.IntoElem() );
	Check( xml.AddChildElem(MCD_T("NAME"), MCD_T("carrots")) );
	Check( xml.AddChildElem(MCD_T("QTY"), MCD_T("1")) );
	strSubDoc = xml.GetSubDoc();
	Check( xml.OutOfElem() );
	strChildSubDoc = xml.GetChildSubDoc();
	Check( strSubDoc == strChildSubDoc );
	// Set xml2 from xml subdocument and modify
	Check( xml2.SetDoc(xml.GetChildSubDoc()) );
	Check( xml2.FindElem() );
	Check( xml2.SetAttrib(MCD_T("subtest"), strTestAttrib) );
	Check( xml2.SetAttrib(MCD_T("test"), MCD_T("")) );
	Check( xml2.FindChildElem(MCD_T("QTY")) );
	Check( xml2.RemoveChildElem() );
	strSubDoc = xml2.GetDoc();
#if ! defined(MARKUP_MSXML)
	// MSXML: Invalid subdocument causes first-chance exception
	// It works, but this #ifdef avoids alarming us with the Debug output
	Check( ! xml.AddChildSubDoc(MCD_T("invalidsub<doc>")) );
#endif // not MSXML
	Check( xml.AddChildSubDoc(strSubDoc) );
	Check( xml.IntoElem() );
	Check( xml.RemoveElem() );
	Check( xml.AddSubDoc(strSubDoc) );
	Check( xml.GetAttrib(MCD_T("subtest")) == strTestAttrib );
	Check( xml.OutOfElem() );
	Check( xml.InsertChildSubDoc(strSubDoc) );
	xml.ResetChildPos();
	Check( xml.InsertChildSubDoc(strSubDoc) );
	xml.IntoElem();
	xml.FindChildElem();
	Check( xml.GetChildData() == MCD_T("carrots") );
	// Brannan Test: subdocument with no children getting flagged as empty in 7.0
	xml.ResetChildPos();
	xml.FindElem();
	xml.FindChildElem( MCD_T("QTY") );
	xml.AddChildSubDoc( xml.GetChildSubDoc() );
	Check( xml.GetChildData() == MCD_T("1") );

	// Comments Test, make sure it bypasses declaration, DTD, and comments
	StartCheckZone( MCD_T("Comments Test") );
	MCD_STR strPTDoc =
		MCD_T("<?xml version=\"1.0\"?>\n")
		MCD_T("<!DOCTYPE PARSETEST [\n")
		MCD_T("<!ELEMENT PARSETEST (ITEM*)>\n")
		MCD_T("<!ATTLIST PARSETEST v CDATA \"\" s CDATA \"\">\n")
		MCD_T("<!ELEMENT ITEM ANY>\n")
		MCD_T("<!ATTLIST ITEM note CDATA \"\">\n")
		MCD_T("]>\n")
		MCD_T("<PARSETEST v=\"1\" s=\'6\'>\n")
			MCD_T("<!--- </P>& special chars -->\n")
			MCD_T("<ITEM note=\"Here&apos;s to &quot;us&quot;\"/>\n")
			MCD_T("<ITEM note=\"see data\">Here's to \"us\"</ITEM>\n")
		MCD_T("</PARSETEST>\n")
		;
	Check( xml.SetDoc( strPTDoc ) );
	Check( xml.FindChildElem() );
	Check( xml.GetAttrib( MCD_T("v") ) == MCD_T("1") );
	Check( xml.GetAttribName(0) == MCD_T("v") );
	Check( xml.GetAttribName(1) == MCD_T("s") );
	Check( xml.GetAttribName(2) == MCD_T("") );
	Check( xml.GetChildAttrib(MCD_T("note")) == MCD_T("Here's to \"us\"") );
	Check( xml.IntoElem() );
	Check( xml.FindElem() );
	Check( xml.GetData() == MCD_T("Here's to \"us\"") );

#if ! defined(MARKUP_MSXML)
	// Save Restore Position Test
	StartCheckZone( MCD_T("Save Restore Position Test") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("SavePosTest") );
	Check( ! xml.RestorePos() ); // crash in 6.6 and 7.x when no saved pos yet
	Check( xml.SavePos(MCD_T("\xD7")) ); // crash in 7.0 to 8.2 with non-ASCII
	xml.IntoElem(); // no main position
	xml.SavePos();
	xml.ResetPos();
	xml.RestorePos();
	Check( xml.OutOfElem() );
	Check( xml.GetTagName() == MCD_T("SavePosTest") );
	CMarkup mPosNames;
	int nSavePos;
	const int nSavePosTestSize = 100;
	for ( nSavePos=0; nSavePos<nSavePosTestSize; ++nSavePos )
	{
		// Find an unused random name
		MCD_STR strPosName;
		while ( 1 )
		{
			int nPosNameLen = RandInt(10)+1;
			for ( int nPosNameChar=0; nPosNameChar<nPosNameLen; ++nPosNameChar )
				strPosName += (MCD_CHAR)('A'+RandInt(26));
			if ( ! xml.RestorePos(strPosName) )
				break;
		}
		xml.AddChildElem( strPosName );
		xml.SavePos( strPosName );
		mPosNames.AddElem( MCD_T("P"), strPosName );
	}
	xml2 = xml;
	xml = xml2;
	int nPosNameCount = nSavePos;
	mPosNames.ResetPos();
	for ( nSavePos=0; nSavePos<nPosNameCount; ++nSavePos )
	{
		mPosNames.FindElem();
		MCD_STR strPosName = mPosNames.GetData();
		Check( xml.RestorePos(strPosName) );
		Check( xml.GetChildTagName() == strPosName );

		// Move saved position to TEMP2 child
		Check( xml.IntoElem() );
		Check( xml.AddChildElem( MCD_T("TEMP") ) );
		Check( xml.IntoElem() );
		Check( xml.AddChildElem( MCD_T("TEMP2") ) );
		Check( xml.IntoElem() );
		xml.SavePos( strPosName );
		if ( RandInt(2) ) // 1 in 2
			Check( xml.OutOfElem() );
		if ( RandInt(2) ) // 1 in 2
			Check( xml.OutOfElem() );
		if ( nSavePos % 2 )
			xml.RemoveElem();
	}
	mPosNames.ResetPos();
	for ( nSavePos=0; nSavePos<nPosNameCount; ++nSavePos )
	{
		mPosNames.FindElem();
		MCD_STR strPosName = mPosNames.GetData();
		if ( nSavePos % 2 )
			Check( ! xml.RestorePos(strPosName) );
		else
		{
			Check( xml.RestorePos(strPosName) );
			Check( xml.OutOfElem() );
			Check( xml.OutOfElem() );
			Check( xml.GetTagName() == strPosName );
		}
	}

	// Save Restore Example
	int anSomeData[] = { 10, 30, 50, 20, 90, 0 };
	int nD;
	StartCheckZone( MCD_T("Save Restore Example") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("SavePosExample") );
	xml.IntoElem(); // no main position
	xml.AddElem( MCD_T("Total") );
	xml.SavePos( MCD_T("Total") );
	xml.AddElem( MCD_T("Data") );
	xml.SavePos( MCD_T("Data") );
	for ( nD=0; anSomeData[nD]; ++nD )
	{
		xml.RestorePos( MCD_T("Total") );
		xml.SetData( MCD_STRTOINT(xml.GetData()) + anSomeData[nD] );
		xml.RestorePos( MCD_T("Data") );
		xml.AddChildElem( MCD_T("Value"), anSomeData[nD] );
	}
	xml2.SetDoc( NULL );
	xml2.AddElem( MCD_T("SavePosExample") );
	xml2.IntoElem();
	xml2.AddElem( MCD_T("Total") );
	xml2.AddElem( MCD_T("Data") );
	for ( nD=0; anSomeData[nD]; ++nD )
	{
		xml2.ResetMainPos();
		xml2.FindElem();
		xml2.SetData( MCD_STRTOINT(xml2.GetData()) + anSomeData[nD] );
		xml2.FindElem();
		xml2.AddChildElem( MCD_T("Value"), anSomeData[nD] );
	}
	Check( xml.GetDoc() == xml2.GetDoc() );

	// SetMapSize Test with 4 different maps and random values
	StartCheckZone( MCD_T("SetMapSize Test") );
	xml.SetDoc( MCD_T("") );
	xml.AddElem( MCD_T("MAP") );
	xml.IntoElem();
	Check( xml.SetMapSize( 503 ) );
	Check( xml.SetMapSize( 1009, 1 ) );
	Check( xml.SetMapSize( 10007, 2 ) );
	Check( xml.SetMapSize( 53, 3 ) );
	int nMap, nMapItem, nMapTestVal, nMapItemExtra;
	MCD_CHAR szMapTest[25];
	int anMapTestTotal[4] = { 0,0,0,0 };
	int nValCollisions = 0;
	for ( nMap=0; nMap<4; ++nMap )
	{
		for ( nMapItem=0; nMapItem<100; ++nMapItem )
		{
			nMapTestVal = RandInt(100);
			MCD_SPRINTF( MCD_SSZ(szMapTest), MCD_T("%d"), nMapTestVal );
			anMapTestTotal[nMap] += nMapTestVal;
			if ( xml.RestorePos(szMapTest,nMap) )
			{
				++nValCollisions;
				nMapItemExtra = MCD_STRTOINT(xml.GetAttrib(MCD_T("e")));
				xml.SetAttrib( MCD_T("e"), nMapItemExtra + 1 );
				xml.ResetMainPos();
			}
			else
			{
				xml.AddElem( MCD_T("Item"), nMapTestVal );
				xml.SetAttrib( MCD_T("m"), nMap );
				xml.SavePos( szMapTest, nMap );
			}
		}
	}
	int anMapTestCheck[4] = { 0,0,0,0 };
	int nValCollisionsCheck = 0;
	xml.ResetMainPos();
	while ( xml.FindElem() )
	{
		nMap = MCD_STRTOINT(xml.GetAttrib(MCD_T("m")));
		nMapItemExtra = MCD_STRTOINT(xml.GetAttrib(MCD_T("e")));
		nValCollisionsCheck += nMapItemExtra;
		nMapTestVal = MCD_STRTOINT(xml.GetData());
		anMapTestCheck[nMap] += nMapTestVal*(nMapItemExtra+1);
	}
	for ( nMap=0; nMap<4; ++nMap )
		Check( anMapTestCheck[nMap] == anMapTestTotal[nMap] );
	Check( nValCollisionsCheck == nValCollisions );

	// Prepend XML Declaration test
	// (not implemented in MSXML because modifying XML Declaration is restricted)
	StartCheckZone( MCD_T("Prepend XML Declaration") );
	Check( ! xml.SetDoc( MCD_T("<?xml version=\"1.0\"?>\r\n") ) );
	Check( xml.AddElem( MCD_T("ROOT") ) );
	Check( xml.IntoElem() );
	Check( xml.AddElem( MCD_T("CHILD") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("ROOT") ) );
	Check( xml.FindChildElem( MCD_T("CHILD") ) );

	// Copy Constructor Test (not MSXML!)
	StartCheckZone( MCD_T("Copy Constructor Test non-MSXML") );
	xml.SetDoc( NULL );
	xml2 = xml;
	xml.AddElem( MCD_T("R") );
	xml.AddChildElem( MCD_T("C") );
	xml2 = xml;
	xml2.FindChildElem();
	Check( xml2.GetChildTagName() == xml.GetChildTagName() );
	xml.IntoElem();
	xml.SavePos();
	xml.RemoveElem();
	xml2 = xml;
	Check( ! xml2.RestorePos() );

	// Ill-formed Markup Test
	StartCheckZone( MCD_T("Ill-formed Markup Test") );
	Check( ! xml.SetDoc( MCD_T("<P>Hello<BR>Hi") ) );
	mResult.SetDoc( xml.GetResult() );
	Check( mResult.FindElem(MCD_T("unended_start_tag")) );
	Check( mResult.FindElem(MCD_T("root_has_sibling")) );
	Check( xml.FindElem( MCD_T("P") ) );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("Hello") );
	Check( xml.FindElem( MCD_T("BR") ) );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("Hi") );
	Check( xml.AddElem( MCD_T("P"), MCD_T("Hola") ) );
	Check( xml.GetData() == MCD_T("Hola") );
	Check( ! xml.SetDoc( MCD_T("<P>Hello<BR>Hi</P>") ) );
	Check( xml.FindElem( MCD_T("P") ) );
	//[CMARKUPDEV
	int nStartElem, nLenElem;
	Check( xml.GetOffsets( &nStartElem, &nLenElem ) );
	Check( nStartElem == 0 );
	Check( nLenElem == 18 );
	Check( ! xml.SetDoc( MCD_T("<A><B><C><C></B></A>") ) );
	Check( xml.FindElem() );
	Check( xml.FindChildElem() );
	Check( xml.IntoElem() );
	Check( xml.GetOffsets( &nStartElem, &nLenElem ) );
	Check( nStartElem == 3 );
	Check( nLenElem == 13 );
	//]CMARKUPDEV
	xml.SetDocFlags( xml.MDF_IGNORECASE );
	Check( xml.SetDoc( MCD_T("<A></a>") ) );
	Check( xml.FindElem( MCD_T("a") ) );
	Check( xml.SetAttrib( MCD_T("a1"), MCD_T("V1") ) );
	Check( xml.SetAttrib( MCD_T("A1"), MCD_T("v1") ) );
	Check( ! xml.AddSubDoc( MCD_T("<b>") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("a") ) );
	Check( xml.GetAttrib(MCD_T("A1")) == MCD_T("v1") );
	Check( xml.GetAttrib(MCD_T("a1")) == MCD_T("v1") );
	Check( xml.FindElem( MCD_T("B") ) );
	xml.SetDocFlags( 0 );
	Check( ! xml.SetDoc( MCD_T("<A><B></C></B></A>") ) );
	Check( xml.FindElem() );
	Check( xml.IntoElem() );
	Check( xml.FindElem( MCD_T("B") ) );
	Check( xml.IntoElem() );
	Check( xml.FindNode() == xml.MNT_LONE_END_TAG );
	Check( xml.GetTagName() == MCD_T("C") );
	Check( xml.GetData() == MCD_T("C") );
	Check( xml.OutOfElem() );
	Check( xml.SetElemContent( MCD_T("data") ) );
	Check( ! xml.InsertSubDoc( MCD_T("<S/><S/>") ) );
	Check( ! xml.SetElemContent( MCD_T("<SC>") ) );
	Check( xml.FindElem( MCD_T("S") ) );

	// Multi-Rooted Subdoc Test
	StartCheckZone( MCD_T("Multi-Rooted Subdoc Test") );
	Check( xml.SetElemContent( MCD_T("<SC><SCC/></SC><SC><SCC/></SC><SC/>") ) );
	Check( xml.FindChildElem( MCD_T("SC") ) );
	Check( xml.SetChildAttrib( MCD_T("asc"), MCD_T("verbose") ) );
	Check( xml.IntoElem() );
	Check( xml.FindChildElem( MCD_T("SCC") ) );
	Check( xml.SetChildAttrib( MCD_T("ascc"), MCD_T("verbose") ) );
	Check( xml.OutOfElem() );
	xml.ResetMainPos();
	Check( xml.FindElem( MCD_T("S") ) );
	Check( xml.FindChildElem( MCD_T("SC") ) );
	Check( xml.SetChildAttrib( MCD_T("asc"), MCD_T("verbose") ) );
	Check( xml.SetChildAttrib( MCD_T("asc2"), 2 ) );
	Check( xml.FindElem( MCD_T("S") ) );
	Check( xml.FindChildElem( MCD_T("SC") ) );
	Check( xml.FindChildElem( MCD_T("SC") ) );
	Check( xml.FindElem( MCD_T("B") ) );

	// Trim and Collapse Whitespace Test
	StartCheckZone( MCD_T("Trim and Collapse Whitespace Test") );
	Check( xml.SetDoc( MCD_T("<T><A a=' o  k ' b='o' c=' ok' d='o\tk' e=' ok' f='' g='\t'>\r\ntwo\r\n\twords\t</A></T>") ) );
	xml.SetDocFlags( xml.MDF_TRIMWHITESPACE );
	Check( xml.FindChildElem() );
	Check( xml.IntoElem() );
	Check( xml.GetAttrib(MCD_T("a")) == MCD_T("o  k") );
	Check( xml.GetAttrib(MCD_T("b")) == MCD_T("o") );
	Check( xml.GetAttrib(MCD_T("c")) == MCD_T("ok") );
	Check( xml.GetAttrib(MCD_T("d")) == MCD_T("o\tk") );
	Check( xml.GetAttrib(MCD_T("e")) == MCD_T("ok") );
	Check( xml.GetAttrib(MCD_T("f")) == MCD_T("") );
	Check( xml.GetAttrib(MCD_T("g")) == MCD_T("") );
	Check( xml.GetData() == MCD_T("two\r\n\twords") );
	Check( xml.IntoElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("two\r\n\twords") );
	Check( xml.OutOfElem() );
	//[CMARKUPDEV
	xml.ResetPos();
	Check( xml.FindElem(MCD_T("//A[@a='o  k']")) );
	//]CMARKUPDEV
	xml.SetDocFlags( xml.MDF_COLLAPSEWHITESPACE );
	Check( xml.GetAttrib(MCD_T("a")) == MCD_T("o k") );
	Check( xml.GetAttrib(MCD_T("b")) == MCD_T("o") );
	Check( xml.GetAttrib(MCD_T("c")) == MCD_T("ok") );
	Check( xml.GetAttrib(MCD_T("d")) == MCD_T("o k") );
	Check( xml.GetAttrib(MCD_T("e")) == MCD_T("ok") );
	Check( xml.GetAttrib(MCD_T("f")) == MCD_T("") );
	Check( xml.GetAttrib(MCD_T("g")) == MCD_T("") );
	Check( xml.GetData() == MCD_T("two words") );
	//[CMARKUPDEV
	xml.ResetPos();
	Check( xml.FindElem(MCD_T("//A[@d='o k']")) );
	//]CMARKUPDEV
	xml.SetDocFlags( 0 );

	// GetNthAttrib Test
	MCD_STR strAttrib, strValue;
	StartCheckZone( MCD_T("GetNthAttrib Test") );
	Check( xml.SetDoc( MCD_T("<T a0='0' a1 a2='2&apos;'/>") ) );
	Check( ! xml.GetNthAttrib(0,strAttrib,strValue) );
	Check( xml.FindElem() );
	Check( xml.GetNthAttrib(0,strAttrib,strValue) );
	Check( xml.GetAttribName(0) == strAttrib );
	Check( xml.GetAttrib(strAttrib) == strValue );
	Check( xml.GetNthAttrib(1,strAttrib,strValue) );
	Check( xml.GetAttribName(1) == strAttrib );
	Check( xml.GetAttrib(strAttrib) == strValue );
	Check( xml.GetNthAttrib(2,strAttrib,strValue) );
	Check( xml.GetAttribName(2) == strAttrib );
	Check( xml.GetAttrib(strAttrib) == strValue );
	Check( strValue == MCD_T("2'") );

	// Don't mistake end slash on non-quoted value for empty element (see cDminus2)
	StartCheckZone( MCD_T("Non Quoted Attribute Test") );
	Check( xml.SetDoc( MCD_T("<A href=/path/>ok</A>") ) );
	Check( xml.FindElem() );
	Check( xml.GetAttrib(MCD_T("href")) == MCD_T("/path/") );

	// AddElem Flags Test
	StartCheckZone( MCD_T("AddElem Flags Test") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("ROOT"), MCD_T(""), xml.MNF_WITHNOLINES );
	xml.AddChildElem( MCD_T("A"), MCD_T(""), xml.MNF_WITHNOEND|xml.MNF_WITHNOLINES );
	Check( xml.IntoElem() );
	Check( xml.AddElem( MCD_T("A"), MCD_T(""), xml.MNF_WITHNOEND ) );
	Check( xml.AddChildElem( MCD_T("B") ) );
	Check( xml.AddChildElem( MCD_T("B"), MCD_T("D") ) );
	Check( xml.OutOfElem() );
	xml.AddChildElem( MCD_T("BR"), MCD_T(""), xml.MNF_WITHXHTMLSPACE );
	xml.SetChildAttrib( MCD_T("class"), MCD_T("MinorBrk") );
	//[CMARKUPDEV
	xml.RemoveChildAttrib( MCD_T("class") );
	xml.SetChildAttrib( MCD_T("class"), MCD_T("MinorBrk") );
	Check( xml.SetDoc( MCD_T("<A\r\n  t='j'/>") ) );
	Check( xml.FindElem() );
	Check( xml.RemoveAttrib( MCD_T("t") ) );
	Check( xml.GetDoc() == MCD_T("<A/>") );
	//]CMARKUPDEV
	Check( ! xml.SetDoc( MCD_T("<A>test") ) );
	Check( xml.FindElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("test") );
	Check( ! xml.SetDoc( MCD_T("<A>test\r\n") ) );
	Check( xml.FindElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( TruncToLastEOL(MCD_2PCSZ(xml.GetData())) == MCD_T("test") );
	Check( ! xml.SetDoc( MCD_T("<A>test\r\n  <B>okay") ) );
	Check( xml.FindElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( TruncToLastEOL(MCD_2PCSZ(xml.GetData())) == MCD_T("test") );
	Check( xml.FindElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( TruncToLastEOL(MCD_2PCSZ(xml.GetData())) == MCD_T("okay") );
	xml.ResetMainPos();
	Check( xml.FindElem() );
	Check( xml.SetData(MCD_T("t2")) );
	Check( xml.GetData() == MCD_T("t2") );
	Check( xml.SetData(MCD_T("test3")) );
	Check( xml.GetData() == MCD_T("test3") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("P"), MCD_T(""), xml.MNF_WITHNOLINES );
	xml.IntoElem();
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("First line") ) );
	Check( xml.AddElem( MCD_T("BR"), MCD_T(""), xml.MNF_WITHNOEND | xml.MNF_WITHNOLINES ) );
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("Second line") ) );
	Check( xml.GetDoc() == MCD_T("<P>First line<BR>Second line</P>") );
	xml.OutOfElem();
	xml.SetElemContent( MCD_T("First line<BR>Second line") );
	Check( xml.GetDoc() == MCD_T("<P>First line<BR>Second line</P>") );

	// Ill-formed SubDoc Fragment Test
	StartCheckZone( MCD_T("SubDoc Fragment Test") );
	Check( ! xml.SetDoc( MCD_T("<A/><B/>") ) );
	Check( ! xml.SetDoc( MCD_T(" something ") ) );
	Check( ! xml.AddSubDoc( MCD_T("<A>") ) );
	Check( ! xml.AddSubDoc( MCD_T("<B>") ) );
	Check( ! xml.AddSubDoc( MCD_T("</A>") ) ); // lone end tag
	mResult.SetDoc( xml.GetResult() );
	Check( mResult.FindElem(MCD_T("lone_end_tag")) );
	Check( xml.AddSubDoc( MCD_T("<C/>") ) );
	Check( ! xml.SetElemContent( MCD_T("<CC>") ) );
	Check( xml.GetTagName() == MCD_T("C") );
	Check( xml.FindChildElem() );
	Check( xml.GetChildTagName() == MCD_T("CC") );
	Check( xml.SetElemContent( MCD_T("d") ) );
	Check( xml.GetData() == MCD_T("d") );
	Check( ! xml.SetElemContent( MCD_T("<DD>") ) );
	Check( xml.FindChildElem() );
	Check( xml.GetChildTagName() == MCD_T("DD") );

	// Replace element between two text nodes
	StartCheckZone( MCD_T("Replace Element Test") );
	Check( xml.SetDoc( MCD_T("T<A/>T") ) );
	Check( xml.FindElem( MCD_T("A") ) );
	Check( xml.RemoveNode() );
	Check( xml.GetNodeType() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("T") );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("B") ) );
	Check( xml.SetAttrib( MCD_T("v"), 1 ) );
	Check( xml.GetTagName() == MCD_T("B") );
	Check( xml.GetAttrib( MCD_T("v") ) == MCD_T("1") );
	Check( xml.IntoElem() );
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("T0") ) );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("C1") ) );
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("T1") ) );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("C2") ) );
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("T2") ) );
	xml.ResetMainPos();
	Check( xml.FindElem( MCD_T("C1") ) );
	Check( xml.RemoveNode() );
	Check( xml.GetNodeType() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("T0") );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("C1") ) );
	Check( xml.FindElem( MCD_T("C2") ) );
	Check( xml.RemoveNode() );
	Check( xml.GetNodeType() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("T1") );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("C2") ) );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("T2") );
	xml.ResetMainPos();
	Check( xml.FindElem( MCD_T("C2") ) );
	Check( xml.IntoElem() );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("D3") ) );
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("T0") ) );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("D4") ) );
	Check( xml.IntoElem() );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("E1") ) );
	Check( xml.SetData( MCD_T("X") ) );
	Check( xml.OutOfElem() );
	Check( xml.OutOfElem() );
	xml.ResetMainPos();
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("C1") );
	Check( xml.IntoElem() );
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("T1") ) );
	Check( xml.AddNode( xml.MNT_ELEMENT, MCD_T("D2") ) );
	Check( xml.AddNode( xml.MNT_TEXT, MCD_T("T2") ) );
	xml.ResetMainPos();
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("D2") );
	xml.ResetMainPos();
	Check( xml.InsertNode( xml.MNT_ELEMENT, MCD_T("D1") ) );
	Check( xml.InsertNode( xml.MNT_TEXT, MCD_T("TEST") ) );
	Check( xml.SetData( MCD_T("T0") ) );
	Check( xml.FindNode() == xml.MNT_ELEMENT );
	Check( xml.SetData( MCD_T("X") ) );
	Check( xml.FindElem( MCD_T("D2") ) );
	//[CMARKUPDEV
	Check( xml.FindPrevElem( MCD_T("D1") ) );
	Check( xml.GetData() == MCD_T("X") );
	//]CMARKUPDEV
	xml.ResetMainPos();
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("T0") );
	Check( xml.OutOfElem() );
	xml.ResetMainPos();
	Check( xml.FindElem( MCD_T("C2") ) );
	Check( xml.IntoElem() );
	Check( xml.FindNode() == xml.MNT_ELEMENT );
	Check( xml.GetTagName() == MCD_T("D3") );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("T0") );
	Check( xml.FindNode() == xml.MNT_ELEMENT );
	Check( xml.GetTagName() == MCD_T("D4") );
	Check( xml.FindChildElem() );
	Check( xml.GetChildData() == MCD_T("X") );

	// With Entities Test
	StartCheckZone( MCD_T("With Entities Test") );
	xml.SetDoc(NULL);
	Check( xml.AddElem( MCD_T("A"), MCD_T("&#x20;"), xml.MNF_WITHREFS ) );
	Check( xml.GetData() == MCD_T(" ") );
	Check( xml.SetAttrib( MCD_T("a"), MCD_T("&#x20;"), xml.MNF_WITHREFS ) );
	Check( xml.GetAttrib(MCD_T("a")) == MCD_T(" ") );
	Check( xml.AddChildElem( MCD_T("CH"), MCD_T("&amp;"), xml.MNF_WITHREFS ) );
	Check( xml.GetChildData() == MCD_T("&") );
	Check( xml.SetChildData( MCD_T("&#32;"), xml.MNF_WITHREFS ) );
	Check( xml.GetChildData() == MCD_T(" ") );
	Check( xml.SetChildData( MCD_T("&"), xml.MNF_WITHREFS ) );
	Check( xml.GetChildData() == MCD_T("&") );
	Check( xml.SetChildData( MCD_T("&&"), xml.MNF_WITHREFS ) );
	Check( xml.GetChildData() == MCD_T("&&") );
	Check( xml.SetChildData( MCD_T("&#3 2"), xml.MNF_WITHREFS ) );
	Check( xml.GetChildData() == MCD_T("&#3 2") );
	Check( xml.EscapeText( MCD_T("&#3 2"), xml.MNF_WITHREFS ) == MCD_T("&amp;#3 2") );
	Check( xml.EscapeText( MCD_T("\""), xml.MNF_ESCAPEQUOTES ) == MCD_T("&quot;") );
	Check( xml.EscapeText( MCD_T("\"") ) == MCD_T("\"") );
	Check( xml.EscapeText( MCD_T("&#32;\'"), xml.MNF_WITHREFS|xml.MNF_ESCAPEQUOTES ) == MCD_T("&#32;&apos;") );

	// Node Error Test -- even gibberish will not stop node navigation
	// But there is no guaranteed node parsing system with gibberish
	StartCheckZone( MCD_T("Node Error Test") );
	Check( ! xml.SetDoc( MCD_T("<) <> <*  ><<>>") ) );
	bool bReachedEndOfGibberish = false;
	int nGibberishNodes = 0;
	while ( nGibberishNodes < 100 )
	{
		int nGNType = xml.FindNode();
		if ( nGNType == 0 )
		{
			bReachedEndOfGibberish = true;
			break;
		}
		else if ( nGNType == xml.MNT_NODE_ERROR )
		{
			MCD_STR strJunkNode = xml.GetData();
			Check( ! MCD_STRISEMPTY(strJunkNode) );
		}
		++nGibberishNodes;
	}
	Check( bReachedEndOfGibberish );

	// Document Creation Performance test (ensure document creation speed does not degrade)
	StartCheckZone( MCD_T("Document Creation Performance Test") );
	xml.SetDoc(NULL);
	int nDocPerfBaseTime = 0;
	for ( int nDocPerfTest = 0; nDocPerfTest < 5; ++nDocPerfTest )
	{
		nTimeStart = GetMilliCount();
		int nDocPerfAdds = 1500;
		while ( nDocPerfAdds-- )
			xml.AddElem( MCD_T("elem"), MCD_T("data") );
		int nDocPerfTime = GetMilliSpan( nTimeStart );
		if ( ! nDocPerfTest )
			nDocPerfBaseTime = nDocPerfTime + nDocPerfTime/5; // +20%
		else
			Check( nDocPerfTime < 80 || nDocPerfTime < nDocPerfBaseTime );
	}

#endif // not MSXML

	// Character Reference Test, check various character decoding issues
	StartCheckZone( MCD_T("Character Reference Test") );
	Check( xml.SetDoc(
		MCD_T("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
		MCD_T("<CHAR v='C&#34;&#x22;S'>\n")
		MCD_T("<CORE>&#60;&#x3c;&#62;&#x3e;&#38;&#x26;&#39;&#x27;&#34;&#x22;</CORE>\n")
		MCD_T("<SPACE xml:space=\"preserve\">&#32;&#x20;&#9;&#x9;&#10;&#xa;</SPACE>\n")
		MCD_T("<REF>&#20013;&#22269;&#20154;</REF>\n")
		MCD_T("</CHAR>\n")
		) );
	Check( xml.FindChildElem(MCD_T("CORE")) );
	Check( xml.GetAttrib( MCD_T("v") ) == MCD_T("C\"\"S") );
	Check( xml.GetChildData() == MCD_T("<<>>&&''\"\"") );
	Check( xml.FindChildElem(MCD_T("SPACE")) );
	Check( xml.GetChildData() == MCD_T("  \t\t\n\n") );
	Check( xml.FindChildElem(MCD_T("REF")) );
#if defined(MARKUP_WCHAR)
	Check( xml.GetChildData() == MCD_T("\x4E2D\x56FD\x4EBA") );
#elif ! defined(MARKUP_MSXML) // not WCHAR and not MSXML
#if defined(MARKUP_MBCS)
#if ! defined(MARKUP_WINCONV)
	setlocale( LC_ALL, "C" );
	if ( setlocale(LC_ALL,".936") ) // is GB2312 DBCS code page available?
		Check( xml.GetChildData() == MCD_T("\xD6\xD0\xB9\xFA\xC8\xCB") ); // DBCS
	else
		Check( xml.GetChildData() == MCD_T("&#20013;&#22269;&#20154;") ); // unmodified
	setlocale( LC_ALL, "" ); // reset to ACP
#endif // not WINCONV
#else // not MBCS
	// UTF-8 (note: MSXML converts chinese character reference to UCS-2, but not UTF-8)
	Check( xml.GetChildData() == MCD_T("\xE4\xB8\xAD\xE5\x9B\xBD\xE4\xBA\xBA") );
#endif // not MBCS
#endif // not WCHAR and not MSXML

#if defined(MARKUP_MBCS)
	// GB2312 Test, check getting and setting of sample GB2312 chars
	StartCheckZone( MCD_T("GB2312 Test") );
	MCD_STR strSampleGB2312Chars = MCD_T("\xD6\xD0\xB9\xFA\xC8\xCB");
	Check( xml.SetDoc(
		MCD_T("<?xml version=\"1.0\" encoding=\"GB2312\"?>\n")
		MCD_T("<CHAR>\xD6\xD0\xB9\xFA\xC8\xCB</CHAR>")
		) );
	Check( xml.FindElem() );
	Check( xml.GetData() == strSampleGB2312Chars );
	Check( xml.SetAttrib(MCD_T("v"),strSampleGB2312Chars) );
	Check( xml.GetAttrib(MCD_T("v")) == strSampleGB2312Chars );
#endif // MBCS

	// Empty Data Test
	StartCheckZone( MCD_T("Empty Data") );
	xml.SetDoc( NULL );
	Check( xml.AddElem(MCD_T("A"),MCD_T("data")) );
	Check( xml.SetData(MCD_T("")) );
	Check( xml.AddChildElem(MCD_T("B"),MCD_T("data")) );
	Check( xml.SetChildData(MCD_T("")) );
	Check( xml.IntoElem() );
	Check( xml.IntoElem() );
	Check( xml.AddElem(MCD_T("C"),MCD_T("data")) );
	Check( xml.OutOfElem() );
	Check( xml.OutOfElem() );
	Check( xml.GetChildTagName() == MCD_T("B") );
	Check( xml.GetTagName() == MCD_T("A") );

	// Depth First Traversal Test, loop through all elements
	StartCheckZone( MCD_T("Depth First Traversal Test") );
	xml.SetDoc( MCD_T("<A><B><C/><C/><C><D/></C></B><B><C/></B></A>") );
	MCD_STR strListOfTagNames;
	bool bFinished = false;
	if ( xml.FindElem() )
		strListOfTagNames = MCD_2PCSZ(xml.GetTagName());
	if ( ! xml.FindChildElem() )
		bFinished = true;
	while ( ! bFinished )
	{
		// Process Element
		xml.IntoElem();
		strListOfTagNames += MCD_2PCSZ(xml.GetTagName());

		// Next element (depth first)
		bool bFound = xml.FindChildElem();
		while ( ! bFound && ! bFinished )
		{
			if ( xml.OutOfElem() )
				bFound = xml.FindChildElem();
			else
				bFinished = true;
		}
	}
	Check( strListOfTagNames == MCD_T("ABCCCDBC") );

	// Depth First Traversal Test 2, include root in loop
	StartCheckZone( MCD_T("Depth First Traversal Test 2") );
	MCD_STRCLEAR( strListOfTagNames );
	bFinished = false;
	xml.ResetPos();
	if ( ! xml.FindElem() )
		bFinished = true;
	while ( ! bFinished )
	{
		// Process element
		MCD_STR strTag = MCD_2PCSZ(xml.GetTagName());
		strListOfTagNames += strTag;

		// Next element (depth first)
		bool bFound = xml.FindChildElem();
		while ( ! bFound && ! bFinished )
		{
			if ( xml.OutOfElem() )
				bFound = xml.FindChildElem();
			else
				bFinished = true;
		}
		if ( bFound )
			xml.IntoElem();
	}
	Check( strListOfTagNames == MCD_T("ABCCCDBC") );

	// Load/Save Test
	StartCheckZone( MCD_T("Load/Save Test") );
	MCD_PCSZ_FILENAME pszLSFileName = MCD_T_FILENAME("CMarkupRunTest.xml");
	m_bLSFileLoaded = xml.Load( pszLSFileName ) ? true: false;
	if ( m_bLSFileLoaded )
	{
		// Load/Save
		xml.FindElem();
		Check( xml.Save(pszLSFileName) );
		Check( xml2.Load(pszLSFileName) );
		Check( xml2.FindElem() );
		Check( xml2.GetTagName() == xml.GetTagName() );
	}

	// Node Test, navigate/modify comments and other nodes
	StartCheckZone( MCD_T("Node Test") );
	strPTDoc =
		MCD_T("<?xml version=\"1.0\"?>\n")

		// To get MSXML to preserve whitespace in mixed content
		// you can use a DTD and an xml:space attribute
		// otherwise MSXML would not have a whitespace node between </B> <I>
		// See "White Space Handling" in the XML Specification
		// Also note the mixed content declaration of ITEM
		MCD_T("<!DOCTYPE PARSETEST [\n")
		MCD_T("<!ELEMENT PARSETEST (ITEM*)>\n")
		MCD_T("<!ATTLIST PARSETEST v CDATA '' s CDATA ''>\n")
		MCD_T("<!ELEMENT ITEM (#PCDATA|B|I)*>\n")
		MCD_T("<!ATTLIST ITEM note CDATA '' xml:space (default|preserve) 'preserve'>\n")
		MCD_T("<!ELEMENT B ANY>\n")
		MCD_T("<!ELEMENT I ANY>\n")
		MCD_T("]>\n")

		MCD_T("<!--tightcomment-->\n")
		MCD_T("<PARSETEST v=\"1\" s=\'6\'>\n")
			MCD_T("<!-- mid comment -->\n")
			MCD_T("<ITEM note=\"hi\"/>\n")
			MCD_T("<ITEM note=\"see data\">hi</ITEM>\n")
			MCD_T("<ITEM> mixed <B>content</B> <I>okay</I></ITEM>\n")
		MCD_T("</PARSETEST>\n")
		MCD_T("<!-- end comment -->\n")
		;
	Check( xml.SetDoc(strPTDoc) );
	Check( xml.FindNode() == xml.MNT_PROCESSING_INSTRUCTION );
	Check( xml.GetData() == MCD_T("xml version=\"1.0\"") );
	Check( xml.FindNode(xml.MNT_COMMENT) );
	Check( xml.GetData() == MCD_T("tightcomment") );
	Check( xml.SetData( MCD_T("comment 1 changed") ) );
	Check( xml.GetData() == MCD_T("comment 1 changed") );
	Check( xml.FindNode(xml.MNT_EXCLUDE_WHITESPACE) == xml.MNT_ELEMENT );
	Check( xml.GetTagName() == MCD_T("PARSETEST") );
	Check( xml.IntoElem() );
	Check( xml.FindNode(xml.MNT_EXCLUDE_WHITESPACE) == xml.MNT_COMMENT );
	Check( xml.GetData() == MCD_T(" mid comment ") );
	Check( xml.FindNode(xml.MNT_EXCLUDE_WHITESPACE) == xml.MNT_ELEMENT );
	Check( xml.FindNode(xml.MNT_EXCLUDE_WHITESPACE) == xml.MNT_ELEMENT );
	Check( xml.FindNode(xml.MNT_EXCLUDE_WHITESPACE) == xml.MNT_ELEMENT );
	Check( xml.IntoElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T(" mixed ") );
	Check( xml.FindNode() == xml.MNT_ELEMENT );
	Check( xml.GetTagName() == MCD_T("B") );
	Check( xml.IntoElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("content") );
	Check( xml.FindNode() == 0 );
	Check( xml.OutOfElem() );
	Check( xml.FindNode() == xml.MNT_WHITESPACE );
	Check( xml.GetData() == MCD_T(" ") );
	Check( xml.FindNode() == xml.MNT_ELEMENT );
	Check( xml.GetTagName() == MCD_T("I") );
	Check( xml.IntoElem() );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("okay") );
	Check( xml.FindNode() == 0 );
	Check( xml.OutOfElem() );
	Check( xml.FindNode() == 0 );
	Check( xml.OutOfElem() );

	// Create a document xml2 by copying all nodes of document xml
	// Resulting document xml2 differs only by attributes and CRLF instead of just LF
	xml2.SetDoc( NULL );
	xml.ResetPos();
	bool bBypassWhitespace = false;
	while ( 1 )
	{
		int nType = xml.FindNode();
		if ( nType == 0 )
		{
			if ( ! xml.OutOfElem() )
				break;
			xml2.OutOfElem();
		}
		else
		{
			if ( bBypassWhitespace && nType == xml.MNT_WHITESPACE )
				bBypassWhitespace = false;
			else
			{
				if ( nType == xml.MNT_ELEMENT )
				{
					xml2.AddNode( nType, xml.GetTagName() );
					xml.IntoElem();
					xml2.IntoElem();
				}
				else
				{
					xml2.AddNode( nType, xml.GetData() );
					if ( nType & ( xml.MNT_PROCESSING_INSTRUCTION|xml.MNT_COMMENT|xml.MNT_DOCUMENT_TYPE ) )
					{
						// Bypass whitespace on nodes that automatically add it
						xml2.FindNode( xml2.MNT_WHITESPACE );
						bBypassWhitespace = true;
					}
				}
			}
		}
	}
	// Create a document representing depth first traversal of nodes excluding whitespace
	xml3.SetDoc( NULL );
	xml3.AddElem( MCD_T("NodeTraversal") );
	xml2.ResetPos();
	while ( 1 )
	{
		int nType = xml2.FindNode();
		if ( nType == 0 )
		{
			if ( ! xml2.OutOfElem() )
				break;
		}
		else if ( nType != xml2.MNT_WHITESPACE )
		{
			if ( nType == xml2.MNT_ELEMENT )
				xml3.AddChildElem( MCD_T("Node"), xml2.GetTagName() );
			else
				xml3.AddChildElem( MCD_T("Node"), xml2.GetData() );
			xml3.AddChildAttrib( MCD_T("type"), xml2.GetNodeType() );
			if ( nType == xml2.MNT_ELEMENT )
				xml2.IntoElem();
		}
	}
	// Now double check node traversal in xml3 against original xml
	// This helps verify xml --> xml2 node by node copy
	// and xml2 --> xml3 node traversal transformation into elements
	xml.ResetPos();
	xml3.ResetPos();
	while ( 1 )
	{
		int nType = xml.FindNode();
		if ( nType == 0 )
		{
			if ( ! xml.OutOfElem() )
				break;
		}
		else if ( nType != xml.MNT_WHITESPACE )
		{
			if ( Check(xml3.FindChildElem(),MCD_T("missing element in traversal double check"))==-1 )
				break;
			if ( Check(nType==MCD_STRTOINT(xml3.GetChildAttrib(MCD_T("type"))),MCD_T("wrong type in traversal double check"))==-1 )
				break;
			if ( nType == xml.MNT_ELEMENT )
				xml.IntoElem();
		}
	}
	// Add element and remove it using RemoveNode
	// Note that AddElem adds a CRLF (whitespace) after the element
	// which is removed as a separate node
	xml.ResetPos();
	xml.FindElem();
	xml.InsertChildElem( MCD_T("ITEM"), MCD_T("RemoveNode") );
	xml.IntoElem();
	xml.RemoveNode();
	if ( xml.FindNode( xml.MNT_WHITESPACE ) )
		xml.RemoveNode();
	// Remove all whitespace between nodes
	xml3.ResetPos();
	while ( 1 )
	{
		int nType = xml3.FindNode( xml3.MNT_WHITESPACE | xml3.MNT_ELEMENT );
		if ( nType == 0 )
		{
			if ( ! xml3.OutOfElem() )
				break;
		}
		else if ( nType == xml3.MNT_WHITESPACE )
		{
			Check( xml3.RemoveNode(), MCD_T("RemoveNode()") );
		}
		else // element
		{
			xml3.IntoElem();
		}
	}

	//[CMARKUPDEV
#if ! defined(MARKUP_MSXML)
	// Breadth First Traversal Test, loop through all elements
	// This relies on GetElemIndex and GotoElemIndex
	// This is not for MSXML because I think we would have to store node pointers
	// On each level, only add elements with children to the array for the next level
	StartCheckZone( MCD_T("Breadth First Traversal Test") );
	xml.SetDoc( MCD_T("<A><B><C/><C/><C><D/></C></B><B/></A>") );
	MCD_STRCLEAR( strListOfTagNames );
	CMarkup mParents, mNext;
	if ( xml.FindElem() ) // process root before loop
		strListOfTagNames = MCD_2PCSZ(xml.GetTagName());
	if ( xml.FindChildElem() ) // only add root to array if it has children
		mParents.AddElem( MCD_T("I"), xml.GetElemIndex() );
	while ( mParents.IsWellFormed() )
	{
		// Loop through parents known to have children
		mParents.ResetPos();
		while ( mParents.FindElem() )
		{
			// Loop through children of parent
			xml.GotoElemIndex( MCD_STRTOINT(mParents.GetData()) );
			while ( xml.FindChildElem() )
			{
				// Process element
				xml.IntoElem();
				strListOfTagNames += xml.GetTagName();
				if ( xml.FindChildElem() )
					mNext.AddElem( MCD_T("I"), xml.GetElemIndex() );
				xml.OutOfElem();
			}
		}
		mParents.SetDoc( mNext.GetDoc() );
		mNext.SetDoc( NULL );
	}
	Check( strListOfTagNames == MCD_T("ABBCCCD") );

	// Get/GotoElemIndex Test
	StartCheckZone( MCD_T("Get/GotoElemIndex Test") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("R") );
	xml.AddChildElem( MCD_T("C") );
	int nIndexC = xml.GetChildElemIndex();
	xml.InsertChildElem( MCD_T("B") );
	int nIndexB = xml.GetChildElemIndex();
	xml.IntoElem();
	xml.FindElem();
	xml.AddElem( MCD_T("D") );
	int nIndexD = xml.GetElemIndex();
	xml.GotoElemIndex( nIndexB );
	Check( xml.GetTagName() == MCD_T("B") );
	xml.InsertElem( MCD_T("A") );
	xml.OutOfElem();
	int nIndexA = xml.GetChildElemIndex();
	xml.GotoChildElemIndex( nIndexD );
	Check( xml.GetChildTagName() == MCD_T("D") );
	xml.GotoChildElemIndex( nIndexC );
	Check( xml.GetChildTagName() == MCD_T("C") );
	xml.GotoElemIndex( nIndexA );
	Check( xml.GetTagName() == MCD_T("A") );

	// Elem Level Test
	StartCheckZone( MCD_T("Elem Level Test") );
	Check( xml.SetDoc( MCD_T("<A><B>B</B><B/></A>") ) );
	Check( xml.FindChildElem( MCD_T("B") ) );
	Check( xml.GetElemLevel() == 0 );
	Check( xml.IntoElem() );
	Check( xml.GetElemLevel() == 1 );
	Check( xml.FindElem() );
	Check( xml.GetElemLevel() == 1 );
	Check( ! xml.FindElem() );
	Check( xml.GetElemLevel() == 1 );
	Check( xml.OutOfElem() );
	Check( xml.GetElemLevel() == 0 );
	Check( ! xml.OutOfElem() );
	Check( xml.GetElemLevel() == 0 );

	// Changing Element End Test
	StartCheckZone( MCD_T("Changing Element End Test") );
	Check( ! xml.SetDoc( MCD_T("<A>trail") ) );
	Check( xml.FindElem() );
	Check( xml.GetData() == MCD_T("") );
	Check( xml.GetElemFlags() & xml.MNF_NONENDED );
	Check( xml.AddChildSubDoc( MCD_T("<P/>") ) );
	Check( ! (xml.GetElemFlags() & xml.MNF_NONENDED) );
	Check( ! xml.AddSubDoc( MCD_T("<P>and<B>go</B><BR>now</J></P>") ) ); // ill-formed
	Check( xml.FindPrevElem() );
	Check( xml.GetTagName() == MCD_T("A") );
	Check( ! xml.FindPrevElem() ); // should leave current pos at A
	Check( ! xml.SetData( MCD_T("try") ) ); // no end tag
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("P") );
	Check( xml.FindChildElem() );
	Check( xml.IntoElem() );
	Check( ! (xml.GetElemFlags() & xml.MNF_NONENDED) );
	Check( xml.FindElem() );
	Check( xml.GetElemFlags() & xml.MNF_NONENDED );
	Check( xml.OutOfElem() );
	Check( xml.FindPrevElem() );
	Check( xml.RemoveElem() );
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("P") );
	Check( xml.GetElemFlags() & xml.MNF_ILLDATA );
	Check( xml.IntoElem() );
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("B") );
	xml.ResetMainPos();
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.RemoveNode() );
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("B") );
	Check( xml.GetData() == MCD_T("go") );
	Check( xml.SetData( MCD_T("") ) );
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("BR") );
	Check( xml.FindNode() == xml.MNT_TEXT );
	Check( xml.GetData() == MCD_T("now") );
	Check( xml.FindNode() == xml.MNT_LONE_END_TAG );
	Check( xml.RemoveNode() );
	Check( xml.FindPrevElem() );
	Check( xml.GetTagName() == MCD_T("B") );
	Check( xml.OutOfElem() );
	Check( ! (xml.GetElemFlags()&xml.MNF_ILLDATA) );
	Check( xml.IntoElem() );
	Check( ! xml.SetElemContent( MCD_T("</J>") ) );
	Check( xml.GetElemFlags() & xml.MNF_ILLDATA );
	Check( xml.SetData( MCD_T("") ) );
	Check( ! (xml.GetElemFlags()&xml.MNF_ILLDATA) );
	Check( xml.RemoveNode() );
	Check( xml.FindPrevElem() );
	Check( xml.GetTagName() == MCD_T("BR") );
	Check( xml.GetElemFlags() & xml.MNF_NONENDED );
	Check( xml.RemoveNode() );
	Check( xml.OutOfElem() );
	Check( xml.GetTagName() == MCD_T("P") );
	Check( xml.GetData() == MCD_T("now") );

	// Modified Flag Test
	StartCheckZone( MCD_T("Modified Flag Test") );
	Check( xml.GetDocFlags() & xml.MDF_MODIFIED );
	xml.SetDocFlags( xml.GetDocFlags() & ~xml.MDF_MODIFIED );
	Check( ! (xml.GetDocFlags() & xml.MDF_MODIFIED) );

	// Format Doc Test
	StartCheckZone( MCD_T("Format Doc Test") );
	Check( xml.SetDoc( MCD_T("<A\r\nv ='5' x\r>\t<!-- h -->\r\n\r\n\n<B \t n=\t'4'\t\r\n/></A>") ) );
	MCD_STR strFormattedDoc = xml.GetDocFormatted();
	Check( strFormattedDoc == MCD_T("<A v='5' x>") MCD_EOL MCD_T("<!-- h -->") MCD_EOL MCD_T("<B n='4'/>") MCD_EOL MCD_T("</A>") MCD_EOL );
	strFormattedDoc = xml.GetDocFormatted( 2 );
	Check( strFormattedDoc == MCD_T("<A v='5' x>") MCD_EOL MCD_T("  <!-- h -->") MCD_EOL MCD_T("  <B n='4'/>") MCD_EOL MCD_T("</A>") MCD_EOL );
	strFormattedDoc = xml.GetDocFormatted( 17 );
	Check( strFormattedDoc == MCD_T("<A v='5' x>") MCD_EOL MCD_T("\t<!-- h -->") MCD_EOL MCD_T("\t<B n='4'/>") MCD_EOL MCD_T("</A>") MCD_EOL );
	xml.SetDoc( MCD_T("<AX vx=\"\" vy=/>") );
	strFormattedDoc = xml.GetDocFormatted();
	Check( strFormattedDoc == MCD_T("<AX vx=\"\" vy=/>") MCD_EOL );
	xml.SetDoc( MCD_T("<AX   v  x/>") );
	strFormattedDoc = xml.GetDocFormatted();
	Check( strFormattedDoc == MCD_T("<AX v x/>") MCD_EOL );

#endif // not MSXML

	// Copy Constructor Test
	// MSXML copy constructor/assignment operator only implemented in developer version
	// MSXML compiler supplied default copy is shallow
	StartCheckZone( MCD_T("Copy Constructor Test Developer") );
	xml.SetDoc( NULL );
	xml2 = xml;
	xml.AddElem( MCD_T("COPY") );
	xml.AddChildElem( MCD_T("CHECK") );
	xml2 = xml;
	xml2.FindChildElem();
	Check( xml2.GetChildTagName() == xml.GetChildTagName() );
	Check( xml2.GetTagName() == xml.GetTagName() );

	// Merge Test
	StartCheckZone( MCD_T("Merge Test") );
	xml.SetDoc(NULL);
	xml.AddElem( MCD_T("Settings") );
	xml.IntoElem();
	xml.AddElem( MCD_T("Licensed"), 1 );
	xml.AddElem( MCD_T("DefaultContact") );
	xml.IntoElem();
	xml.AddElem( MCD_T("Name") );
	xml.SetAttrib( MCD_T("type"), MCD_T("char") );
	xml.SetAttrib( MCD_T("len"), 10 );
	xml.AddElem( MCD_T("Field") );
	xml.SetAttrib( MCD_T("type"), MCD_T("date") );
	xml.SetAttrib( MCD_T("len"), 20 );
	xml2.SetDoc(NULL);
	xml2.AddElem( MCD_T("Settings") );
	xml2.IntoElem();
	xml2.AddElem( MCD_T("DefaultContact") );
	xml2.SetAttrib( MCD_T("status"), MCD_T("ready") );
	xml2.IntoElem();
	xml2.AddElem( MCD_T("Field") );
	xml2.SetAttrib( MCD_T("type"), MCD_T("date") );
	xml2.SetAttrib( MCD_T("len"), 5 );
	xml2.AddElem( MCD_T("Origin") );
	xml2.SetAttrib( MCD_T("len"), 80 );
	xml2.OutOfElem();
	xml2.AddElem( MCD_T("Title"), MCD_T("Maintenance") );
	// Generic merge when element names are unique among siblings
	// Merge document 2 (xml2) into document 1 (xml)
	// removing elements/attribs from 2 as added to or overrided in 1
	SimpleMerge( xml, xml2 );
	Check( xml.FindGetData(MCD_T("/*/Licensed")) == MCD_T("1") );
	Check( xml.FindGetData(MCD_T("/*/Title")) == MCD_T("Maintenance") );
	Check( xml.FindElem( MCD_T("/*/DefaultContact/Name") ) );
	Check( xml.FindElem( MCD_T("/*/DefaultContact/Origin") ) );
	Check( xml.FindElem( MCD_T("/*/DefaultContact/Field") ) );
	Check( xml.GetAttrib(MCD_T("len")) == MCD_T("5") );

	// FindPrevElem Test
	StartCheckZone( MCD_T("FindPrevElem Test") );
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("R") );
	xml.ResetPos();
	Check( xml.FindPrevElem() );
	Check( xml.AddChildElem(MCD_T("A")) );
	Check( ! xml.FindPrevChildElem() ); // not found, but position remains at A
	Check( xml.AddChildElem(MCD_T("B")) );
	Check( xml.FindPrevChildElem() );
	Check( xml.GetChildTagName() == MCD_T("A") );
	xml.ResetChildPos();
	Check( xml.FindPrevChildElem() );
	Check( xml.GetChildTagName() == MCD_T("B") );
	Check( xml.FindPrevChildElem(MCD_T("A")) );
	Check( xml.GetChildTagName() == MCD_T("A") );
	Check( ! xml.FindPrevChildElem(MCD_T("B")) );
	Check( xml.GetChildTagName() == MCD_T("A") );
	xml.ResetChildPos();
	Check( xml.FindPrevChildElem(MCD_T("B")) );
	Check( xml.GetChildTagName() == MCD_T("B") );
	// Brannan Test 2: Previous Link
	Check( xml.InsertChildElem( MCD_T("M") ) );
	Check( xml.FindChildElem() );
	Check( xml.GetChildTagName() == MCD_T("B") );
	Check( xml.FindPrevChildElem() );
	Check( xml.GetChildTagName() == MCD_T("M") );

	// RemoveAttrib Test
	StartCheckZone( MCD_T("Remove Attribute Test") );
	xml.SetDoc(NULL);
	xml.AddElem( MCD_T("HELLO") );
	xml.SetAttrib( MCD_T("a"), MCD_T("1<") );
	xml.SetAttrib( MCD_T("b"), MCD_T("1<") );
	xml.RemoveAttrib( MCD_T("a") );
	xml.SetAttrib( MCD_T("cd"), MCD_T("1<") );
	xml.AddChildElem( MCD_T("CHILD") );
	xml.SetChildAttrib( MCD_T("i"), MCD_T("1<") );
	xml.SetChildAttrib( MCD_T("j"), MCD_T("1<") );
	xml.SetAttrib( MCD_T("e"), MCD_T("1<") );
	xml.RemoveChildAttrib( MCD_T("i") );
	Check( ! xml.RemoveChildAttrib( MCD_T("doesnotexist") ) );
	xml.SetAttrib( MCD_T("f"), MCD_T("1<") );
	xml.SetAttrib( MCD_T("gh"), MCD_T("1") );
	xml.ResetChildPos();
	Check( xml.FindChildElem(MCD_T("CHILD")) );
	xml.RemoveAttrib( MCD_T("gh") );
	xml.RemoveAttrib( MCD_T("cd") );
	Check( xml.RemoveChildAttrib(MCD_T("j")) );

	// Document order traversal (Depth first) using // path
	StartCheckZone( MCD_T("Everywhere Path Test") );
	Check( xml.SetDoc( MCD_T("<A><C/><B><C/><A/></B><C/><B><A/><C/></B></A>") ) );
	Check( xml.FindElem( MCD_T("//A") ) );
	Check( xml.GetElemPath() == MCD_T("/A") );
	Check( xml.FindElem( MCD_T("//A") ) );
	Check( xml.GetElemPath() == MCD_T("/A/B/A") );
	Check( xml.FindElem( MCD_T("//A") ) );
	Check( xml.GetElemPath() == MCD_T("/A/B[2]/A") );
	Check( ! xml.FindElem( MCD_T("//A") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("//C") ) );
	Check( xml.GetElemPath() == MCD_T("/A/C") );
	Check( xml.FindElem( MCD_T("//C") ) );
	Check( xml.GetElemPath() == MCD_T("/A/B/C") );
	Check( xml.FindElem( MCD_T("//C") ) );
	Check( xml.GetElemPath() == MCD_T("/A/C[2]") );
	Check( xml.FindElem( MCD_T("//C") ) );
	Check( xml.GetElemPath() == MCD_T("/A/B[2]/C") );
	Check( xml.FindElem( MCD_T("/A/B[2]/C") ) );
	Check( xml.SetAttrib( MCD_T("test"), 1 ) );
	Check( ! xml.FindElem( MCD_T("//C") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("//C[@test]") ) );
	MCD_STRCLEAR( strListOfTagNames );
	xml.ResetPos();
	while ( xml.FindElem( MCD_T("//*") ) )
		strListOfTagNames += xml.GetTagName();
	Check( strListOfTagNames == MCD_T("ACBCACBAC") );

#if ! defined(MARKUP_MSXML)
	// Multiroot Path Test
	StartCheckZone( MCD_T("Multiroot Path Test") );
	Check( ! xml.SetDoc( MCD_T("<A><Z/><Z/></A><A><Z><Y>qa</Y></Z></A><B/><A/>") ) );
	Check( xml.FindElem( MCD_T("/A[2]/Z/Y") ) );
	Check( xml.SetAttrib( MCD_T("p"), MCD_T("A2ZY") ) );
	Check( xml.FindElem( MCD_T("/A[3]") ) );
	Check( xml.SetAttrib( MCD_T("p"), MCD_T("A3") ) );
	Check( xml.FindElem( MCD_T("/B") ) );
	Check( xml.SetAttrib( MCD_T("p"), MCD_T("B") ) );
	Check( xml.FindElem( MCD_T("/A/Z[2]") ) );
	Check( xml.SetAttrib( MCD_T("p"), MCD_T("AZ2") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("A") ) );
	Check( xml.FindChildElem( MCD_T("Z[2]") ) );
	Check( xml.IntoElem() );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("AZ2") );
	Check( xml.OutOfElem() );
	Check( xml.FindElem( MCD_T("A") ) );
	Check( xml.FindChildElem( MCD_T("Z") ) );
	Check( xml.IntoElem() );
	Check( xml.FindChildElem( MCD_T("Y") ) );
	Check( xml.GetChildAttrib( MCD_T("p") ) == MCD_T("A2ZY") );
	Check( xml.OutOfElem() );
	Check( xml.FindElem( MCD_T("A") ) );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("A3") );
	Check( xml.FindPrevElem( MCD_T("B") ) );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("B") );
	Check( xml.FindElem( MCD_T("/*[4]") ) );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("A3") );
	Check( ! xml.FindElem( MCD_T("/A[4]") ) );
	Check( ! xml.FindElem( MCD_T("/A[2]/Z[2]") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("*[@p]") ) );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("B") );
	Check( xml.AddChildElem( MCD_T("Z"), MCD_T("d") ) );
	Check( xml.SetChildAttrib( MCD_T("p"), MCD_T("BZ") ) );
	Check( xml.FindElem( MCD_T("*[@p]") ) );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("A3") );
	Check( ! xml.FindElem( MCD_T("*[@p]") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("*[Z]") ) );
	Check( xml.GetTagName() == MCD_T("A") );
	Check( xml.FindElem( MCD_T("*[Z]") ) );
	Check( xml.GetTagName() == MCD_T("A") );
	Check( xml.FindChildElem( MCD_T("Z/Y") ) );
	Check( xml.GetChildAttrib( MCD_T("p") ) == MCD_T("A2ZY") );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("*[Z]/Z[2]") ) );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("AZ2") );
	Check( xml.OutOfElem() );
	Check( xml.FindElem( MCD_T("B[Z]/Z[@p]") ) );
	Check( xml.GetAttrib( MCD_T("p") ) == MCD_T("BZ") );

	// HasAttrib Test
	StartCheckZone( MCD_T("HasAttrib Attribute Test") );
	MCD_STR strHasAttribValue;
	xml.SetDoc(NULL);
	xml.AddElem( MCD_T("HELLO") );
	Check( ! xml.HasAttrib(MCD_T("a")) );
	Check( ! xml.HasAttrib(MCD_T("a"), &strHasAttribValue) );
	xml.SetAttrib( MCD_T("a"), MCD_T("1<'") );
	Check( xml.HasAttrib(MCD_T("a")) );
	Check( xml.HasAttrib(MCD_T("a"), &strHasAttribValue) );
	Check( strHasAttribValue == MCD_T("1<'") );
	xml.RemoveAttrib( MCD_T("a") );
	Check( ! xml.HasAttrib(MCD_T("a")) );

	// Anywhere path and mixed content test fixed in release 10.0
	StartCheckZone( MCD_T("Anywhere Mixed Test") );
	Check( xml.SetDoc( MCD_T("<A><B1/><B2>text</B2></A>") ) );
	Check( xml.FindElem( MCD_T("//B2") ) );
	Check( xml.IntoElem() );
	Check( xml.FindNode() );
	Check( ! xml.FindElem( MCD_T("//B1") ) );

	// Path Attribute Value Test
	StartCheckZone( MCD_T("Path Attribute Value Test") );
	Check( ! xml.SetDoc( MCD_T("<A p=\"1\" q=\"A\"><B1 p=\"1\"/><B2 q=\"1\" p=\"22&quot;\"/><A/>") ) );
	Check( xml.FindElem( MCD_T("//*[@p=1]") ) );
	Check( xml.GetTagName() == MCD_T("A") );
	Check( xml.FindElem( MCD_T("//*[@p=1]") ) );
	Check( xml.GetTagName() == MCD_T("B1") );
	Check( xml.FindElem( MCD_T("*[@q='1']") ) );
	Check( xml.GetTagName() == MCD_T("B2") );
	xml.ResetPos();
	MCD_STR strPathAttribVal = MCD_T("//B2[@p='");
	strPathAttribVal += xml.EscapeText(MCD_T("22\""),xml.MNF_ESCAPEQUOTES) + MCD_T("']");
	Check( xml.FindElem(strPathAttribVal) );
	Check( xml.FindPrevElem() );
	Check( xml.FindElem(strPathAttribVal) );
	Check( xml.GetTagName() == MCD_T("B2") );
	Check( ! xml.FindElem( MCD_T("//*[@q='A']") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("//*[@q='A']") ) );

	// Path Predicates Test
	Check( xml.SetDoc(
		MCD_T("<root>\r\n")
		MCD_T("<B ID=\"1\"/>\r\n")
		MCD_T("<B ID=\"2\">\r\n")
		MCD_T("<C ID=\"1\"/>\r\n")
		MCD_T("</B>\r\n")
		MCD_T("</root>\r\n")
		) );
	StartCheckZone( MCD_T("Path Predicates Test") );
	Check( xml.FindElem(MCD_T("/root/B[@ID='2']/C")) ); // online question
	Check( ! xml.FindElem(MCD_T("/root/B[ID]/C")) ); // non-existent child elem
	xml.ResetPos();
	Check( xml.FindElem(MCD_T("//B[C]")) );
	Check( xml.GetAttrib(MCD_T("ID")) == MCD_T("2") );

	// ElemContent Test
	StartCheckZone( MCD_T("ElemContent Test") );
	Check( xml.SetDoc( MCD_T("<A/>") ) );
	Check( xml.FindElem() );
	Check( xml.SetElemContent( MCD_T("<B/><B/>") ) );
	xml.ResetPos();
	Check( xml.FindElem( MCD_T("A") ) );
	Check( xml.IntoElem() );
	Check( xml.FindElem( MCD_T("B") ) );
	Check( xml.FindElem( MCD_T("B") ) );

	// Read/Write test
	if ( m_bLSFileLoaded )
	{
		StartCheckZone( MCD_T("File Read/Write Test") );
		MCD_PCSZ_FILENAME pszRWFileName = MCD_T_FILENAME("CMarkupReadWriteTest.xml");
		xml.SetDoc( NULL );
		// Test non-ASCII char is copyright symbol U-A9 is A9 in Windows-1252, C2 A9 in UTF-8
		MCD_CHAR szRWTestData[9];
#if defined(MARKUP_WCHAR)
		szRWTestData[0] = (wchar_t)0xa9;
		szRWTestData[1] = 0;
#elif defined(MARKUP_MBCS)
		// Get copyright symbol in whatever is the system locale ANSI encoding
		// Note that this test will differ depending on system config
		int nMBCharLen = wctomb( szRWTestData, (wchar_t)0xa9 );
		if ( nMBCharLen < 0 )
		{
			// Use ASCII C if not available
			szRWTestData[0] = 'C';
			nMBCharLen = 1;
		}
		szRWTestData[nMBCharLen] = '\0';
#else // UTF-8
		MCD_PSZCPY( szRWTestData, MCD_T("\xc2\xa9") );
#endif // UTF-8
		MCD_STR strRWTestData = szRWTestData;
		// MBCS build of MFC CMarkup will convert to UTF-8 file
		Check( xml.AddElem( MCD_T("ReadWriteTest"), strRWTestData ) );
		Check( xml.Save( pszRWFileName ) );
		Check( xml.Load( pszRWFileName ) );
		xml.FindElem();
		Check( xml.GetData() == strRWTestData );
		// Use UTF-16 encoding and UTF-16 BOM in file
		xml.SetDocFlags( xml.MDF_UTF16LEFILE );
		Check( xml.Save( pszRWFileName ) );
		xml.SetDocFlags( 0 );
		Check( xml.Load( pszRWFileName ) );
		Check( xml.GetDocFlags() & xml.MDF_UTF16LEFILE );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("bom")) );
		Check( mResult.FindElem(MCD_T("read")) );
		Check( mResult.GetAttrib(MCD_T("encoding")) == MCD_T("UTF-16LE") );
		xml.FindElem();
		Check( xml.GetData() == strRWTestData );
		// Use UTF-8 Preamble (BOM) in file
		xml.SetDocFlags( xml.MDF_UTF8PREAMBLE );
		Check( xml.Save( pszRWFileName ) );
		xml.SetDocFlags( 0 );
		Check( xml.Load( pszRWFileName ) );
		Check( xml.GetDocFlags() & xml.MDF_UTF8PREAMBLE );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("bom")) );
		Check( mResult.FindElem(MCD_T("read")) );
		Check( mResult.GetAttrib(MCD_T("encoding")) == MCD_T("UTF-8") );
		xml.FindElem();
		Check( xml.GetData() == strRWTestData );
		xml.SetDocFlags( 0 );
		// Specify encoding in XML Declaration
		xml.ResetPos();
		Check( xml.InsertNode( xml.MNT_PROCESSING_INSTRUCTION, MCD_T("xml") ) );
		Check( xml.SetAttrib( MCD_T("version"), MCD_T("1.0") ) );

#if defined(MARKUP_WINCONV) || defined(MARKUP_ICONV)
		StartCheckZone( MCD_T("File Ansi Encoding Test") );
		MCD_PCSZ aEncs[] =
		{
			MCD_T("Windows-1252"),
			MCD_T("iso_8859-15"),
			MCD_T("iso8859-1"),
			MCD_T("iso-8859-7"),
			MCD_T("ISO8859-9"),
			MCD_T("koi8-r"),
			NULL
		};
		MCD_STR strEncCheckDoc = xml.GetDoc();
		int nRWTextFlags = 0;
		MCD_STR strRWTextDoc, strRWTextResult, strRWTextEnc;
		int nEncTest;
		for ( nEncTest=0; aEncs[nEncTest]; ++nEncTest )
		{
			xml.SetDoc(strEncCheckDoc);
			xml.FindNode();
			xml.SetAttrib( MCD_T("encoding"), aEncs[nEncTest] );
			xml.Save( pszRWFileName );
			MCD_STRCLEAR( strRWTextEnc ); // don't override, just find out what encoding ReadTextFile reports
			CMarkup::ReadTextFile( pszRWFileName, strRWTextDoc, &strRWTextResult, &nRWTextFlags, &strRWTextEnc );
			Check( strRWTextEnc == aEncs[nEncTest] );
			xml.Load( pszRWFileName );
			xml.FindElem();
			Check( xml.GetData() == strRWTestData, aEncs[nEncTest] );
			MCD_STRCLEAR( strRWTextEnc ); // find out what encoding WriteTextFile reports
			strRWTextDoc = xml.GetDoc();
			CMarkup::WriteTextFile( pszRWFileName, strRWTextDoc, &strRWTextResult, &nRWTextFlags, &strRWTextEnc );
			Check( strRWTextEnc == aEncs[nEncTest] );
		}
#if ! defined(MARKUP_MSXML)
		MCD_STR strOverrideEncoding = MCD_T("Windows-1252");
		MCD_STR strEncReadDoc;
		MCD_STR strEncR;
		int nEncFlags = 0;
		for ( nEncTest=0; aEncs[nEncTest]; ++nEncTest )
		{
			xml.SetDoc(strEncCheckDoc);
			xml.FindNode();
			xml.SetAttrib( MCD_T("encoding"), aEncs[nEncTest] );
			CMarkup::WriteTextFile( pszRWFileName, xml.GetDoc(), &strEncR, &nEncFlags, &strOverrideEncoding );
			mResult.SetDoc( strEncR );
			Check( mResult.FindElem(MCD_T("write")) && mResult.GetAttrib(MCD_T("encoding")) == strOverrideEncoding );
			CMarkup::ReadTextFile( pszRWFileName, strEncReadDoc, &strEncR, &nEncFlags, &strOverrideEncoding );
			mResult.SetDoc( strEncR );
			Check( mResult.FindElem(MCD_T("read")) && mResult.GetAttrib(MCD_T("encoding")) == strOverrideEncoding );
			xml.SetDoc( strEncReadDoc );
			xml.FindElem();
			Check( xml.GetData() == strRWTestData );
		}

#if defined(MARKUP_TEST1255TO2022JP) && defined(MARKUP_ICONV) && ! defined(MARKUP_WCHAR)
		// IConv Bad Character Test
		// Ubuntu: iconv() problem UTF-8 to CP1255 on strings ending in d7 93 (but command line iconv worked)
		// old OS X did not have moniker "ISO2022JP"
		StartCheckZone( "IConv Bad Character Test" );
		MCD_STR strConvDoc = "\xc6\x92\xd7\x93\xc6\x92";
		MCD_STR strConvEncoding = "CP1255";
		Check( CMarkup::WriteTextFile(pszRWFileName, strConvDoc, NULL, NULL, &strConvEncoding) );
		Check( CMarkup::ReadTextFile(pszRWFileName, strConvDoc, NULL, NULL, &strConvEncoding) );
		Check( strConvDoc == "\xc6\x92\xd7\x93\xc6\x92" );
		strConvEncoding = "ISO2022JP";
		Check( CMarkup::ReadTextFile(pszRWFileName, strConvDoc, NULL, NULL, &strConvEncoding) );
		Check( strConvDoc == "???" );
#endif // TEST1255TO2022JP and ICONV not WCHAR
#endif // not MSXML
#endif // WINCONV or ICONV

		// File Error Test
		StartCheckZone( MCD_T("File Error Test") );
		Check( ! xml.Load(MCD_T_FILENAME("doesnotexist.test.xml")) );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("file_error")) );
		Check( MCD_STRTOINT(mResult.GetAttrib(MCD_T("n"))) != 0 ); // 2 on Windows

		// UTF-16BE File Test
		StartCheckZone( MCD_T("UTF-16BE File Test") );
		xml.SetDoc( MCD_T("<BigEndian/>\r\n") );
		xml.SetDocFlags( xml.MDF_UTF16BEFILE );
		Check( xml.Save( pszRWFileName ) );
		xml.SetDocFlags( 0 );
		Check( xml.Load( pszRWFileName ) );
		Check( xml.GetDocFlags() & xml.MDF_UTF16BEFILE );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("bom")) );
		Check( mResult.FindElem(MCD_T("read")) );
		Check( mResult.GetAttrib(MCD_T("encoding")) == MCD_T("UTF-16BE") );

#if defined(MARKUP_WINCONV)
		// Null In File Test
		// null removal works for non-WINCONV but we are not testing it here
		// because WriteTextFile may not be able to create file with null due to conversion
		StartCheckZone( MCD_T("Null In File Test") );
		MCD_CHAR szDocWithNull[16] = { MCD_T("<N>Testing0</N>") };
		szDocWithNull[10] = '\0'; // put a null where that 0 is
		MCD_STR strDocWithNull, strDocResult;
		MCD_STRASSIGN(strDocWithNull,szDocWithNull,15);
		int nFlagsDocWithNull = xml.MDF_UTF16BEFILE;
		CMarkup::WriteTextFile( pszRWFileName, strDocWithNull, &strDocResult, &nFlagsDocWithNull );
		Check( xml.Load( pszRWFileName ) );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("nulls_removed")) );
		Check( MCD_STRTOINT(mResult.GetAttrib(MCD_T("count"))) == 1 );
		Check( xml.FindElem() );
		Check( xml.GetData() == MCD_T("Testing") );
#endif // WINCONV

		// Empty File Test
		StartCheckZone( MCD_T("Empty File Test") );
		MCD_STR strEmptyDoc, strReadMsg;
		xml.SetDoc( strEmptyDoc );
		xml.SetDocFlags( xml.MDF_UTF16BEFILE );
		Check( xml.Save( pszRWFileName ) );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("bom")) );
		Check( mResult.FindElem(MCD_T("write")) );
		Check( mResult.GetAttrib(MCD_T("length")) == MCD_T("0") );
		xml.SetDocFlags( 0 );
		Check( ! xml.Load( pszRWFileName ) );
		Check( xml.GetDocFlags() & xml.MDF_UTF16BEFILE );
		Check ( CMarkup::ReadTextFile(pszRWFileName,strEmptyDoc,&strReadMsg) );
		xml.SetDocFlags( xml.MDF_UTF16LEFILE );
		Check( xml.Save( pszRWFileName ) );
		xml.SetDocFlags( 0 );
		Check( ! xml.Load( pszRWFileName ) );
		Check( xml.GetDocFlags() & xml.MDF_UTF16LEFILE );
		Check ( CMarkup::ReadTextFile(pszRWFileName,strEmptyDoc,&strReadMsg) );
		xml.SetDocFlags( 0 );
		Check( xml.Save( pszRWFileName ) );
		xml.SetDocFlags( 0 );
		Check( ! xml.Load( pszRWFileName ) );
		Check ( CMarkup::ReadTextFile(pszRWFileName,strEmptyDoc,&strReadMsg) );

		// File Write Mode Test
		MCD_STR strFileModeText = MCD_T("TextText TextText");
		MCD_STR strFileModeEncoding = MCD_T("UTF-8");
#ifdef MARKUP_TESTFILEWRITEMODEWITHGB18030
		strFileModeEncoding = MCD_T("GB18030");
#if defined(MARKUP_WCHAR)
		strFileModeText = MCD_T("\x9001\x8bed\x97f3\x670d\x52a1 ");
#else // not WCHAR
		strFileModeText = MCD_T("\xE9\x80\x81\xE8\xAF\xAD\xE9\x9F\xB3\xE6\x9C\x8D\xE5\x8A\xA1 ");
#endif // not WCHAR
#endif // TESTFILEWRITEMODEWITHGB18030
		for ( int nFMT=0; nFMT<13; ++nFMT )
			strFileModeText = strFileModeText + strFileModeText;
		// Note: offsets depend on MARKUP_FILEBLOCKSIZE, strFileModeText length
		xml.SetDoc( NULL ); // resets document capacity to ensure block reallocs tested
		int nFMTOffsetStart, nFMTOffsetLen, nFMTOffsetInStart, nFMTOffsetInLen;
		int nFMTOffset = 0;
		StartCheckZone( MCD_T("File Write Mode Test") );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_WRITEFILE ) );
		Check( xml.AddNode( CMarkup::MNT_PROCESSING_INSTRUCTION, MCD_T("xml") ) );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		Check( nFMTOffsetStart == 0 );
		Check( xml.SetAttrib( MCD_T("version"), MCD_T("1.0") ) );
		Check( xml.SetAttrib( MCD_T("encoding"), strFileModeEncoding ) );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		Check( nFMTOffsetStart == 0 );
		nFMTOffset = 33 + MCD_STRLENGTH(strFileModeEncoding);
		Check( nFMTOffsetLen == nFMTOffset );
		Check( xml.AddElem( MCD_T("html") ) );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset += MCD_EOLLEN;
		Check( nFMTOffsetStart == nFMTOffset );
		Check( nFMTOffsetLen == 7 );
		Check( xml.SetAttrib( MCD_T("lang"), MCD_T("en") ) );
		Check( xml.GetElemPath() == MCD_T("/html") );
		Check( xml.IntoElem() );
		Check ( xml.GetParentElemPath() == MCD_T("/html") );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset += 17; // just after html start tag
		Check( nFMTOffsetLen == 0 );
		Check( xml.AddElem( MCD_T("head") ) );
		Check ( xml.GetParentElemPath() == MCD_T("/html") );
		Check ( xml.GetElemPath() == MCD_T("/html/head") );
		Check( xml.IntoElem() );
		Check ( xml.GetParentElemPath() == MCD_T("/html/head") );
		Check( xml.AddElem( MCD_T("title"), MCD_T("Hello World") ) );
		Check( xml.AddElem( MCD_T("meta") ) );
		Check( xml.SetAttrib( MCD_T("name"), MCD_T("description") ) );
		Check( xml.SetAttrib( MCD_T("content"), MCD_T("file mode test") ) );
		Check( xml.GetElemPath() == MCD_T("/html/head/meta") );
		Check( xml.Flush() );
		nFMTOffset = 0;
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("write")) );
		Check( mResult.GetAttrib(MCD_T("encoding")) == strFileModeEncoding );
		Check( xml.OutOfElem() );
		Check ( xml.GetParentElemPath() == MCD_T("/html") );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset += MCD_EOLLEN + 7; // after CRLF and head end tag
		Check( nFMTOffsetStart == nFMTOffset );
		Check( nFMTOffsetLen == 0 );
		Check( xml.AddElem( MCD_T("body") ) );
		Check( xml.IntoElem() );
		Check( xml.AddElem( MCD_T("p"), strFileModeText ) );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset = 6 + MCD_EOLLEN; // kept the split the empty body tag when adjusting buffer
		Check( nFMTOffsetStart == nFMTOffset );
		Check( nFMTOffsetLen == 7 + MCD_STRLENGTH(strFileModeText) );
		Check( nFMTOffsetInStart == nFMTOffsetStart + 3 );
		Check( nFMTOffsetInLen == nFMTOffsetLen - 7 );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("write")) );
		Check( ! xml.IntoElem() ); // cannot go into non-empty element in write mode
		Check( xml.AddElem( MCD_T("p") ) );
		Check( xml.GetElemPath() == MCD_T("/html/body/p[2]") );
		Check( xml.IntoElem() );
		Check( xml.AddElem( MCD_T("img"), MCD_T(""), CMarkup::MNF_WITHNOLINES ) );
		Check( xml.SetAttrib( MCD_T("src"), strFileModeText ) );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("write")) );
		Check( xml.OutOfElem() );
		Check( xml.AddElem( MCD_T("p"), strFileModeText ) );
		Check( xml.AddElem( MCD_T("p") ) );
		Check( xml.SetData( strFileModeText ) );
		Check( ! xml.SetData( MCD_T("test") ) );
		Check( ! xml.IntoElem() );
		Check( xml.AddElem( MCD_T("p") ) );
		Check( xml.IntoElem() );
		Check( xml.AddNode( CMarkup::MNT_TEXT, strFileModeText ) );
		Check( xml.AddNode( CMarkup::MNT_CDATA_SECTION, strFileModeText ) );
		Check( xml.AddNode( CMarkup::MNT_TEXT, MCD_T("test") ) );
		Check( xml.OutOfElem() );
 		Check( xml.AddElem( MCD_T("p") ) );
		Check( xml.IntoElem() );
		Check( xml.AddNode( CMarkup::MNT_ELEMENT, MCD_T("b") ) );
		Check( xml.GetElemPath() == MCD_T("/html/body/p[6]/b") );
		Check( xml.IntoElem() );
		Check( xml.AddNode( CMarkup::MNT_TEXT, MCD_T("bolded") ) );
		Check( xml.OutOfElem() );
		Check( xml.AddNode( CMarkup::MNT_TEXT, MCD_T(" and ") ) );
		Check( xml.AddElem( MCD_T("i"), MCD_T("italic"), CMarkup::MNF_WITHNOLINES ) );
		Check( xml.AddNode( CMarkup::MNT_ELEMENT, MCD_T("strong") ) );
		Check( xml.GetElemPath() == MCD_T("/html/body/p[6]/strong") );
		Check( xml.IntoElem() );
		Check( xml.AddNode( CMarkup::MNT_TEXT, MCD_T("strong") ) );
		Check( xml.OutOfElem() ); // strong
		Check( xml.OutOfElem() ); // p
 		Check( xml.AddElem( MCD_T("p") ) ); //p[7]
		Check( xml.IntoElem() );
		int nITN;
		for ( nITN=0; nITN<300; ++nITN )
		{
			MCD_CHAR szInefficientTagName[20];
			MCD_SPRINTF( MCD_SSZ(szInefficientTagName), MCD_T("record%d"), nITN );
	 		xml.AddElem( szInefficientTagName );
		}
		Check( xml.IntoElem() );
		Check( xml.AddNode( CMarkup::MNT_CDATA_SECTION, MCD_T("test") ) );
		Check( xml.OutOfElem() ); // record299
		Check( xml.OutOfElem() ); // p
		Check( xml.AddSubDoc( MCD_T("<emptysubdoc/>") ) );
		Check( xml.SetAttrib( MCD_T("s"), 1 ) );
		Check( ! xml.AddSubDoc( MCD_EOL MCD_T("<!-- just a comment -->") ) );
		Check( xml.AddSubDoc( MCD_T("<p>para with CRLF</p>") MCD_EOL ) ); // p[9]
		Check( xml.AddSubDoc( MCD_T("<p>para without CRLF</p>") ) ); // p[10]
		Check( xml.AddSubDoc( MCD_T("<p><b>child</b></p>") ) ); // p[11]
		Check( ! xml.AddSubDoc( MCD_T("<p>one</p>") MCD_EOL MCD_T("<p>two</p>") MCD_EOL ) ); // p[12] p[13]
		Check( xml.AddSubDoc( MCD_T("<p/>") ) ); // p[14]
		Check( xml.IntoElem() );
		Check( xml.AddSubDoc( MCD_T("<b><i>") + strFileModeText + MCD_T("</i></b>") ) );
		// Check( xml.AddSubDoc( MCD_T("<i>ital</i>") ) );
		Check( xml.AddElem( MCD_T("i") ) );
		Check( xml.SetData( MCD_T("ital") ) ); // 11.1 andyh test
		Check( xml.AddSubDoc( MCD_T("<b><i>ib</i></b>") ) );
		Check( xml.OutOfElem() ); // p[14]
		Check( xml.OutOfElem() ); // body
		Check( xml.OutOfElem() ); // html
		Check( ! xml.OutOfElem() );
		Check( xml.AddNode( CMarkup::MNT_COMMENT, MCD_T("trailing comment") ) );
		Check( xml.Close() );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("write")) );
		Check( mResult.GetAttrib(MCD_T("encoding")) == strFileModeEncoding );

		// File Read Mode Test
		StartCheckZone( MCD_T("File Read Mode Test") );
		xml.Open( pszRWFileName, CMarkup::MDF_READFILE );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("read")) );
		Check( mResult.GetAttrib(MCD_T("encoding")) == strFileModeEncoding );
		Check ( xml.FindNode() == CMarkup::MNT_PROCESSING_INSTRUCTION );
		Check ( xml.GetAttrib(MCD_T("version")) == MCD_T("1.0") );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset = 33 + MCD_STRLENGTH(strFileModeEncoding);
		Check( nFMTOffsetStart == 0 );
		Check( nFMTOffsetLen == nFMTOffset );
		Check( xml.FindElem() );
		Check ( xml.GetElemLevel() == 0 );
		Check ( xml.GetAttrib(MCD_T("lang")) == MCD_T("en") );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset += MCD_EOLLEN;
		Check( nFMTOffsetStart == nFMTOffset );
		Check( nFMTOffsetLen == 16 );
		Check( xml.IntoElem() );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset += 16; // creates a start tag and goes after it
		Check( nFMTOffsetStart == nFMTOffset );
		Check( nFMTOffsetLen == 0 );
		Check( xml.FindNode(CMarkup::MNT_ELEMENT) );
		Check ( xml.GetTagName() == MCD_T("head") );
		Check ( xml.GetElemPath() == MCD_T("/html/head") );
		Check( xml.IntoElem() );
		Check ( xml.GetParentElemPath() == MCD_T("/html/head") );
		Check( xml.FindElem() );
		Check ( xml.GetTagName() == MCD_T("title") );
		Check ( xml.GetSubDoc() == MCD_T("<title>Hello World</title>") MCD_EOL );
		Check ( xml.GetData() == MCD_T("Hello World") );
		Check ( xml.GetSubDoc() == MCD_T("<title>Hello World</title>") MCD_EOL );
		Check ( xml.GetElemPath() == MCD_T("/html/head/title") );
		Check ( xml.GetParentElemPath() == MCD_T("/html/head") );
		Check( xml.FindElem() );
		Check ( xml.GetAttrib(MCD_T("name")) == MCD_T("description") );
		Check( ! xml.IntoElem() ); // empty element
		Check ( xml.GetElemPath() == MCD_T("/html/head/meta") );
		Check ( ! xml.FindNode(CMarkup::MNT_PROCESSING_INSTRUCTION) );
		Check ( xml.GetParentElemPath() == MCD_T("/html/head") );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset += 83 + MCD_EOLLEN * 4; // before head end tag
		Check( nFMTOffsetStart == nFMTOffset );
		Check( nFMTOffsetLen == 0 );
		Check( xml.OutOfElem() );
		Check ( xml.GetParentElemPath() == MCD_T("/html") );
		Check( xml.GetOffsets( &nFMTOffsetStart, &nFMTOffsetLen, &nFMTOffsetInStart, &nFMTOffsetInLen ) );
		nFMTOffset += 7; // after head end tag
		Check( nFMTOffsetStart == nFMTOffset );
		Check( nFMTOffsetLen == 0 );
		Check( xml.FindElem() );
		Check ( xml.GetElemPath() == MCD_T("/html/body") );
		Check ( xml.GetTagName() == MCD_T("body") );
		Check ( xml.GetElemLevel() == 1 );
		Check( xml.IntoElem() );
		Check( xml.FindElem( MCD_T("p") ) );
		Check( xml.GetResult() == MCD_T("") );
		Check( xml.FindElem() );
		mResult.SetDoc( xml.GetResult() );
		Check( mResult.FindElem(MCD_T("read")) );
		Check ( xml.GetElemPath() == MCD_T("/html/body/p[2]") );
		Check( xml.IntoElem() );
		Check( xml.FindElem( MCD_T("img") ) );
		Check ( xml.GetElemLevel() == 3 );
		Check ( xml.GetAttrib(MCD_T("src")) == strFileModeText );
		Check ( xml.GetSubDoc() == MCD_T("<img src=\"") + strFileModeText + MCD_T("\"/>") );
		Check( xml.OutOfElem() );
		Check( xml.FindElem( MCD_T("p") ) );
		Check ( xml.GetElemPath() == MCD_T("/html/body/p[3]") );
		Check( xml.IntoElem() );
		Check( xml.FindNode() == CMarkup::MNT_TEXT );
		Check ( xml.GetData() == strFileModeText );
		Check( xml.OutOfElem() );
		Check( xml.FindElem() ); // p[4]
		Check ( xml.GetElemPath() == MCD_T("/html/body/p[4]") );
		Check ( xml.GetSubDoc() == MCD_T("<p>") + strFileModeText + MCD_T("</p>") MCD_EOL );
		Check( xml.FindElem() ); // p[5]
		Check ( xml.GetData() == strFileModeText + strFileModeText + MCD_T("test") );
		Check( xml.FindElem() ); // p[6]
		Check( xml.IntoElem() );
		Check( xml.FindElem( MCD_T("i") ) );
		Check ( xml.GetElemPath() == MCD_T("/html/body/p[6]/i") );
		Check( xml.OutOfElem() );
		Check( xml.FindElem( MCD_T("p/record255[1]") ) );
		Check ( xml.GetElemPath() == MCD_T("/html/body/p[7]/record255") );
		Check( xml.FindElem() );
		Check ( xml.GetElemPath() == MCD_T("/html/body/p[7]/record256[0]") );
		Check( xml.FindElem( MCD_T("record299") ) );
		Check ( xml.GetData() == MCD_T("test") );
		Check ( xml.GetData() == MCD_T("test") ); // twice in a row
		Check( xml.OutOfElem() );
		Check( xml.FindElem() ); // p[8]
		Check ( xml.GetSubDoc() == MCD_T("<emptysubdoc s=\"1\"/>") MCD_EOL );
		Check ( xml.FindNode(CMarkup::MNT_COMMENT) );
		Check( xml.FindElem() ); // p[9]
		Check ( xml.GetSubDoc() == MCD_T("<p>para with CRLF</p>") MCD_EOL );
		Check( xml.FindElem() ); // p[10]
		Check ( xml.GetSubDoc() == MCD_T("<p>para without CRLF</p>") MCD_EOL ); // now has newline
		Check( xml.FindElem() ); // p[11]
		Check ( xml.GetSubDoc() == MCD_T("<p><b>child</b></p>") MCD_EOL );
		Check( xml.FindElem() ); // p[12]
		Check ( xml.GetSubDoc() == MCD_T("<p>one</p>") MCD_EOL );
		Check( xml.FindElem() ); // p[13]
		Check ( xml.GetSubDoc() == MCD_T("<p>two</p>") MCD_EOL );
		Check( xml.FindElem() ); // p[14]
		Check ( xml.GetSubDoc() == MCD_T("<p>") MCD_EOL MCD_T("<b><i>") + strFileModeText
			+ MCD_T("</i></b>") MCD_EOL MCD_T("<i>ital</i>") MCD_EOL MCD_T("<b><i>ib</i></b>") MCD_EOL MCD_T("</p>") MCD_EOL );
		Check( ! xml.FindElem() );
		Check( xml.OutOfElem() ); // body
		Check( xml.OutOfElem() ); // html
		Check ( xml.FindNode(CMarkup::MNT_COMMENT) );
		Check ( xml.FindNode() );
		Check( ! xml.FindNode() );
		Check( xml.Close() );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_READFILE ) );
		Check( xml.FindElem( MCD_T("/html/body/p[7]/record270") ) );
		Check( xml.Close() );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_READFILE ) );
		Check( xml.FindElem( MCD_T("//*[@lang]") ) );
		Check ( xml.GetAttrib(MCD_T("lang")) == MCD_T("en") );
		Check ( xml.FindGetData(MCD_T("//title")) == MCD_T("Hello World") );
		Check( xml.FindElem( MCD_T("//*[@name='description']") ) );
		Check ( xml.GetAttrib(MCD_T("content")) == MCD_T("file mode test") );
		Check( xml.FindElem(MCD_T("//*[@src]")) );
		Check( xml.GetTagName() == MCD_T("img") );
		Check( ! xml.FindElem() );
		Check( xml.Close() );
		CMarkup::WriteTextFile( pszRWFileName, MCD_T("  ") + strFileModeText );
		MCD_STR strReadModeDetectEncoding;
		Check( xml.Open( pszRWFileName, CMarkup::MDF_READFILE, &strReadModeDetectEncoding ) );
		Check( strReadModeDetectEncoding == MCD_T("UTF-8") );
		mResult.SetDoc( xml.GetResult() );
		Check( xml.Close() );

		// Append File Test
		StartCheckZone( MCD_T("Append File Test") );
		int nAFT;
		xml.SetDocFlags( 0 );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_WRITEFILE|CMarkup::MDF_UTF16BEFILE ) ); // UTF-16BE BOM
		Check( xml.GetDocFlags() & CMarkup::MDF_WRITEFILE );
		Check( xml.Close() );
		Check( ! (xml.GetDocFlags() & CMarkup::MDF_WRITEFILE) );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_APPENDFILE ) ); // UTF-16BE holds over
		for ( nAFT=0; nAFT<463; ++nAFT )
	 		xml.AddElem( MCD_T("testappendable"), MCD_T("datappendable") );
		Check( xml.GetElemPath() == MCD_T("/testappendable[463]") );
		Check( xml.GetDocFlags() & CMarkup::MDF_WRITEFILE );
		Check( xml.GetDocFlags() & CMarkup::MDF_APPENDFILE );
		Check( xml.Close() );
		Check( ! (xml.GetDocFlags() & CMarkup::MDF_WRITEFILE) );
		Check( xml.GetDocFlags() & CMarkup::MDF_UTF16BEFILE );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_APPENDFILE ) );
		for ( nAFT=0; nAFT<537; ++nAFT )
	 		xml.AddElem( MCD_T("testappendable"), MCD_T("datappendable") );
		Check( xml.Close() );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_READFILE ) );
		nAFT=0;
		while ( xml.FindElem() )
			++nAFT;
		Check( xml.Close() );
		Check( ! (xml.GetDocFlags() & CMarkup::MDF_READFILE) );
		Check( nAFT == 1000 );
		xml.SetDocFlags( 0 );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_WRITEFILE ) );
		Check( xml.Close() );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_APPENDFILE ) );
		for ( nAFT=0; nAFT<723; ++nAFT )
	 		xml.AddElem( MCD_T("testappendable"), MCD_T("datappendable") );
		Check( xml.Close() );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_APPENDFILE ) );
		for ( nAFT=0; nAFT<277; ++nAFT )
	 		xml.AddElem( MCD_T("testappendable"), MCD_T("datappendable") );
		Check( xml.Close() );
		Check( xml.Open( pszRWFileName, CMarkup::MDF_READFILE ) );
		nAFT=0;
		while ( xml.FindElem() )
			++nAFT;
		Check( xml.Close() );
		Check( nAFT == 1000 );

		// File Mode Performance Test
		StartCheckZone( MCD_T("File Mode Performance Test") );
		xml = CMarkup(); // clear document capacity
		Check( xml.Open( pszRWFileName, CMarkup::MDF_WRITEFILE ) );
		const MCD_INTFILEOFFSET nFilePerfTestSize = 102400; // >4GB test: 0x100005000;
		const int nFilePerfMilliSample = 150;
		MCD_INTFILEOFFSET nFilePerfWriteTotal = 0;
		MCD_INTFILEOFFSET nFilePerfWriteFileSize = 0;
		int nFilePerfWriteTime = 0;
		nTimeStart = GetMilliCount();
		while ( 1 )
		{
			// High markup density 64 char record (6 tags, 13 chars of data; low markup density is significantly faster)
			xml.AddElem( MCD_T("row") ); // 15
			xml.IntoElem();
			xml.AddElem( MCD_T("bogy"), MCD_T("abcde") ); // 20
			xml.SetAttrib( MCD_T("id"), MCD_T("500") ); // 9
			xml.AddElem( MCD_T("bogy"), MCD_T("fghij") ); // 20
			xml.OutOfElem();
			nFilePerfWriteFileSize += 64;
			if ( nFilePerfWriteFileSize >= nFilePerfTestSize )
			{
				nFilePerfWriteTotal += nFilePerfWriteFileSize;
				nFilePerfWriteTime = GetMilliSpan( nTimeStart );
				if ( nFilePerfWriteTime >= nFilePerfMilliSample )
					break;
				xml.Close();
				xml.Open( pszRWFileName, CMarkup::MDF_WRITEFILE );
				nFilePerfWriteFileSize = 0;
			}
		}
		Check( xml.Close() );
		int nFilePerfReadTotal = 0;
		int nFilePerfReadTime = 0;
		nTimeStart = GetMilliCount();
		Check( xml.Open( pszRWFileName, CMarkup::MDF_READFILE ) );
		while ( nFilePerfReadTime < nFilePerfMilliSample )
		{
			if ( xml.FindElem() )
			{
				xml.IntoElem();
				xml.FindElem();
				xml.GetData();
				xml.GetAttrib( MCD_T("id") );
				xml.FindElem();
				xml.GetData();
				xml.OutOfElem();
				nFilePerfReadTotal += 64;
			}
			else
			{
				xml.Close();
				xml.Open( pszRWFileName, CMarkup::MDF_READFILE );
			}
			if ( ! (nFilePerfReadTotal % 10240) )
				nFilePerfReadTime = GetMilliSpan( nTimeStart );
		}
		Check( xml.Close() );
		m_nWriterSpeed = (int)((MCD_INTFILEOFFSET)(nFilePerfWriteTotal/nFilePerfWriteTime));
		m_nReaderSpeed = nFilePerfReadTotal/nFilePerfReadTime;

		xml.SetDocFlags( 0 );
	}

	// XML Declaration Encoding Test
	StartCheckZone( MCD_T("XML Declaration Encoding") );
	MCD_STR strEncodingLabel = MCD_T("Windows-1250");
	MCD_STR strEncodingDoc = MCD_T("<?xml version=\"1.0\" encoding=\"");
	strEncodingDoc += strEncodingLabel;
	strEncodingDoc += MCD_T("\"?>\r\n<ROOT/>\r\n");
	Check( xml.SetDoc(strEncodingDoc) );
	Check( xml.FindNode() );
	Check( xml.GetAttrib(MCD_T("encoding")) == strEncodingLabel );
	Check( xml.GetDeclaredEncoding(strEncodingDoc) == strEncodingLabel );

	// HTML Meta Charset Test vary spacing/punctuation in content value
	StartCheckZone( MCD_T("HTML Meta Charset") );
	MCD_STR strHtmlTop = MCD_T("<html><head><title>T</title>")
		MCD_T("<META http-equiv=Content-Type content=\"");
	MCD_STR strHtmlBottom =  MCD_T("\"></head><body>Hello</body></html>");
	MCD_STR strHtmlCharsetDoc;
	strHtmlCharsetDoc = strHtmlTop + MCD_T("text/html;charset= iso-8859-1") + strHtmlBottom;
	Check( xml.GetDeclaredEncoding(strHtmlCharsetDoc) == MCD_T("iso-8859-1") );
	strHtmlCharsetDoc = strHtmlTop + MCD_T("text/html; charset=utf-8") + strHtmlBottom;
	Check( xml.GetDeclaredEncoding(strHtmlCharsetDoc) == MCD_T("utf-8") );
	strHtmlCharsetDoc = strHtmlTop + MCD_T(" text/html ; charset = windows-1252 ") + strHtmlBottom;
	Check( xml.GetDeclaredEncoding(strHtmlCharsetDoc) == MCD_T("windows-1252") );
	strHtmlCharsetDoc = strHtmlTop + MCD_T("text/html;charset=Shift_JIS;hypothetical=t;") + strHtmlBottom;
	Check( xml.GetDeclaredEncoding(strHtmlCharsetDoc) == MCD_T("Shift_JIS") );
#endif // not MSXML

#if defined(MARKUP_MSXML) && ! defined(MARKUP_MSXML1)
	// MSXML 3.0 XPath Test, SelectionLanguage is set here
	StartCheckZone( MCD_T("MSXML XPath Test") );
	xml.SetDoc( MCD_T("<Admin><Area AreaName=\"a\"/></Admin>") );
	xml.m_pDOMDoc->setProperty( MCD_T("SelectionLanguage"), MCD_T("XPath") );
	MSXMLNS::IXMLDOMNodeListPtr pNodes
		= xml.m_pDOMDoc->selectNodes( MCD_T("/Admin/Area[string-length(@AreaName) = 1]") );
	int nSelectCount = pNodes->Getlength();
	Check( nSelectCount > 0 );
	Check( xml.SetMainPosPtr(pNodes->Getitem(0)) );
	Check( xml.GetTagName() == MCD_T("Area") );

	// MSXML 3.0+ supports XSLT transformation
	StartCheckZone( MCD_T("MSXML XSLT Test") );
	Check( xml.SetDoc(MCD_T("<list><i><n>sit</n></i><i><n>stand</n></i></list>")) );
	CMarkupMSXML xsl;
	Check( xsl.SetDoc(
		MCD_T("<xsl:stylesheet version=\"1.0\" ")
		MCD_T("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\r\n")
		MCD_T("<xsl:template match=\"/\">\r\n")
		MCD_T("<html><body><table>\r\n")
		MCD_T("  <xsl:for-each select=\"/*/i\">\r\n")
		MCD_T("  <tr><td><xsl:value-of select=\"n\"/></td></tr>\r\n")
		MCD_T("  </xsl:for-each>\r\n")
		MCD_T("</table></body></html>\r\n")
		MCD_T("</xsl:template>\r\n")
		MCD_T("</xsl:stylesheet>\r\n")
		) );
	Check( xml.SetDoc( xml.Transform(xsl) ) );
	Check( xml.FindElem() );
	Check( xml.IntoElem() );
	Check( xml.FindElem() ); // body
	Check( xml.IntoElem() );
	Check( xml.FindElem() ); // html
	Check( xml.IntoElem() );
	Check( xml.FindElem() ); // tr
	Check( xml.IntoElem() );
	Check( xml.FindElem() ); // td
	Check( xml.GetTagName() == MCD_T("td") );
	Check( xml.GetData() == MCD_T("sit") );
#endif // MSXML3 or MSXML4

#if defined(MARKUP_MSXML)
	// MSXML Select Single Node
	// See MSDN: IXMLDOMNode::selectSingleNode Returns Invalid Pointer
	// so we use raw_selectSingleNode as recommended instead of selectSingleNode
	StartCheckZone( MCD_T("MSXML selectSingleNode Test") );
	xml.SetDoc( MCD_T("<TOP><MID><LEAF1/></MID><MID/><MID><LEAF2/></MID></TOP>") );
	MSXMLNS::IXMLDOMNodePtr pResult;
	Check( SUCCEEDED( xml.m_pDOMDoc->raw_selectSingleNode(
		_bstr_t(MCD_T("/TOP/MID/LEAF2")),
		&pResult) ) );
	Check( ((bool)pResult) );
	Check( xml.SetMainPosPtr(pResult) );
	Check( xml.GetTagName() == MCD_T("LEAF2") );
	_bstr_t bstrQuery( MCD_T("/TOP/MID/LEAF2") );
	Check( xml.SetMainPosPtr(xml.m_pDOMDoc->selectSingleNode(bstrQuery)) );
	Check( xml.GetTagName() == MCD_T("LEAF2") );
	_bstr_t bstrQueryNotFound( MCD_T("/TOP/MID/LEAF3") );
	Check( ! xml.SetMainPosPtr(xml.m_pDOMDoc->selectSingleNode(bstrQueryNotFound)) );

	// MSXML Default Namespace Test
	MCD_STR strNamespace = MCD_T("http://www.mimosa.org/TechXMLV3-0");
	StartCheckZone( MCD_T("MSXML Default Namespace Test") );
	MCD_STR strDocMSXML;
	xml.SetDoc( NULL );
	xml.SetDefaultNamespace( strNamespace );
	xml.AddElem( MCD_T("mim_0003"));
	strDocMSXML = xml.GetDoc();
	Check( strDocMSXML.Find(strNamespace) != -1 );
	xml.AddChildElem( MCD_T("connect_req"));
	strDocMSXML = xml.GetDoc();
	Check( strDocMSXML.Find(strNamespace,strDocMSXML.Find(strNamespace)+1) == -1 ); // only once
	xml.IntoElem(); // inside mim_003
	xml.AddElem( MCD_T("connect_req"));
	xml.IntoElem(); // inside connect_req
	xml.AddElem( MCD_T("param"));
	strDocMSXML = xml.GetDoc();
	Check( strDocMSXML.Find(strNamespace,strDocMSXML.Find(strNamespace)+1) == -1 ); // only once
	xml.SetDefaultNamespace( NULL );
#endif // MSXML

	// Path Test
	StartCheckZone( MCD_T("Path Test") );
	xml.SetDoc(NULL);
	xml.AddElem( MCD_T("ROOT") );
	xml.AddChildElem( MCD_T("CHELEM") );
	xml.FindElem( MCD_T("/*/CHELEM") );
	Check( xml.GetTagName() == MCD_T("CHELEM") );
	xml.FindChildElem( MCD_T("/*/CHELEM") );
	Check( xml.GetChildTagName() == MCD_T("CHELEM") );
	xml.IntoElem();
	xml.AddChildElem( MCD_T("C") );
	xml.IntoElem();
	xml.AddChildElem( MCD_T("CC"), MCD_T("CCStuff") );
	xml.ResetPos();
	Check( xml.FindChildElem( MCD_T("CHELEM/C/CC") ) );
	Check( xml.FindGetData( MCD_T("/*/*/C/CC") ) == MCD_T("CCStuff") );
	Check( xml.FindSetData( MCD_T("/*/*/C/*"), MCD_T("CCDiff") ) );
	Check( xml.OutOfElem() || ! xml.OutOfElem() || ! xml.OutOfElem() );

#if ! defined(MARKUP_MSXML)
	// FindSetData Create Test
	StartCheckZone( MCD_T("FindSetData Create Test") );
	xml.SetDoc(NULL);
	Check( xml.FindSetData( MCD_T("/Config"), MCD_T("\r\n") ) );
	Check( xml.FindSetData( MCD_T("/*"), MCD_T("") ) );
	Check( xml.FindSetData( MCD_T("/*/User/Name"), MCD_T("a") ) );
	Check( xml.FindSetData( MCD_T("/*/*/Branch"), MCD_T("b-") ) );
	Check( xml.FindSetData( MCD_T("/*/*/*[2]"), MCD_T("b") ) );
	Check( xml.AddElem( MCD_T("Branch"), MCD_T("c-") ) );
	Check( xml.FindSetData( MCD_T("/*/*/Branch[2]"), MCD_T("c") ) );
	Check( xml.FindSetData( MCD_T("/*/User/Address/Zip"), MCD_T("10000") ) );
	Check( xml.FindGetData( MCD_T("/*/*/Address/*") ) == MCD_T("10000") );
	Check( xml.FindSetData( MCD_T("/Config/Machine/Chip"), MCD_T("A") ) );
	Check( xml.FindGetData( MCD_T("/Config/Machine/Chip") ) == MCD_T("A") );
	Check( xml.FindSetData( MCD_T("/*/*[2]/Chip"), MCD_T("B") ) );
	Check( xml.FindGetData( MCD_T("/*/*[2]/*") ) == MCD_T("B") );
	Check( xml.FindSetData( MCD_T("/*/Machine/Amp"), MCD_T("&#x20;"), CMarkup::MNF_WITHREFS ) );
	Check( xml.FindGetData( MCD_T("/*/Machine/Amp") ) == MCD_T(" ") );
	Check( xml.FindSetData( MCD_T("/*/Machine/Amp"), MCD_T("]]>"), CMarkup::MNF_WITHCDATA ) ); // replace text
	Check( xml.FindGetData( MCD_T("/*/Machine/Amp") ) == MCD_T("]]>") );
	Check( xml.FindSetData( MCD_T("/*/Machine/Amp"), MCD_T("]]>"), CMarkup::MNF_WITHCDATA ) ); // replace CData Sections
	Check( xml.FindGetData( MCD_T("/*/Machine/Amp") ) == MCD_T("]]>") );
	Check( xml.FindSetData( MCD_T("/*/Machine/AmpNew"), MCD_T("]]>"), CMarkup::MNF_WITHCDATA ) ); // add new
	Check( xml.FindGetData( MCD_T("/*/Machine/AmpNew") ) == MCD_T("]]>") );
	Check( xml.FindSetData( MCD_T("/*/Machine/Atr/@test"), MCD_T("&#x20;"), CMarkup::MNF_WITHREFS ) );
	Check( xml.FindGetData( MCD_T("/*/Machine/Atr/@test") ) == MCD_T(" ") );
	Check( xml.FindSetData( MCD_T("/Config/@test"), MCD_T("&#x20;") ) );
	Check( xml.FindGetData( MCD_T("/Config/@test") ) == MCD_T("&#x20;") );

	// GetDocElemCount Test
	StartCheckZone( MCD_T("GetDocElemCount Test") );
	xml.SetDoc(NULL);
	Check( xml.GetDocElemCount() == 0 );
	xml.AddElem( MCD_T("B") );
	Check( xml.GetDocElemCount() == 1 );
	xml.SetDoc( MCD_T("<HTML><BR></HTML>") );
	Check( xml.GetDocElemCount() == 2 );
	xml.FindElem( MCD_T("//BR") );
	xml.AddChildElem( MCD_T("X") );
	Check( xml.GetDocElemCount() == 3 );
	xml.RemoveElem();
	Check( xml.GetDocElemCount() == 1 );
	xml.OutOfElem();
	xml.RemoveElem();
	Check( xml.GetDocElemCount() == 0 );
	xml.AddElem( MCD_T("B") );
	Check( xml.GetDocElemCount() == 1 );

	// HTML Corrupt Quote Test
	// recover from corrupt quotes by ignoring quotes not preceeded by an equal sign
	StartCheckZone( MCD_T("HTML Corrupt Quote Test") );
	xml.SetDoc( MCD_T("<a href=\"<td width=\"101\"><a href=\'<td class=\'j2\'><a/>") );
	Check( xml.FindElem() );
	Check( xml.GetAttrib( MCD_T("href") ) == MCD_T("<td width=") );
	Check( xml.FindElem() );
	Check( xml.GetAttrib( MCD_T("href") ) == MCD_T("<td class=") );
	Check( xml.FindElem() );
	Check( ! xml.FindElem() );

	// Base64 Test
	StartCheckZone( MCD_T("Base64 Test") );
	unsigned char aBin[7] = { 0x91, 0xff, 0xfe, 0xaa, 0x00, 0x01, 0x8c };
	unsigned char aBin2[20];
	MCD_STR strBase64 = xml.EncodeBase64( aBin, 7 );
	Check( xml.DecodeBase64( strBase64, aBin2, 20 ) == 7 );
	Check( memcmp( aBin, aBin2, 7 ) == 0 );
#endif // not MSXML

	// Newset Test (AddNode when root node has been removed)
	StartCheckZone( MCD_T("Newset Test") );
	xml.SetDoc( MCD_T("<set>test</set>") );
	xml.FindNode();
	xml.RemoveNode();
	xml.AddNode( xml.MNT_PROCESSING_INSTRUCTION, MCD_T("xml version=\"1.0\"") );
	xml.AddElem( MCD_T("newset"), MCD_T("test") );
	// <?xml version="1.0"?>
	// <newset>test</newset>
	xml.ResetPos();
	Check( xml.FindElem() );
	Check( xml.GetTagName() == MCD_T("newset") );
	Check( xml.GetData() == MCD_T("test") );

	// Index Test (Sorting)
	// One of the uses of the Index methods is sorting
	// whether your XML is serving as the live data source for a list or grid
	// or you are processing input XML and generating sorted output XML
	// you often want to retrieve your keys into an efficient array
	// and cross reference back to the XML using the element indexes
	//
	// Build sample document with last names and random 3-digit code
#if ! defined(MARKUP_MSXML)
	StartCheckZone( MCD_T("Index Test") );
	int nI, nJ;
	MCD_PCSZ aszSortables[] =
	{
		MCD_T("Hoffman"),
		MCD_T("Garvey"),
		MCD_T("Woods"),
		MCD_T("Jefferson"),
		MCD_T("Garcia"),
		MCD_T("Jordan"),
		NULL
	};
	xml.SetDoc( NULL );
	xml.AddElem( MCD_T("ATTENDANCE") );
	for ( nI=0; aszSortables[nI]; ++nI )
	{
		xml.AddChildElem( MCD_T("ATTENDEE") );
		xml.SetChildAttrib( MCD_T("lastname"), aszSortables[nI] );
		xml.SetChildAttrib( MCD_T("code"), RandInt(900) + 100 );
	}
	int nAttendeeCount = nI;

	// Extract sort indexes
	int* aSortIndexes = new int[nAttendeeCount];
	xml.ResetChildPos();
	xml.IntoElem();
	nI = 0;
	while ( xml.FindElem(MCD_T("ATTENDEE")) )
		aSortIndexes[nI++] = xml.GetElemIndex();
	// Sort
	int nSwap;
	MCD_STR strI, strJ;
	for ( nI=0; nI<nAttendeeCount-1; ++nI )
	{
		xml.GotoElemIndex( aSortIndexes[nI] );
		strI = xml.GetAttrib( MCD_T("lastname") );
		for ( nJ=nI+1; nJ<nAttendeeCount; ++nJ )
		{
			xml.GotoElemIndex( aSortIndexes[nJ] );
			strJ = xml.GetAttrib( MCD_T("lastname") );
			if ( strI > strJ )
			{
				nSwap = aSortIndexes[nI];
				aSortIndexes[nI] = aSortIndexes[nJ];
				aSortIndexes[nJ] = nSwap;
			}
		}
	}
	// At this point, the aSortIndexes can be used
	// without modifying the XML document to access elements
	// For example, find element that was sorted to the beginning of the list
	xml.GotoElemIndex( aSortIndexes[0] );
	Check( xml.GetAttrib(MCD_T("lastname")) == MCD_T("Garcia") );
	// We can also generate a sorted document with subdocuments
	xml.ResetPos();
	xml.FindElem();
	xml2.SetDoc( NULL );
	xml2.AddElem( xml.GetTagName() );
	for ( nI=0; nI<nAttendeeCount; ++nI )
	{
		xml.GotoElemIndex( aSortIndexes[nI] );
		xml2.AddChildSubDoc( xml.GetSubDoc() );
	}
	xml2.ResetPos();
	xml2.FindChildElem();
	Check( xml2.GetChildAttrib(MCD_T("lastname")) == MCD_T("Garcia") );
	delete [] aSortIndexes;

	// Offsets Check
	int nOCS, nOCL, nOCInS, nOCInL;
	StartCheckZone( MCD_T("Offsets Check") );
	xml.SetDoc( MCD_T("<A b='c' d=e><F/><G>hij</G><K></K></A>") );
	Check( xml.FindElem() );
	Check( xml.GetOffsets(&nOCS,&nOCL,&nOCInS,&nOCInL) );
	Check( nOCS == 0 && nOCL == 38 && nOCInS == 13 && nOCInL == 21 );
	Check( xml.GetAttribOffsets(MCD_T("b"),&nOCS,&nOCL,&nOCInS,&nOCInL) );
	Check( nOCS == 3 && nOCL == 5 && nOCInS == 6 && nOCInL == 1 );
	Check( xml.GetAttribOffsets(MCD_T("d"),&nOCS,&nOCL,&nOCInS,&nOCInL) );
	Check( nOCS == 9 && nOCL == 3 && nOCInS == 11 && nOCInL == 1 );
	Check( xml.FindChildElem() );
	Check( xml.IntoElem() );
	Check( xml.GetOffsets(&nOCS,&nOCL,&nOCInS,&nOCInL) ); // 13 <F/>
	Check( nOCS == 13 && nOCL == 4 && nOCInS == 13 && nOCInL == 0 );
	Check( xml.FindElem() );
	Check( xml.GetOffsets(&nOCS,&nOCL,&nOCInS,&nOCInL) ); // 17 <G>hij</G>
	Check( nOCS == 17 && nOCL == 10 && nOCInS == 20 && nOCInL == 3 );
	Check( xml.FindElem() );
	Check( xml.GetOffsets(&nOCS,&nOCL,&nOCInS,&nOCInL) ); // 27 <K></K>
	Check( nOCS == 27 && nOCL == 7 && nOCInS == 30 && nOCInL == 0 );

	// String Reference Capacity Check
	StartCheckZone( MCD_T("String Reference Capacity Check") );
	// Allocate a string to a specified capacity and assign a (shorter) value
	#define MCD_STRASSIGNCAPACITY(s,p,n,c) { MCD_CHAR* d=MCD_GETBUFFER(s,c);\
		memcpy(d,p,n*sizeof(MCD_CHAR)); MCD_RELEASEBUFFER(s,d,n); }
	const int nCapacity = 10000;
	MCD_PCSZ pszInit = MCD_T("<init/>");
	int nInitLen = MCD_PSZLEN(pszInit);
	MCD_STR strRefCheckDoc;
	MCD_STRASSIGNCAPACITY( strRefCheckDoc, pszInit, nInitLen, nCapacity );
	int nGivenCapacity = MCD_STRCAPACITY(strRefCheckDoc);
	Check( nGivenCapacity >= nCapacity );
	MCD_STR strRefCountingVerify = strRefCheckDoc;
	if (MCD_STRCAPACITY(strRefCountingVerify) == nGivenCapacity)
	{
		// String implementation has reference counting, make sure CMarkup utilizes it
		MCD_STRCLEAR(strRefCountingVerify); // release ref to strRefCheckDoc
		Check( xml.SetDoc(strRefCheckDoc) ); // set by reference
		Check( MCD_STRCAPACITY(xml.GetDoc()) == nGivenCapacity );
		MCD_STR strDocResult = xml.GetDoc();
		Check( MCD_STRCAPACITY(strDocResult) == nGivenCapacity );
		MCD_PCSZ pszSet = MCD_T("<set_over_top_of_same_mem/>");
		int nSetLen = MCD_PSZLEN(pszSet);
		xml.SetDoc( MCD_T("") ); // release ref to strRefCheckDoc without clearing indexes
		MCD_STRCLEAR(strDocResult); // release ref to strRefCheckDoc

		// Now make sure reassigning with smaller capacity doesn't give up original capacity
		MCD_STRASSIGNCAPACITY( strRefCheckDoc, pszSet, nSetLen, nSetLen );
		Check( MCD_STRCAPACITY(strRefCheckDoc) == nGivenCapacity );
		Check( xml.SetDoc(strRefCheckDoc) ); // by reference
		Check( MCD_STRCAPACITY(xml.GetDoc()) == nGivenCapacity );
		MCD_STR strDocResult2 = xml.GetDoc();
		Check( MCD_STRCAPACITY(strDocResult2) == nGivenCapacity );
	}

#endif // not MSXML

	// Path vs XPath check
	// CMarkupMSXML was changed in Release 8.0 to behave like CMarkup
	// Use explicit selectSingleNode and selectNodes for XPath
	StartCheckZone( MCD_T("Path vs XPath Test") );
	Check( xml.SetDoc(
		MCD_T("<?xml version=\"1.0\" encoding=\"windows-1252\"?>\r\n")
		MCD_T("<root>\r\n")
		MCD_T("<B ID=\"B1\"/>\r\n")
		MCD_T("<B ID=\"B2\">\r\n")
		MCD_T("<C ID=\"C1\"/>\r\n")
		MCD_T("</B>\r\n")
		MCD_T("</root>\r\n")
		) );
	Check( ! xml.FindElem(MCD_T("/root/B/C")) ); // Absolute path fails

	// Attributes in processing instructions
	StartCheckZone( MCD_T("PI Attribs Test") );
	Check( xml.SetDoc(
		MCD_T("<PIATTRIBS>\n")
			MCD_T("<?testapp x=\"hello\"?>\n")
			MCD_T("<ITEM/>\n")
		MCD_T("</PIATTRIBS>\n")
		) );
#if defined(MARKUP_MSXML)
	// MSXML only supports existing version, encoding, and standalone attributes in XML declaration
	Check( xml.InsertNode( xml.MNT_PROCESSING_INSTRUCTION, MCD_T("xml version=\"1.0\"") ) );
	Check( xml.GetAttrib(MCD_T("version")) == MCD_T("1.0") );
#else // not MSXML
	Check( xml.InsertNode( xml.MNT_PROCESSING_INSTRUCTION, MCD_T("xml") ) );
	Check( xml.SetAttrib( MCD_T("version"), MCD_T("1.0") ) );
	Check( xml.SetAttrib( MCD_T("encoding"), MCD_T("UTF-8") ) );
	Check( xml.FindElem() );
	Check( xml.IntoElem() );
	Check( xml.FindNode(xml.MNT_PROCESSING_INSTRUCTION) );
	Check( xml.GetAttrib(MCD_T("x")) == MCD_T("hello") );
	Check( xml.FindElem() );
	Check( xml.IntoElem() );
	Check( xml.InsertNode( xml.MNT_PROCESSING_INSTRUCTION, MCD_T("testapp")) );
	Check( xml.SetAttrib( MCD_T("x"), MCD_T("yes I mean") ) );
	Check( xml.SetAttrib( MCD_T("x"), MCD_T("hi") ) ); // replace
	Check( xml.SetAttrib( MCD_T("y"), MCD_T("there") ) ); // multiple
	Check( xml.GetAttrib(MCD_T("y")) == MCD_T("there") );
#endif // not MSXML

#if ! defined(MARKUP_MSXML)
	// UTF16ToUTF8 Test
	char szUTF8[5];
	int nUTFLen;
	unsigned short wszUTF16[3];
	StartCheckZone( MCD_T("UTF-16 UTF-8 Conversion Test") );
	unsigned short wszUTF16Test1[3] = { 0xD950, 0xDF21, 0 }; // 0x64321
	nUTFLen = xml.UTF16To8(szUTF8,wszUTF16Test1,5);
	Check( memcmp(szUTF8,"\xF1\xA4\x8C\xA1",4) == 0 );
	nUTFLen = xml.UTF8To16(wszUTF16,szUTF8,nUTFLen+1);
	Check( memcmp(wszUTF16,wszUTF16Test1,4) == 0 );
	unsigned short wszUTF16Test2[3] = { 0xD834, 0xDD1E, 0 }; // 0x1D11E treble clef
	nUTFLen = xml.UTF16To8(szUTF8,wszUTF16Test2,5);
	Check( memcmp(szUTF8,"\xF0\x9D\x84\x9E",4) == 0 );
	nUTFLen = xml.UTF8To16(wszUTF16,szUTF8,nUTFLen+1);
	Check( memcmp(wszUTF16,wszUTF16Test2,4) == 0 );
	unsigned short wszUTF16Test3[2] = { 0x007A, 0 }; // z
	nUTFLen = xml.UTF16To8(szUTF8,wszUTF16Test3,5);
	Check( memcmp(szUTF8,"z",1) == 0 );
	nUTFLen = xml.UTF8To16(wszUTF16,szUTF8,nUTFLen+1);
	Check( memcmp(wszUTF16,wszUTF16Test3,2) == 0 );

#if ! defined(MARKUP_WINCONV) && ! defined(MARKUP_ICONV) && ! defined(MARKUP_WCHAR)
	// ANSI to UTF-8 Test
	StartCheckZone( MCD_T("ANSI UTF-8 Conversion Test") );
	// The AToUTF8 and UTF8ToA functions require setlocale to be called beforehand
	setlocale( LC_ALL, "C" );
	if ( setlocale(LC_ALL,".1252") ) // is Windows-1252 code page available?
	{
		// Windows 1252, registered trademark symbol \xAE is \xC2\xAE in UTF-8 (Unicode 174)
		MCD_STR strATOU = xml.AToUTF8("\xAE");
		Check( strATOU == "\xC2\xAE" );
		MCD_STR strUTOA = xml.UTF8ToA(strATOU);
		Check( strUTOA == "\xAE" );
		// euro symbol \x80 is \xC2\xAE in UTF-8 (Unicode 174)
		strATOU = xml.AToUTF8("\x80");
		Check( strATOU == "\xE2\x82\xAC" );
		strUTOA = xml.UTF8ToA(strATOU);
		Check( strUTOA == "\x80" );
	}
	if ( setlocale(LC_ALL,".936") ) // is GB2312 DBCS code page available?
	{
		// GB2312 Chinese charset
		// middle character \xD6\xD0 is \xE4\xB8\xAD in UTF-8 (Unicode 20013)
		MCD_STR strATOU = xml.AToUTF8("\xD6\xD0");
		Check( strATOU == "\xE4\xB8\xAD" );
		MCD_STR strUTOA = xml.UTF8ToA(strATOU);
		Check( strUTOA == "\xD6\xD0" );
	}
	setlocale( LC_ALL, "" ); // reset to ACP
#endif // not WINCONV and not ICONV and not WCHAR
#endif // not MSXML
	//]CMARKUPDEV

	MCD_CHAR szResults[100];
	if ( m_nErrorCount )
	{
		MCD_SPRINTF( MCD_SSZ(szResults), MCD_T("%d errors, "), m_nErrorCount );
		m_strResult += szResults;
	}
	MCD_PCSZ pszCharType = MCD_T("b");
	if ( sizeof(MCD_CHAR) == 2 )
		pszCharType = MCD_T("w");
	else if ( sizeof(MCD_CHAR) == 4 )
		pszCharType = MCD_T("dw");
	MCD_SPRINTF( MCD_SSZ(szResults), MCD_T("%d checks, perf %s/ms: create %d, attrib %d, parse %d, attrib %d"),
		m_nTotalChecks, pszCharType, m_nCreateSpeed, m_nAttribCreateSpeed, m_nParseSpeed, m_nAttribParseSpeed );
	m_strResult += szResults;
	if ( m_nWriterSpeed )
	{
		MCD_SPRINTF( MCD_SSZ(szResults), MCD_T("\r\nfile mode perf b/ms: writer %d, reader %d"), m_nWriterSpeed, m_nReaderSpeed );
		m_strResult += szResults;
	}
	m_strResult += MCD_T("\r\n");
	if ( ! m_bLSFileLoaded )
		m_strResult += MCD_T("File I/O tests skipped\r\n");
	return 0;
}

void CMarkupTest::LoadParse( MCD_CSTR_FILENAME pszPath )
{
	// Load and parse with CMarkup
	#ifdef MARKUP_MSXML
	CMarkupMSXML xml;
	#else
	CMarkup xml;
	#endif

	int nTimeStart = GetMilliCount();
	bool bResult = xml.Load( pszPath );
	m_strLoadResult = xml.GetResult();
	m_nLoadMilliseconds = GetMilliSpan( nTimeStart );

	// Display results
	MCD_CHAR szResult[100];
	if ( bResult )
	{
		MCD_SPRINTF( MCD_SSZ(szResult), MCD_T("Loaded and parsed in %d milliseconds"), m_nLoadMilliseconds );
	}
	else
	{
		MCD_SPRINTF( MCD_SSZ(szResult), MCD_T("Load or parse error (after %d milliseconds)"), m_nLoadMilliseconds );
	}
	m_strResult = szResult;
	MCD_STR strError = xml.GetError();
	if ( ! MCD_STRISEMPTY(strError) )
	{
		m_strResult += MCD_T("\r\n");
		m_strResult += strError;
	}
}
